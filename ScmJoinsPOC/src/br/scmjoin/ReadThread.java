package br.scmjoin;

public class ReadThread implements Runnable {

	static int blockSize = 8192;
	int numerobloco = 0;
	int numeroblocoend = 0;

	public ReadThread(int numerobloco, int endblock) {
		super();
		this.numerobloco = numerobloco;
		this.numeroblocoend = endblock;
	}

	public static void main(String args[]) throws Exception {
		
		HandleFile haf;
		haf = new HandleFile(blockSize);
		Measure.setStart();
		haf.open("D:/TPCH/TPCH10/lineitem.b");
		int numerodeBlocos = haf.numberOfBlocks;
		haf.close();
		haf = null;
		ReadThread read1 = new ReadThread(1, numerodeBlocos/8);
		ReadThread read2 = new ReadThread(numerodeBlocos/8, 2*(numerodeBlocos/8));
		ReadThread read3 = new ReadThread(2*(numerodeBlocos/8), 3*(numerodeBlocos/8));
		ReadThread read4 = new ReadThread(3*(numerodeBlocos/8), 4*(numerodeBlocos/8));
		ReadThread read5 = new ReadThread(4*(numerodeBlocos/8), 5*(numerodeBlocos/8));
		ReadThread read6 = new ReadThread(5*(numerodeBlocos/8), 6*(numerodeBlocos/8));
		ReadThread read7 = new ReadThread(6*(numerodeBlocos/8), 7*(numerodeBlocos/8));
		ReadThread read8 = new ReadThread(7*(numerodeBlocos/8), 99999999);
		
		new Thread(read1).start();
		new Thread(read2).start();
		new Thread(read3).start();
		new Thread(read4).start();
		new Thread(read5).start();
		new Thread(read6).start();
		new Thread(read7).start();
		new Thread(read8).start();
		
//		int qtdLines = 0;
//		try {
//			HandleFile haf;
//			haf = new HandleFile(blockSize);
//			Measure.setStart();
//			haf.open("D:/TPCH/TPCH10/lineitem.b");
//			String header = RafIOCalc.getHeaderString(haf);
//			System.out.println(header);
//			System.out.println(haf.numberOfBlocks);
//			System.out.println(haf.numberOfTuples);
//			System.out.println(haf.mediumSizeOfTuple);
//			String linha = "";
//			byte block[] = new byte[blockSize];
//			byte tupleBlock[];
//			block = haf.nextBlock();
//			while (block != null) {
//				tupleBlock = haf.nextTuple(block);
//				linha = RafIOCalc.getLineString(haf, tupleBlock,
//						haf.getQtCols());-0=================================
//				while (linha != null) {
//					qtdLines++;
//					tupleBlock = haf.nextTuple(block);
//					if (tupleBlock != null)
//						linha = RafIOCalc.getLineString(haf, tupleBlock,
//								haf.getQtCols());
//					else
//						linha = null;
//				}
//				block = haf.nextBlock();
//			}
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		System.out.println("Numero de Linhas: " + qtdLines);
//		double end = System.currentTimeMillis();
//		double totaltime = Measure.getElapsedTimeDouble();
//		System.out.println(totaltime);
//		
//		
//		
		
		
	}

	@Override
	public void run() {
		int qtdLines = 0;
		try {
			HandleFile haf;
			haf = new HandleFile(blockSize);
			Measure.setStart();
			haf.open("D:/TPCH/TPCH10/lineitem.b");
			String header = RafIOCalc.getHeaderString(haf);
			System.out.println(header);
			System.out.println(haf.numberOfBlocks);
			System.out.println(haf.numberOfTuples);
			System.out.println(haf.mediumSizeOfTuple);
			String linha = "";
			byte block[] = new byte[blockSize];
			byte tupleBlock[];
			int numblockstart = this.numerobloco;
			block = haf.readBlock(numblockstart);
			while (numblockstart < this.numeroblocoend && block != null) {
				numblockstart++;
				tupleBlock = haf.nextTuple(block);
				linha = RafIOCalc.getLineString(haf, tupleBlock,
						haf.getQtCols());
				while (linha != null) {

					qtdLines++;
					tupleBlock = haf.nextTuple(block);
					if (tupleBlock != null)
						linha = RafIOCalc.getLineString(haf, tupleBlock,
								haf.getQtCols());
					else
						linha = null;
				}
				block = haf.readBlock(numblockstart);
			}
			System.out.println(numblockstart);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Numero de Linhas: " + qtdLines + " - Start: "
				+ this.numerobloco);
		double end = System.currentTimeMillis();
		double totaltime = Measure.getElapsedTimeDouble();
		System.out.println(totaltime);

	}

	public static int getBlockSize() {
		return blockSize;
	}

	public static void setBlockSize(int blockSize) {
		ReadThread.blockSize = blockSize;
	}

	public int getNumerobloco() {
		return numerobloco;
	}

	public void setNumerobloco(int numerobloco) {
		this.numerobloco = numerobloco;
	}

}
