package br.scmjoin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Random;
import java.util.TreeMap;

public class DaC_4Threads_Int_POC {

	static TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>> tb1thread11 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();
	static TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>> tb1thread12 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();
	static TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>> tb1thread13 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();
	static TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>> tb1thread14 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();

	static TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>> tb1Nextthread11 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();
	static TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>> tb1Nextthread12 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();
	static TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>> tb1Nextthread13 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();
	static TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>> tb1Nextthread14 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();

	static TreeMap<Integer, ArrayList<Integer>> mapOfTable11 = new TreeMap<Integer, ArrayList<Integer>>();
	static TreeMap<Integer, ArrayList<Integer>> mapOfTable12 = new TreeMap<Integer, ArrayList<Integer>>();
	static TreeMap<Integer, ArrayList<Integer>> mapOfTable13 = new TreeMap<Integer, ArrayList<Integer>>();
	static TreeMap<Integer, ArrayList<Integer>> mapOfTable14 = new TreeMap<Integer, ArrayList<Integer>>();

	static TreeMap<Integer, ArrayList<Integer>> mapOfTable21 = new TreeMap<Integer, ArrayList<Integer>>();
	static TreeMap<Integer, ArrayList<Integer>> mapOfTable22 = new TreeMap<Integer, ArrayList<Integer>>();
	static TreeMap<Integer, ArrayList<Integer>> mapOfTable23 = new TreeMap<Integer, ArrayList<Integer>>();
	static TreeMap<Integer, ArrayList<Integer>> mapOfTable24 = new TreeMap<Integer, ArrayList<Integer>>();

	static TreeMap<Integer, ArrayList<Integer>> mapOfNextTable11 = new TreeMap<Integer, ArrayList<Integer>>();
	static TreeMap<Integer, ArrayList<Integer>> mapOfNextTable12 = new TreeMap<Integer, ArrayList<Integer>>();
	static TreeMap<Integer, ArrayList<Integer>> mapOfNextTable13 = new TreeMap<Integer, ArrayList<Integer>>();
	static TreeMap<Integer, ArrayList<Integer>> mapOfNextTable14 = new TreeMap<Integer, ArrayList<Integer>>();

	static TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>> tb2thread11 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();
	static TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>> tb2thread12 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();
	static TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>> tb2thread13 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();
	static TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>> tb2thread14 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();

	static int numbersOfTuplesInInterMemory;
	static int numbersOfKeysInInterMemory;

	static int numbersOfTuplesInMemorytb1;
	static int numbersOfKeysInMemorytb1;

	static int numbersOfTuplesInMemorytb2;
	static int numbersOfKeysInMemorytb2;

	static int keyInMemorytb11;
	static int tupleInMemorytb11;

	static int keyOutMemorytb11;
	static int tupleOutMemorytb11;

	static int keyInMemorytb12;
	static int tupleInMemorytb12;

	static int keyOutMemorytb12;
	static int tupleOutMemorytb12;

	static int keyInMemorytb13;
	static int tupleInMemorytb13;

	static int keyOutMemorytb13;
	static int tupleOutMemorytb13;

	static int keyInMemorytb14;
	static int tupleInMemorytb14;

	static int keyOutMemorytb14;
	static int tupleOutMemorytb14;

	static int keyInMemorytb21;
	static int tupleInMemorytb21;

	static int keyOutMemorytb21;
	static int tupleOutMemorytb21;

	static int keyInMemorytb22;
	static int tupleInMemorytb22;

	static int keyOutMemorytb22;
	static int tupleOutMemorytb22;

	static int keyInMemorytb23;
	static int tupleInMemorytb23;

	static int keyOutMemorytb23;
	static int tupleOutMemorytb23;

	static int keyInMemorytb24;
	static int tupleInMemorytb24;

	static int keyOutMemorytb24;
	static int tupleOutMemorytb24;

	static String data;

	boolean memory;

	static int numberOfTupleFinalJoin2;
	static int numberOfTupleFinalJoin2Disk;

	static int numfinal;

	static int numberOfOverFlowTb1;
	static int numberOfOverFlowTb2;
	static int numberOfWriteTb1;
	static int numberOfWriteTb2;

	static int numtotaltuplestb = 0;

	EarlyHash sincron01 = new EarlyHash();
	EarlyHash sincron02 = new EarlyHash();
	private static LogFile resultlog;
	static int blockSize = 8192;
	int numerobloco = 0;
	int numeroblocoend = 0;
	double totaltime = 0;
	public static HashFunctionDaC h4 = new HashFunctionDaC();
	public static HashFunctionDaC hFull = new HashFunctionDaC();
	public static int auxRandom;

	static int reLeiturasAcumuladas = 0;
	static int reLeiturasAcumuladasTb1 = 0;
	static int reLeiturasAcumuladasTb2 = 0;
	static int reLeiturasAcumuladasTb3 = 0;
	static int reLeiturasAcumuladasTb4 = 0;
	static int reLeiturasAcumuladasTb5 = 0;
	public static String tupleBD = "";

	public static int calcmemory;
	public static int calcmemoryInter;
	public static String j = "";

	public static void main(String[] args) throws Exception {

		DaC_4Threads_Int_POC join = new DaC_4Threads_Int_POC();

		String[] joinColumns = null;
		String[] earlyColumns = null;
		String[] earlyColumnsTb1 = null;
		String[] earlyColumnsTb2 = null;
		boolean firstJoinTable = false;
		boolean firstLastTable = false;
		boolean lastJoin = false;
		String[] selectRelation = null;
		String headerJoin = null;
		String teste = "QA";
		j = teste;

		int memorySize = Integer.parseInt("209715200");
		String result;
		int escritasOverflowAcumuladasTb1 = 0;
		int escritasOverflowAcumuladasTb2 = 0;
		int escritasTotal = 0;
		int escritasPepiLine = 0;
		double rrStartTime, rrStopTime;
		double tempoPhase1 = 0.0;
		double tempoPhase2 = 0.0;
		double tempoTotal = 0.0;

		calcmemory = (int) (memorySize * 0.8);
		calcmemoryInter = memorySize - calcmemory;

		h4 = new HashFunctionDaC(4);
		hFull = new HashFunctionDaC((memorySize / blockSize) / 4);
		auxRandom = (memorySize / blockSize) / 4;

		result = "C:\\TPCH10\\";
		data = "C:\\TPCH10\\";

		if (teste.equals("QC")) {
			/*
			 * select * from region, lineitem where l orderkey = r regionkey
			 */

			EarlyHash sincron01 = new EarlyHash();
			resultlog = new LogFile(result + "4Threads-QC.txt");

			joinColumns = new String[] { "regionkey" };
			sincron01.setJoinColumns(joinColumns);

			earlyColumns = null;
			sincron01.setEarlyColumns(earlyColumns);

			firstJoinTable = true;
			sincron01.setFirstJoinTable(firstJoinTable);

			firstLastTable = true;
			sincron01.setFirstLastTable(firstLastTable);

			selectRelation = null;
			sincron01.setSelectRelation(selectRelation);

			headerJoin = "orderkey[I(18)]|region[A(55)]|";
			sincron01.setHeaderJoin(headerJoin);

			sincron01.setTable(data + "region.b");
			sincron01.setNumberPhase(1);
			sincron01.setNumberTable(1);

			HandleFile haf11 = new HandleFile(blockSize);
			haf11.create(data + "intTb11.b", headerJoin);
			sincron01.setHaf11(haf11);

			HandleFile haf12 = new HandleFile(blockSize);
			haf12.create(data + "intTb12.b", headerJoin);
			sincron01.setHaf12(haf12);

			HandleFile haf13 = new HandleFile(blockSize);
			haf13.create(data + "intTb13.b", headerJoin);
			sincron01.setHaf13(haf13);

			HandleFile haf14 = new HandleFile(blockSize);
			haf14.create(data + "intTb14.b", headerJoin);
			sincron01.setHaf14(haf14);

			sincron01.setAuxMemoryCurrentTuple(12);
			sincron01.setAuxMemoryCurrentKey(4);
			sincron01.setAuxMemoryPreviusTuple(0);
			sincron01.setAuxMemoryPreviusKey(0);

			sincron01.setMemorySizeJoinKernel(memorySize);

			rrStartTime = System.currentTimeMillis();
			join.phaseOne4Threads(sincron01);
			rrStopTime = System.currentTimeMillis();
			tempoPhase1 = (rrStopTime - rrStartTime) / 1000;

			tupleBD = tupleBD + "REGION|" + numtotaltuplestb + "|" + tempoPhase1 + "|";
			numtotaltuplestb = 0;
			// /Tabela 2
			EarlyHash sincron02 = new EarlyHash();

			joinColumns = new String[] { "orderkey" };
			sincron02.setJoinColumns(joinColumns);

			earlyColumns = null;
			sincron02.setEarlyColumns(earlyColumns);

			firstJoinTable = true;
			sincron02.setFirstJoinTable(firstJoinTable);

			firstLastTable = true;
			sincron02.setFirstLastTable(firstLastTable);

			headerJoin = "orderkey[I(18)]|lineitem[A(55)]|";
			sincron02.setHeaderJoin(headerJoin);

			selectRelation = null;
			sincron02.setSelectRelation(selectRelation);

			sincron02.setTable(data + "lineitem.b");
			sincron02.setNumberPhase(1);
			sincron02.setNumberTable(2);

			HandleFile haf21 = new HandleFile(blockSize);
			haf21.create(data + "intTb21.b", headerJoin);
			sincron02.setHaf21(haf21);

			HandleFile haf22 = new HandleFile(blockSize);
			haf22.create(data + "intTb22.b", headerJoin);
			sincron02.setHaf22(haf22);

			HandleFile haf23 = new HandleFile(blockSize);
			haf23.create(data + "intTb23.b", headerJoin);
			sincron02.setHaf23(haf23);

			HandleFile haf24 = new HandleFile(blockSize);
			haf24.create(data + "intTb24.b", headerJoin);
			sincron02.setHaf24(haf24);

			sincron02.setHaf11(sincron01.haf11);
			sincron02.setHaf12(sincron01.haf12);
			sincron02.setHaf13(sincron01.haf13);
			sincron02.setHaf14(sincron01.haf14);

			sincron02.setAuxMemoryCurrentTuple(12);
			sincron02.setAuxMemoryCurrentKey(4);
			sincron02.setAuxMemoryPreviusTuple(12);
			sincron02.setAuxMemoryPreviusKey(4);

			sincron02.setMemorySizeJoinKernel(memorySize);

			rrStartTime = System.currentTimeMillis();
			join.phaseOne4Threads(sincron02);
			rrStopTime = System.currentTimeMillis();
			double scanTb2 = (rrStopTime - rrStartTime) / 1000;
			tempoPhase1 += (rrStopTime - rrStartTime) / 1000;

			tupleBD = tupleBD + "LINEITEM|" + numtotaltuplestb + "|" + scanTb2 + "|";

			escritasOverflowAcumuladasTb1 = haf11.numberofWrite + haf12.numberofWrite + haf13.numberofWrite
					+ haf14.numberofWrite;
			escritasOverflowAcumuladasTb2 = haf21.numberofWrite + haf22.numberofWrite + haf23.numberofWrite
					+ haf24.numberofWrite;

			escritasTotal += escritasOverflowAcumuladasTb1 + escritasOverflowAcumuladasTb2;

			tempoTotal += tempoPhase1;

			tupleBD = tupleBD + escritasOverflowAcumuladasTb1 + "|" + escritasOverflowAcumuladasTb2 + "|"
					+ (escritasOverflowAcumuladasTb1 + escritasOverflowAcumuladasTb2) + "|" + tempoPhase1 + "|";

			LateHash join12 = new LateHash();

			joinColumns = new String[] { "orderkey" };
			join12.setJoinColumns(joinColumns);

			earlyColumnsTb1 = null;
			join12.setEarlyColumnsTb1(earlyColumnsTb1);

			earlyColumnsTb2 = null;
			join12.setEarlyColumnsTb1(earlyColumnsTb2);

			lastJoin = true;
			join12.setLastJoin(lastJoin);

			String[] tableforLastRelation = new String[] { data + "orders.b", data + "lineitem.b" };
			join12.setTableforLastRelation(tableforLastRelation);

			join12.setFirstJoinTb1(true);
			join12.setFirstJoinTb2(true);

			headerJoin = "orderkey[I(18)]|region[A(55)]|lineitem[A(55)]|";
			join12.setHeaderJoin(headerJoin);

			join12.setReReadDiskTb1(new String[] { "region" });

			join12.setReReadDiskTb2(new String[] { "lineitem" });

			join12.setJoinTb11(data + "intTb11.b");
			join12.setJoinTb12(data + "intTb12.b");
			join12.setJoinTb13(data + "intTb13.b");
			join12.setJoinTb14(data + "intTb14.b");

			join12.setJoinTb21(data + "intTb21.b");
			join12.setJoinTb22(data + "intTb22.b");
			join12.setJoinTb23(data + "intTb23.b");
			join12.setJoinTb24(data + "intTb24.b");

			rrStartTime = System.currentTimeMillis();
			join.phaseTwo4Threads(join12);

			rrStopTime = System.currentTimeMillis();

			tempoPhase2 = (rrStopTime - rrStartTime) / 1000;

			tempoTotal += tempoPhase2;

			tupleBD = tupleBD + tempoPhase2 + "|" + escritasTotal + "|" + reLeiturasAcumuladasTb1 + "|"
					+ reLeiturasAcumuladasTb2 + "|" + reLeiturasAcumuladas + "|"
					+ (numberOfTupleFinalJoin2 + numberOfTupleFinalJoin2Disk) + "|" + tempoTotal;
			System.out.println(tupleBD);
			resultlog.writeLog(tupleBD);

		}

		if (teste.equals("QA")) {
			/*
			 * select * from orders, lineitem where o orderdate >= 1993-10-01 and o
			 * orderdate < 1994-01-01 and l returnflag = �R� and l orderkey = o orderkey
			 */

			EarlyHash sincron01 = new EarlyHash();
			resultlog = new LogFile(result + "4Threads-QA.txt");

			joinColumns = new String[] { "orderkey" };
			sincron01.setJoinColumns(joinColumns);

			earlyColumns = null;
			sincron01.setEarlyColumns(earlyColumns);

			firstJoinTable = true;
			sincron01.setFirstJoinTable(firstJoinTable);

			firstLastTable = true;
			sincron01.setFirstLastTable(firstLastTable);

			selectRelation = new String[] { "o_orderdate>=1993-10-01", "o_orderdate<1994-01-01" };
			sincron01.setSelectRelation(selectRelation);

			headerJoin = "orderkey[I(18)]|orders[A(55)]|";
			sincron01.setHeaderJoin(headerJoin);

			sincron01.setTable(data + "orders.b");
			sincron01.setNumberPhase(1);
			sincron01.setNumberTable(1);

			HandleFile haf11 = new HandleFile(blockSize);
			haf11.create(data + "intTb11.b", headerJoin);
			sincron01.setHaf11(haf11);

			HandleFile haf12 = new HandleFile(blockSize);
			haf12.create(data + "intTb12.b", headerJoin);
			sincron01.setHaf12(haf12);

			HandleFile haf13 = new HandleFile(blockSize);
			haf13.create(data + "intTb13.b", headerJoin);
			sincron01.setHaf13(haf13);

			HandleFile haf14 = new HandleFile(blockSize);
			haf14.create(data + "intTb14.b", headerJoin);
			sincron01.setHaf14(haf14);

			sincron01.setAuxMemoryCurrentTuple(12);
			sincron01.setAuxMemoryCurrentKey(4);
			sincron01.setAuxMemoryPreviusTuple(0);
			sincron01.setAuxMemoryPreviusKey(0);

			sincron01.setMemorySizeJoinKernel(memorySize);

			rrStartTime = System.currentTimeMillis();
			join.phaseOne4Threads(sincron01);
			rrStopTime = System.currentTimeMillis();
			tempoPhase1 = (rrStopTime - rrStartTime) / 1000;

			tupleBD = tupleBD + "ORDERS|" + numtotaltuplestb + "|" + tempoPhase1 + "|";
			numtotaltuplestb = 0;
			// /Tabela 2
			EarlyHash sincron02 = new EarlyHash();

			joinColumns = new String[] { "orderkey" };
			sincron02.setJoinColumns(joinColumns);

			earlyColumns = null;
			sincron02.setEarlyColumns(earlyColumns);

			firstJoinTable = true;
			sincron02.setFirstJoinTable(firstJoinTable);

			firstLastTable = true;
			sincron02.setFirstLastTable(firstLastTable);

			headerJoin = "orderkey[I(18)]|lineitem[A(55)]|";
			sincron02.setHeaderJoin(headerJoin);

			selectRelation = new String[] { "l_returnflag=R" };
			sincron02.setSelectRelation(selectRelation);

			sincron02.setTable(data + "lineitem.b");
			sincron02.setNumberPhase(1);
			sincron02.setNumberTable(2);

			HandleFile haf21 = new HandleFile(blockSize);
			haf21.create(data + "intTb21.b", headerJoin);
			sincron02.setHaf21(haf21);

			HandleFile haf22 = new HandleFile(blockSize);
			haf22.create(data + "intTb22.b", headerJoin);
			sincron02.setHaf22(haf22);

			HandleFile haf23 = new HandleFile(blockSize);
			haf23.create(data + "intTb23.b", headerJoin);
			sincron02.setHaf23(haf23);

			HandleFile haf24 = new HandleFile(blockSize);
			haf24.create(data + "intTb24.b", headerJoin);
			sincron02.setHaf24(haf24);

			sincron02.setHaf11(sincron01.haf11);
			sincron02.setHaf12(sincron01.haf12);
			sincron02.setHaf13(sincron01.haf13);
			sincron02.setHaf14(sincron01.haf14);

			sincron02.setAuxMemoryCurrentTuple(12);
			sincron02.setAuxMemoryCurrentKey(4);
			sincron02.setAuxMemoryPreviusTuple(12);
			sincron02.setAuxMemoryPreviusKey(4);

			sincron02.setMemorySizeJoinKernel(memorySize);

			rrStartTime = System.currentTimeMillis();
			join.phaseOne4Threads(sincron02);
			rrStopTime = System.currentTimeMillis();
			double scanTb2 = (rrStopTime - rrStartTime) / 1000;
			tempoPhase1 += (rrStopTime - rrStartTime) / 1000;

			tupleBD = tupleBD + "LINEITEM|" + numtotaltuplestb + "|" + scanTb2 + "|";

			escritasOverflowAcumuladasTb1 = haf11.numberofWrite + haf12.numberofWrite + haf13.numberofWrite
					+ haf14.numberofWrite;
			escritasOverflowAcumuladasTb2 = haf21.numberofWrite + haf22.numberofWrite + haf23.numberofWrite
					+ haf24.numberofWrite;

			escritasTotal += escritasOverflowAcumuladasTb1 + escritasOverflowAcumuladasTb2;

			tempoTotal += tempoPhase1;

			tupleBD = tupleBD + escritasOverflowAcumuladasTb1 + "|" + escritasOverflowAcumuladasTb2 + "|"
					+ (escritasOverflowAcumuladasTb1 + escritasOverflowAcumuladasTb2) + "|" + tempoPhase1 + "|";

			LateHash join12 = new LateHash();

			joinColumns = new String[] { "orderkey" };
			join12.setJoinColumns(joinColumns);

			earlyColumnsTb1 = null;
			join12.setEarlyColumnsTb1(earlyColumnsTb1);

			earlyColumnsTb2 = null;
			join12.setEarlyColumnsTb1(earlyColumnsTb2);

			lastJoin = true;
			join12.setLastJoin(lastJoin);

			String[] tableforLastRelation = new String[] { data + "orders.b", data + "lineitem.b" };
			join12.setTableforLastRelation(tableforLastRelation);

			join12.setFirstJoinTb1(true);
			join12.setFirstJoinTb2(true);

			headerJoin = "orderkey[I(18)]|orders[A(55)]|lineitem[A(55)]|";
			join12.setHeaderJoin(headerJoin);

			join12.setReReadDiskTb1(new String[] { "orders" });

			join12.setReReadDiskTb2(new String[] { "lineitem" });

			join12.setJoinTb11(data + "intTb11.b");
			join12.setJoinTb12(data + "intTb12.b");
			join12.setJoinTb13(data + "intTb13.b");
			join12.setJoinTb14(data + "intTb14.b");

			join12.setJoinTb21(data + "intTb21.b");
			join12.setJoinTb22(data + "intTb22.b");
			join12.setJoinTb23(data + "intTb23.b");
			join12.setJoinTb24(data + "intTb24.b");

			rrStartTime = System.currentTimeMillis();
			join.phaseTwo4Threads(join12);

			rrStopTime = System.currentTimeMillis();

			tempoPhase2 = (rrStopTime - rrStartTime) / 1000;

			tempoTotal += tempoPhase2;

			tupleBD = tupleBD + tempoPhase2 + "|" + escritasTotal + "|" + reLeiturasAcumuladasTb1 + "|"
					+ reLeiturasAcumuladasTb2 + "|" + reLeiturasAcumuladas + "|"
					+ (numberOfTupleFinalJoin2 + numberOfTupleFinalJoin2Disk) + "|" + tempoTotal;
			System.out.println(tupleBD);
			resultlog.writeLog(tupleBD);

		}

		if (teste.equals("QB")) {
			/*
			 * select * from orders, lineitem where lineitem.l orderkey = orders.o orderkey
			 */

			EarlyHash sincron01 = new EarlyHash();
			resultlog = new LogFile(result + "4Threads-QB.txt");

			joinColumns = new String[] { "orderkey" };
			sincron01.setJoinColumns(joinColumns);

			earlyColumns = null;
			sincron01.setEarlyColumns(earlyColumns);

			firstJoinTable = true;
			sincron01.setFirstJoinTable(firstJoinTable);

			firstLastTable = true;
			sincron01.setFirstLastTable(firstLastTable);

			selectRelation = null;
			sincron01.setSelectRelation(selectRelation);

			headerJoin = "orderkey[I(18)]|orders[A(55)]|";
			sincron01.setHeaderJoin(headerJoin);

			sincron01.setTable(data + "orders.b");
			sincron01.setNumberPhase(1);
			sincron01.setNumberTable(1);

			HandleFile haf11 = new HandleFile(blockSize);
			haf11.create(data + "intTb11.b", headerJoin);
			sincron01.setHaf11(haf11);

			HandleFile haf12 = new HandleFile(blockSize);
			haf12.create(data + "intTb12.b", headerJoin);
			sincron01.setHaf12(haf12);

			HandleFile haf13 = new HandleFile(blockSize);
			haf13.create(data + "intTb13.b", headerJoin);
			sincron01.setHaf13(haf13);

			HandleFile haf14 = new HandleFile(blockSize);
			haf14.create(data + "intTb14.b", headerJoin);
			sincron01.setHaf14(haf14);

			sincron01.setAuxMemoryCurrentTuple(12);
			sincron01.setAuxMemoryCurrentKey(4);
			sincron01.setAuxMemoryPreviusTuple(0);
			sincron01.setAuxMemoryPreviusKey(0);

			sincron01.setMemorySizeJoinKernel(memorySize);

			rrStartTime = System.currentTimeMillis();
			join.phaseOne4Threads(sincron01);
			rrStopTime = System.currentTimeMillis();
			tempoPhase1 = (rrStopTime - rrStartTime) / 1000;

			tupleBD = tupleBD + "ORDERS|" + numtotaltuplestb + "|" + tempoPhase1 + "|";
			numtotaltuplestb = 0;
			// /Tabela 2
			EarlyHash sincron02 = new EarlyHash();

			joinColumns = new String[] { "orderkey" };
			sincron02.setJoinColumns(joinColumns);

			earlyColumns = null;
			sincron02.setEarlyColumns(earlyColumns);

			firstJoinTable = true;
			sincron02.setFirstJoinTable(firstJoinTable);

			firstLastTable = true;
			sincron02.setFirstLastTable(firstLastTable);

			headerJoin = "orderkey[I(18)]|lineitem[A(55)]|";
			sincron02.setHeaderJoin(headerJoin);

			selectRelation = null;
			sincron02.setSelectRelation(selectRelation);

			sincron02.setTable(data + "lineitem.b");
			sincron02.setNumberPhase(1);
			sincron02.setNumberTable(2);

			HandleFile haf21 = new HandleFile(blockSize);
			haf21.create(data + "intTb21.b", headerJoin);
			sincron02.setHaf21(haf21);

			HandleFile haf22 = new HandleFile(blockSize);
			haf22.create(data + "intTb22.b", headerJoin);
			sincron02.setHaf22(haf22);

			HandleFile haf23 = new HandleFile(blockSize);
			haf23.create(data + "intTb23.b", headerJoin);
			sincron02.setHaf23(haf23);

			HandleFile haf24 = new HandleFile(blockSize);
			haf24.create(data + "intTb24.b", headerJoin);
			sincron02.setHaf24(haf24);

			sincron02.setHaf11(sincron01.haf11);
			sincron02.setHaf12(sincron01.haf12);
			sincron02.setHaf13(sincron01.haf13);
			sincron02.setHaf14(sincron01.haf14);

			sincron02.setAuxMemoryCurrentTuple(12);
			sincron02.setAuxMemoryCurrentKey(4);
			sincron02.setAuxMemoryPreviusTuple(12);
			sincron02.setAuxMemoryPreviusKey(4);

			sincron02.setMemorySizeJoinKernel(memorySize);

			rrStartTime = System.currentTimeMillis();
			join.phaseOne4Threads(sincron02);
			rrStopTime = System.currentTimeMillis();
			double scanTb2 = (rrStopTime - rrStartTime) / 1000;
			tempoPhase1 += (rrStopTime - rrStartTime) / 1000;

			tupleBD = tupleBD + "LINEITEM|" + numtotaltuplestb + "|" + scanTb2 + "|";

			escritasOverflowAcumuladasTb1 = haf11.numberofWrite + haf12.numberofWrite + haf13.numberofWrite
					+ haf14.numberofWrite;
			escritasOverflowAcumuladasTb2 = haf21.numberofWrite + haf22.numberofWrite + haf23.numberofWrite
					+ haf24.numberofWrite;

			escritasTotal += escritasOverflowAcumuladasTb1 + escritasOverflowAcumuladasTb2;

			tempoTotal += tempoPhase1;

			tupleBD = tupleBD + escritasOverflowAcumuladasTb1 + "|" + escritasOverflowAcumuladasTb2 + "|"
					+ (escritasOverflowAcumuladasTb1 + escritasOverflowAcumuladasTb2) + "|" + tempoPhase1 + "|";

			LateHash join12 = new LateHash();

			joinColumns = new String[] { "orderkey" };
			join12.setJoinColumns(joinColumns);

			earlyColumnsTb1 = null;
			join12.setEarlyColumnsTb1(earlyColumnsTb1);

			earlyColumnsTb2 = null;
			join12.setEarlyColumnsTb1(earlyColumnsTb2);

			lastJoin = true;
			join12.setLastJoin(lastJoin);

			join12.setHaf1(haf11);

			join12.setHaf2(haf21);

			String[] tableforLastRelation = new String[] { data + "orders.b", data + "lineitem.b" };
			join12.setTableforLastRelation(tableforLastRelation);

			join12.setFirstJoinTb1(true);
			join12.setFirstJoinTb2(true);

			headerJoin = "orderkey[I(18)]|orders[A(55)]|lineitem[A(55)]|";
			join12.setHeaderJoin(headerJoin);

			join12.setReReadDiskTb1(new String[] { "orders" });

			join12.setReReadDiskTb2(new String[] { "lineitem" });

			join12.setHaf11(haf11);
			join12.setHaf12(haf12);

			join12.setHaf21(haf21);
			join12.setHaf22(haf22);

			join12.setJoinTb11(data + "intTb11.b");
			join12.setJoinTb12(data + "intTb12.b");
			join12.setJoinTb13(data + "intTb13.b");
			join12.setJoinTb14(data + "intTb14.b");

			join12.setJoinTb21(data + "intTb21.b");
			join12.setJoinTb22(data + "intTb22.b");
			join12.setJoinTb23(data + "intTb23.b");
			join12.setJoinTb24(data + "intTb24.b");

			rrStartTime = System.currentTimeMillis();
			join.phaseTwo4Threads(join12);

			rrStopTime = System.currentTimeMillis();

			tempoPhase2 = (rrStopTime - rrStartTime) / 1000;

			tempoTotal += tempoPhase2;

			tupleBD = tupleBD + tempoPhase2 + "|" + escritasTotal + "|" + reLeiturasAcumuladasTb1 + "|"
					+ reLeiturasAcumuladasTb2 + "|" + reLeiturasAcumuladas + "|"
					+ (numberOfTupleFinalJoin2 + numberOfTupleFinalJoin2Disk) + "|" + tempoTotal;
			System.out.println(tupleBD);
			resultlog.writeLog(tupleBD);

		}

		if (teste.equals("Q3")) {

			/*
			 * select * from customer, orders, lineitem where c mktsegment = �building� and
			 * c custkey = o custkey and l orderkey = o orderkey and o orderdate <
			 * 1995-03-15 and l shipdate > 1995-03-15
			 */

			EarlyHash sincron01 = new EarlyHash();
			resultlog = new LogFile(result + "4Threads-Q3.txt");

			joinColumns = new String[] { "custkey" };
			sincron01.setJoinColumns(joinColumns);

			earlyColumns = null;
			sincron01.setEarlyColumns(earlyColumns);

			firstJoinTable = true;
			sincron01.setFirstJoinTable(firstJoinTable);

			firstLastTable = false;
			sincron01.setFirstLastTable(firstLastTable);

			selectRelation = new String[] { "c_mktsegment=BUILDING" };
			sincron01.setSelectRelation(selectRelation);

			headerJoin = "custkey[I(18)]|customer[A(55)]|";
			sincron01.setHeaderJoin(headerJoin);

			sincron01.setTable(data + "customer.b");
			sincron01.setNumberPhase(1);
			sincron01.setNumberTable(1);

			HandleFile haf11 = new HandleFile(blockSize);
			haf11.create(data + "intTb11.b", headerJoin);
			sincron01.setHaf11(haf11);

			HandleFile haf12 = new HandleFile(blockSize);
			haf12.create(data + "intTb12.b", headerJoin);
			sincron01.setHaf12(haf12);

			HandleFile haf13 = new HandleFile(blockSize);
			haf13.create(data + "intTb13.b", headerJoin);
			sincron01.setHaf13(haf13);

			HandleFile haf14 = new HandleFile(blockSize);
			haf14.create(data + "intTb14.b", headerJoin);
			sincron01.setHaf14(haf14);

			sincron01.setAuxMemoryCurrentTuple(12);
			sincron01.setAuxMemoryCurrentKey(4);
			sincron01.setAuxMemoryPreviusTuple(0);
			sincron01.setAuxMemoryPreviusKey(0);

			sincron01.setMemorySizeJoinKernel(memorySize);

			rrStartTime = System.currentTimeMillis();
			join.phaseOne4Threads(sincron01);
			rrStopTime = System.currentTimeMillis();

			tempoPhase1 += (rrStopTime - rrStartTime) / 1000;

			tempoTotal += tempoPhase1;

			tupleBD = tupleBD + "Customer|" + numtotaltuplestb + "|" + tempoPhase1 + "|";

			// /Tabela 2
			EarlyHash sincron02 = new EarlyHash();

			joinColumns = new String[] { "custkey" };
			sincron02.setJoinColumns(joinColumns);

			earlyColumns = new String[] { "orderkey" };
			sincron02.setEarlyColumns(earlyColumns);

			firstJoinTable = true;
			sincron02.setFirstJoinTable(firstJoinTable);

			firstLastTable = false;
			sincron02.setFirstLastTable(firstLastTable);

			headerJoin = "custkey[I(18)]|orderkey[I(18)]|orders[A(55)]|";
			sincron02.setHeaderJoin(headerJoin);

			selectRelation = new String[] { "o_orderdate<1995-03-15" };
			sincron02.setSelectRelation(selectRelation);

			sincron02.setTable(data + "orders.b");
			sincron02.setNumberPhase(1);
			sincron02.setNumberTable(2);

			HandleFile haf21 = new HandleFile(blockSize);
			haf21.create(data + "intTb21.b", headerJoin);
			sincron02.setHaf21(haf21);

			HandleFile haf22 = new HandleFile(blockSize);
			haf22.create(data + "intTb22.b", headerJoin);
			sincron02.setHaf22(haf22);

			HandleFile haf23 = new HandleFile(blockSize);
			haf23.create(data + "intTb23.b", headerJoin);
			sincron02.setHaf23(haf23);

			HandleFile haf24 = new HandleFile(blockSize);
			haf24.create(data + "intTb24.b", headerJoin);
			sincron02.setHaf24(haf24);

			sincron02.setHaf11(sincron01.haf11);
			sincron02.setHaf12(sincron01.haf12);
			sincron02.setHaf13(sincron01.haf13);
			sincron02.setHaf14(sincron01.haf14);

			sincron02.setAuxMemoryCurrentTuple(16);
			sincron02.setAuxMemoryCurrentKey(4);
			sincron02.setAuxMemoryPreviusTuple(12);
			sincron02.setAuxMemoryPreviusKey(4);

			sincron02.setMemorySizeJoinKernel(memorySize);

			rrStartTime = System.currentTimeMillis();
			join.phaseOne4Threads(sincron02);
			rrStopTime = System.currentTimeMillis();

			tempoPhase1 = (rrStopTime - rrStartTime) / 1000;

			escritasOverflowAcumuladasTb1 = haf11.numberofWrite + haf12.numberofWrite + haf13.numberofWrite
					+ haf14.numberofWrite;
			escritasOverflowAcumuladasTb2 = haf21.numberofWrite + haf22.numberofWrite + haf23.numberofWrite
					+ haf24.numberofWrite;

			escritasTotal += (escritasOverflowAcumuladasTb1 + escritasOverflowAcumuladasTb2);

			tempoTotal += tempoPhase1;

			tupleBD = tupleBD + "Orders|" + numtotaltuplestb + "|" + tempoPhase1 + "|";

			numtotaltuplestb = 0;

			tupleBD = tupleBD + escritasOverflowAcumuladasTb1 + "|" + escritasOverflowAcumuladasTb2 + "|" + tempoTotal
					+ "|";

			LateHash join12 = new LateHash();

			joinColumns = new String[] { "custkey" };
			join12.setJoinColumns(joinColumns);

			earlyColumnsTb1 = null;
			join12.setEarlyColumnsTb1(earlyColumnsTb1);

			earlyColumnsTb2 = new String[] { "orderkey" };
			join12.setEarlyColumnsTb1(earlyColumnsTb2);

			join12.setAtbNextJoin(new String[] { "orderkey" });

			lastJoin = false;
			join12.setLastJoin(lastJoin);

			String[] tableforLastRelation = null;
			join12.setTableforLastRelation(tableforLastRelation);

			join12.setFirstJoinTb1(true);
			join12.setFirstJoinTb2(true);

			headerJoin = "customer[A(55)]|orderkey[I(18)]|orders[A(55)]|";
			join12.setHeaderJoin(headerJoin);

			HandleFile nextHaf11 = new HandleFile(blockSize);
			nextHaf11.create(data + "Next1intTb11.b", headerJoin);
			join12.setNextHaf11(nextHaf11);

			HandleFile nextHaf12 = new HandleFile(blockSize);
			nextHaf12.create(data + "Next1intTb12.b", headerJoin);
			join12.setNextHaf12(nextHaf12);

			HandleFile nextHaf13 = new HandleFile(blockSize);
			nextHaf13.create(data + "Next1intTb13.b", headerJoin);
			join12.setNextHaf13(nextHaf13);

			HandleFile nextHaf14 = new HandleFile(blockSize);
			nextHaf14.create(data + "Next1intTb14.b", headerJoin);
			join12.setNextHaf14(nextHaf14);

			join12.setJoinTb11(data + "intTb11.b");
			join12.setJoinTb12(data + "intTb12.b");
			join12.setJoinTb13(data + "intTb13.b");
			join12.setJoinTb14(data + "intTb14.b");

			join12.setJoinTb21(data + "intTb21.b");
			join12.setJoinTb22(data + "intTb22.b");
			join12.setJoinTb23(data + "intTb23.b");
			join12.setJoinTb24(data + "intTb24.b");

			join12.setReReadDiskTb1(new String[] { "customer" });
			join12.setReReadDiskTb2(new String[] { "orders" });

			join12.setAuxMemoryKey(4);
			join12.setAuxMemoryRowid(24);
			join12.setTb1(new String[] { "customer" });

			join12.setTb2(new String[] { "orderkey", "orders" });

			rrStartTime = System.currentTimeMillis();
			join.phaseTwo4Threads(join12);

			rrStopTime = System.currentTimeMillis();

			tempoPhase2 = (rrStopTime - rrStartTime) / 1000;

			tempoTotal += tempoPhase2;

			escritasPepiLine = (join12.nextHaf11.numberofWrite + join12.nextHaf12.numberofWrite
					+ join12.nextHaf13.numberofWrite + join12.nextHaf14.numberofWrite);

			escritasTotal += escritasPepiLine;

			tupleBD = tupleBD + tempoPhase2 + "|" + "1461923" + "|" + escritasPepiLine + "|" + tempoTotal + "|";

			tb1thread11 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();
			tb1thread12 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();
			tb1thread13 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();
			tb1thread14 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();

			mapOfTable11 = new TreeMap<Integer, ArrayList<Integer>>();
			mapOfTable12 = new TreeMap<Integer, ArrayList<Integer>>();
			mapOfTable13 = new TreeMap<Integer, ArrayList<Integer>>();
			mapOfTable14 = new TreeMap<Integer, ArrayList<Integer>>();

			mapOfTable21 = new TreeMap<Integer, ArrayList<Integer>>();
			mapOfTable22 = new TreeMap<Integer, ArrayList<Integer>>();
			mapOfTable23 = new TreeMap<Integer, ArrayList<Integer>>();
			mapOfTable24 = new TreeMap<Integer, ArrayList<Integer>>();

			tb2thread11 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();
			tb2thread12 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();
			tb2thread13 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();
			tb2thread14 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();

			tb1thread11 = tb1Nextthread11;
			tb1thread12 = tb1Nextthread12;
			tb1thread13 = tb1Nextthread13;
			tb1thread14 = tb1Nextthread14;

			mapOfTable11 = mapOfNextTable11;
			mapOfTable12 = mapOfNextTable12;
			mapOfTable13 = mapOfNextTable13;
			mapOfTable14 = mapOfNextTable14;

			tb1Nextthread11 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();
			tb1Nextthread12 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();
			tb1Nextthread13 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();
			tb1Nextthread14 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();

			mapOfNextTable11 = new TreeMap<Integer, ArrayList<Integer>>();
			mapOfNextTable12 = new TreeMap<Integer, ArrayList<Integer>>();
			mapOfNextTable13 = new TreeMap<Integer, ArrayList<Integer>>();
			mapOfNextTable14 = new TreeMap<Integer, ArrayList<Integer>>();

			setZero(numbersOfKeysInInterMemory, numbersOfTuplesInInterMemory);

			EarlyHash sincron03 = new EarlyHash();

			joinColumns = new String[] { "orderkey" };
			sincron03.setJoinColumns(joinColumns);

			earlyColumns = null;
			sincron03.setEarlyColumns(earlyColumns);

			firstJoinTable = true;
			sincron03.setFirstJoinTable(firstJoinTable);

			firstLastTable = false;
			sincron03.setFirstLastTable(firstLastTable);

			headerJoin = "orderkey[I(18)]|lineitem[A(55)]|";
			sincron03.setHeaderJoin(headerJoin);

			selectRelation = new String[] { "l_shipdate>1995-03-15" };
			sincron03.setSelectRelation(selectRelation);

			sincron03.setTable(data + "lineitem.b");
			sincron03.setNumberPhase(1);
			sincron03.setNumberTable(2);

			haf21 = new HandleFile(blockSize);
			haf21.create(data + "intTb21.b", headerJoin);
			sincron03.setHaf21(haf21);

			haf22 = new HandleFile(blockSize);
			haf22.create(data + "intTb22.b", headerJoin);
			sincron03.setHaf22(haf22);

			haf23 = new HandleFile(blockSize);
			haf23.create(data + "intTb23.b", headerJoin);
			sincron03.setHaf23(haf23);

			haf24 = new HandleFile(blockSize);
			haf24.create(data + "intTb24.b", headerJoin);
			sincron03.setHaf24(haf24);

			join12.nextHaf11.close();
			join12.nextHaf12.close();
			join12.nextHaf13.close();
			join12.nextHaf14.close();

			haf11.close();
			haf12.close();
			haf13.close();
			haf14.close();

			haf11.open(data + "Next1intTb11.b");
			haf12.open(data + "Next1intTb12.b");
			haf13.open(data + "Next1intTb13.b");
			haf14.open(data + "Next1intTb14.b");

			sincron03.setHaf11(haf11);
			sincron03.setHaf12(haf12);
			sincron03.setHaf13(haf13);
			sincron03.setHaf14(haf14);

			sincron03.setAuxMemoryCurrentTuple(12);
			sincron03.setAuxMemoryCurrentKey(4);
			sincron03.setAuxMemoryPreviusTuple(24);
			sincron03.setAuxMemoryPreviusKey(4);

			sincron03.setMemorySizeJoinKernel(memorySize);

			rrStartTime = System.currentTimeMillis();
			join.phaseOne4Threads(sincron03);
			rrStopTime = System.currentTimeMillis();

			tempoPhase1 = (rrStopTime - rrStartTime) / 1000;
			escritasOverflowAcumuladasTb1 = haf11.numberofWrite + haf12.numberofWrite + haf13.numberofWrite
					+ haf14.numberofWrite;
			escritasOverflowAcumuladasTb2 = haf21.numberofWrite + haf22.numberofWrite + haf23.numberofWrite
					+ haf24.numberofWrite;

			escritasTotal += escritasOverflowAcumuladasTb1 + escritasOverflowAcumuladasTb2;

			tempoTotal += tempoPhase1;

			tupleBD = tupleBD + "Lineitem|" + numtotaltuplestb + "|" + tempoPhase1 + "|";

			tupleBD = tupleBD + escritasOverflowAcumuladasTb1 + "|" + escritasOverflowAcumuladasTb2 + "|";

			LateHash join123 = new LateHash();

			joinColumns = new String[] { "orderkey" };
			join123.setJoinColumns(joinColumns);

			earlyColumnsTb1 = null;
			join123.setEarlyColumnsTb1(earlyColumnsTb1);

			earlyColumnsTb2 = null;
			join123.setEarlyColumnsTb1(earlyColumnsTb2);

			lastJoin = true;
			join123.setLastJoin(lastJoin);

			join123.setHaf1(haf11);

			join123.setHaf2(haf21);

			join123.setHaf11(haf11);
			join123.setHaf12(haf12);

			join123.setHaf21(haf21);
			join123.setHaf22(haf22);

			join123.setJoinTb11(data + "Next1intTb11.b");
			join123.setJoinTb12(data + "Next1intTb12.b");
			join123.setJoinTb13(data + "Next1intTb13.b");
			join123.setJoinTb14(data + "Next1intTb14.b");

			join123.setJoinTb21(data + "intTb21.b");
			join123.setJoinTb22(data + "intTb22.b");
			join123.setJoinTb23(data + "intTb23.b");
			join123.setJoinTb24(data + "intTb24.b");

			tableforLastRelation = new String[] { data + "customer.b", data + "orders.b", data + "lineitem.b" };
			join123.setTableforLastRelation(tableforLastRelation);

			join123.setFirstJoinTb1(false);
			join123.setFirstJoinTb2(true);

			headerJoin = "customer[I(18)]|orders[A(55)]|lineitem[A(55)]|";
			join123.setHeaderJoin(headerJoin);

			join123.setReReadDiskTb1(new String[] { "customer", "orders" });
			join123.setReReadDiskTb2(new String[] { "lineitem" });

			rrStartTime = System.currentTimeMillis();
			join.phaseTwo4Threads(join123);

			rrStopTime = System.currentTimeMillis();
			tempoPhase2 = 0;
			tempoPhase2 = (rrStopTime - rrStartTime) / 1000;

			tempoTotal += tempoPhase2;

			tupleBD = tupleBD + tempoPhase2 + "|" + (tempoPhase1 + tempoPhase2) + "|" + escritasTotal + "|"
					+ reLeiturasAcumuladas + "|" + (numberOfTupleFinalJoin2 + numberOfTupleFinalJoin2Disk) + "|"
					+ tempoTotal;
			System.out.println(tupleBD);
			resultlog.writeLog(tupleBD);

		}
		if (teste.equals("Q10")) {
			/*
			 * select * from customer, nation, orders, lineitem where c custkey = o custkey
			 * and l orderkey = o orderkey and c nationkey = n nationkey and o orderdate >=
			 * 1993-10-01 and o orderdate < 1994-01-01 and l returnflag= �R�
			 */

			EarlyHash sincron01 = new EarlyHash();
			resultlog = new LogFile(result + "4Threads-Q10.txt");

			joinColumns = new String[] { "custkey" };
			sincron01.setJoinColumns(joinColumns);

			earlyColumns = new String[] { "nationkey" };
			sincron01.setEarlyColumns(earlyColumns);

			firstJoinTable = true;
			sincron01.setFirstJoinTable(firstJoinTable);

			firstLastTable = false;
			sincron01.setFirstLastTable(firstLastTable);

			selectRelation = null;
			sincron01.setSelectRelation(selectRelation);

			headerJoin = "custkey[I(18)]|nationkey[I(18)]|customer[A(55)]|";
			sincron01.setHeaderJoin(headerJoin);

			sincron01.setTable(data + "customer.b");
			sincron01.setNumberPhase(1);
			sincron01.setNumberTable(1);

			HandleFile haf11 = new HandleFile(blockSize);
			haf11.create(data + "intTb11.b", headerJoin);
			sincron01.setHaf11(haf11);

			HandleFile haf12 = new HandleFile(blockSize);
			haf12.create(data + "intTb12.b", headerJoin);
			sincron01.setHaf12(haf12);

			HandleFile haf13 = new HandleFile(blockSize);
			haf13.create(data + "intTb13.b", headerJoin);
			sincron01.setHaf13(haf13);

			HandleFile haf14 = new HandleFile(blockSize);
			haf14.create(data + "intTb14.b", headerJoin);
			sincron01.setHaf14(haf14);

			sincron01.setAuxMemoryCurrentTuple(16);
			sincron01.setAuxMemoryCurrentKey(4);
			sincron01.setAuxMemoryPreviusTuple(0);
			sincron01.setAuxMemoryPreviusKey(0);

			sincron01.setMemorySizeJoinKernel(memorySize);

			rrStartTime = System.currentTimeMillis();
			join.phaseOne4Threads(sincron01);
			rrStopTime = System.currentTimeMillis();

			tempoPhase1 += (rrStopTime - rrStartTime) / 1000;
			tempoTotal += tempoPhase1;

			tupleBD = tupleBD + "Customer|" + numtotaltuplestb + "|" + tempoPhase1 + "|";

			// /Tabela 2
			EarlyHash sincron02 = new EarlyHash();

			joinColumns = new String[] { "custkey" };
			sincron02.setJoinColumns(joinColumns);

			earlyColumns = new String[] { "orderkey" };
			sincron02.setEarlyColumns(earlyColumns);

			firstJoinTable = true;
			sincron02.setFirstJoinTable(firstJoinTable);

			firstLastTable = false;
			sincron02.setFirstLastTable(firstLastTable);

			headerJoin = "custkey[I(18)]|orderkey[I(18)]|orders[A(55)]|";
			sincron02.setHeaderJoin(headerJoin);

			selectRelation = new String[] { "o_orderdate>=1993-10-01 and o_orderdate<1994-01-01" };
			sincron02.setSelectRelation(selectRelation);

			sincron02.setTable(data + "orders.b");
			sincron02.setNumberPhase(1);
			sincron02.setNumberTable(2);

			HandleFile haf21 = new HandleFile(blockSize);
			haf21.create(data + "intTb21.b", headerJoin);
			sincron02.setHaf21(haf21);

			HandleFile haf22 = new HandleFile(blockSize);
			haf22.create(data + "intTb22.b", headerJoin);
			sincron02.setHaf22(haf22);

			HandleFile haf23 = new HandleFile(blockSize);
			haf23.create(data + "intTb23.b", headerJoin);
			sincron02.setHaf23(haf23);

			HandleFile haf24 = new HandleFile(blockSize);
			haf24.create(data + "intTb24.b", headerJoin);
			sincron02.setHaf24(haf24);

			sincron02.setHaf11(sincron01.haf11);
			sincron02.setHaf12(sincron01.haf12);
			sincron02.setHaf13(sincron01.haf13);
			sincron02.setHaf14(sincron01.haf14);

			sincron02.setAuxMemoryCurrentTuple(16);
			sincron02.setAuxMemoryCurrentKey(4);
			sincron02.setAuxMemoryPreviusTuple(16);
			sincron02.setAuxMemoryPreviusKey(4);

			sincron02.setMemorySizeJoinKernel(memorySize);

			rrStartTime = System.currentTimeMillis();
			join.phaseOne4Threads(sincron02);

			rrStopTime = System.currentTimeMillis();

			tempoPhase1 = (rrStopTime - rrStartTime) / 1000;

			escritasOverflowAcumuladasTb1 = haf11.numberofWrite + haf12.numberofWrite + haf13.numberofWrite
					+ haf14.numberofWrite;
			escritasOverflowAcumuladasTb2 = haf21.numberofWrite + haf22.numberofWrite + haf23.numberofWrite
					+ haf24.numberofWrite;

			escritasTotal += escritasOverflowAcumuladasTb1 + escritasOverflowAcumuladasTb2;

			tempoTotal += tempoPhase1;

			tupleBD = tupleBD + "Orders|" + numtotaltuplestb + "|" + tempoPhase1 + "|" + escritasOverflowAcumuladasTb1
					+ "|" + escritasOverflowAcumuladasTb2 + "|" + (tempoPhase1 + tempoPhase2) + "|";

			LateHash join12 = new LateHash();

			joinColumns = new String[] { "custkey" };
			join12.setJoinColumns(joinColumns);

			earlyColumnsTb1 = new String[] { "nationkey" };
			join12.setEarlyColumnsTb1(earlyColumnsTb1);

			earlyColumnsTb2 = new String[] { "orderkey" };
			join12.setEarlyColumnsTb1(earlyColumnsTb2);

			join12.setAtbNextJoin(new String[] { "orderkey" });

			lastJoin = false;
			join12.setLastJoin(lastJoin);

			String[] tableforLastRelation = null;
			join12.setTableforLastRelation(tableforLastRelation);

			join12.setFirstJoinTb1(true);
			join12.setFirstJoinTb2(true);

			headerJoin = "nationkey[I(18)]|customer[A(55)]|orderkey[I(18)]|orders[A(55)]|";
			join12.setHeaderJoin(headerJoin);

			HandleFile nextHaf11 = new HandleFile(blockSize);
			nextHaf11.create(data + "Next1intTb11.b", headerJoin);
			join12.setNextHaf11(nextHaf11);

			HandleFile nextHaf12 = new HandleFile(blockSize);
			nextHaf12.create(data + "Next1intTb12.b", headerJoin);
			join12.setNextHaf12(nextHaf12);

			HandleFile nextHaf13 = new HandleFile(blockSize);
			nextHaf13.create(data + "Next1intTb13.b", headerJoin);
			join12.setNextHaf13(nextHaf13);

			HandleFile nextHaf14 = new HandleFile(blockSize);
			nextHaf14.create(data + "Next1intTb14.b", headerJoin);
			join12.setNextHaf14(nextHaf14);

			join12.setJoinTb11(data + "intTb11.b");
			join12.setJoinTb12(data + "intTb12.b");
			join12.setJoinTb13(data + "intTb13.b");
			join12.setJoinTb14(data + "intTb14.b");

			join12.setJoinTb21(data + "intTb21.b");
			join12.setJoinTb22(data + "intTb22.b");
			join12.setJoinTb23(data + "intTb23.b");
			join12.setJoinTb24(data + "intTb24.b");

			join12.setReReadDiskTb1(new String[] { "customer" });
			join12.setReReadDiskTb2(new String[] { "orders" });

			join12.setAuxMemoryKey(4);
			join12.setAuxMemoryRowid(24);
			join12.setTb1(new String[] { "nationkey", "customer" });

			join12.setTb2(new String[] { "orderkey", "orders" });

			rrStartTime = System.currentTimeMillis();
			join.phaseTwo4Threads(join12);

			rrStopTime = System.currentTimeMillis();

			tempoPhase2 = (rrStopTime - rrStartTime) / 1000;

			tempoTotal += tempoPhase2;

			escritasPepiLine = nextHaf11.numberofWrite + nextHaf12.numberofWrite + nextHaf13.numberofWrite
					+ nextHaf13.numberofWrite;

			escritasTotal += escritasPepiLine;

			tupleBD = tupleBD + tempoPhase2 + "|" + "573157" + "|" + escritasPepiLine + "|"
					+ (tempoPhase1 + tempoPhase2) + "|";

			tb1thread11 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();
			tb1thread12 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();
			tb1thread13 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();
			tb1thread14 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();

			mapOfTable11 = new TreeMap<Integer, ArrayList<Integer>>();
			mapOfTable12 = new TreeMap<Integer, ArrayList<Integer>>();
			mapOfTable13 = new TreeMap<Integer, ArrayList<Integer>>();
			mapOfTable14 = new TreeMap<Integer, ArrayList<Integer>>();

			mapOfTable21 = new TreeMap<Integer, ArrayList<Integer>>();
			mapOfTable22 = new TreeMap<Integer, ArrayList<Integer>>();
			mapOfTable23 = new TreeMap<Integer, ArrayList<Integer>>();
			mapOfTable24 = new TreeMap<Integer, ArrayList<Integer>>();

			tb2thread11 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();
			tb2thread12 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();
			tb2thread13 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();
			tb2thread14 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();

			tb1thread11 = tb1Nextthread11;
			tb1thread12 = tb1Nextthread12;
			tb1thread13 = tb1Nextthread13;
			tb1thread14 = tb1Nextthread14;

			mapOfTable11 = mapOfNextTable11;
			mapOfTable12 = mapOfNextTable12;
			mapOfTable13 = mapOfNextTable13;
			mapOfTable14 = mapOfNextTable14;

			tb1Nextthread11 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();
			tb1Nextthread12 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();
			tb1Nextthread13 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();
			tb1Nextthread14 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();

			mapOfNextTable11 = new TreeMap<Integer, ArrayList<Integer>>();
			mapOfNextTable12 = new TreeMap<Integer, ArrayList<Integer>>();
			mapOfNextTable13 = new TreeMap<Integer, ArrayList<Integer>>();
			mapOfNextTable14 = new TreeMap<Integer, ArrayList<Integer>>();

			setZero(numbersOfKeysInInterMemory, numbersOfTuplesInInterMemory);

			EarlyHash sincron03 = new EarlyHash();

			joinColumns = new String[] { "orderkey" };
			sincron03.setJoinColumns(joinColumns);

			earlyColumns = null;
			sincron03.setEarlyColumns(earlyColumns);

			firstJoinTable = true;
			sincron03.setFirstJoinTable(firstJoinTable);

			firstLastTable = false;
			sincron03.setFirstLastTable(firstLastTable);

			headerJoin = "orderkey[I(18)]|lineitem[A(55)]|";
			sincron03.setHeaderJoin(headerJoin);

			selectRelation = new String[] { "l_returnflag=R" };
			sincron03.setSelectRelation(selectRelation);

			sincron03.setTable(data + "lineitem.b");
			sincron03.setNumberPhase(1);
			sincron03.setNumberTable(2);

			haf21 = new HandleFile(blockSize);
			haf21.create(data + "intTb21.b", headerJoin);
			sincron03.setHaf21(haf21);

			haf22 = new HandleFile(blockSize);
			haf22.create(data + "intTb22.b", headerJoin);
			sincron03.setHaf22(haf22);

			haf23 = new HandleFile(blockSize);
			haf23.create(data + "intTb23.b", headerJoin);
			sincron03.setHaf23(haf23);

			haf24 = new HandleFile(blockSize);
			haf24.create(data + "intTb24.b", headerJoin);
			sincron03.setHaf24(haf24);

			join12.nextHaf11.close();
			join12.nextHaf12.close();
			join12.nextHaf13.close();
			join12.nextHaf14.close();

			haf11.open(data + "Next1intTb11.b");
			haf12.open(data + "Next1intTb12.b");
			haf13.open(data + "Next1intTb13.b");
			haf14.open(data + "Next1intTb14.b");

			sincron03.setHaf11(haf11);
			sincron03.setHaf12(haf12);
			sincron03.setHaf13(haf13);
			sincron03.setHaf14(haf14);

			sincron03.setAuxMemoryCurrentTuple(12);
			sincron03.setAuxMemoryCurrentKey(4);
			sincron03.setAuxMemoryPreviusTuple(24);
			sincron03.setAuxMemoryPreviusKey(4);

			sincron03.setMemorySizeJoinKernel(memorySize);

			rrStartTime = System.currentTimeMillis();
			join.phaseOne4Threads(sincron03);
			rrStopTime = System.currentTimeMillis();
			tempoPhase1 = 0;
			tempoPhase1 += (rrStopTime - rrStartTime) / 1000;
			escritasOverflowAcumuladasTb1 = haf11.numberofWrite + haf12.numberofWrite + haf13.numberofWrite
					+ haf14.numberofWrite;
			escritasOverflowAcumuladasTb2 = haf21.numberofWrite + haf22.numberofWrite + haf23.numberofWrite
					+ haf24.numberofWrite;

			escritasTotal += escritasOverflowAcumuladasTb1 + escritasOverflowAcumuladasTb2;

			tempoTotal += tempoPhase1;

			tupleBD = tupleBD + "Lineitem|" + numtotaltuplestb + "|" + tempoPhase1 + "|" + escritasOverflowAcumuladasTb1
					+ "|" + escritasOverflowAcumuladasTb2 + "|";

			LateHash join123 = new LateHash();

			joinColumns = new String[] { "orderkey" };
			join123.setJoinColumns(joinColumns);

			earlyColumnsTb1 = new String[] { "nationkey" };
			join123.setEarlyColumnsTb1(earlyColumnsTb1);

			earlyColumnsTb2 = null;
			join123.setEarlyColumnsTb1(earlyColumnsTb2);

			lastJoin = false;
			join123.setLastJoin(lastJoin);

			tableforLastRelation = new String[] { data + "customer.b", data + "orders.b", data + "lineitem.b" };
			join123.setTableforLastRelation(tableforLastRelation);

			join123.setFirstJoinTb1(false);
			join123.setFirstJoinTb2(true);

			join123.setTb1(new String[] { "nationkey", "customer", "orders" });
			join123.setTb2(new String[] { "lineitem" });

			headerJoin = "nationkey[I(18)]|customer[A(55)]|orders[A(55)]|lineitem[A(55)]|";
			join123.setHeaderJoin(headerJoin);

			join123.setReReadDiskTb1(new String[] { "customer", "orders" });
			join123.setReReadDiskTb2(new String[] { "lineitem" });

			nextHaf11 = new HandleFile(blockSize);
			nextHaf11.create(data + "Next2intTb11.b", headerJoin);
			join123.setNextHaf11(nextHaf11);

			nextHaf12 = new HandleFile(blockSize);
			nextHaf12.create(data + "Next2intTb12.b", headerJoin);
			join123.setNextHaf12(nextHaf12);

			nextHaf13 = new HandleFile(blockSize);
			nextHaf13.create(data + "Next2intTb13.b", headerJoin);
			join123.setNextHaf13(nextHaf13);

			nextHaf14 = new HandleFile(blockSize);
			nextHaf14.create(data + "Next2intTb14.b", headerJoin);
			join123.setNextHaf14(nextHaf14);

			join123.setJoinTb11(data + "Next1intTb11.b");
			join123.setJoinTb12(data + "Next1intTb12.b");
			join123.setJoinTb13(data + "Next1intTb13.b");
			join123.setJoinTb14(data + "Next1intTb14.b");

			join123.setJoinTb21(data + "intTb21.b");
			join123.setJoinTb22(data + "intTb22.b");
			join123.setJoinTb23(data + "intTb23.b");
			join123.setJoinTb24(data + "intTb24.b");

			join123.setAuxMemoryKey(4);
			join123.setAuxMemoryRowid(36);

			join123.setAtbNextJoin(new String[] { "nationkey" });

			rrStartTime = System.currentTimeMillis();
			join.phaseTwo4Threads(join123);
			rrStopTime = System.currentTimeMillis();
			tempoPhase2 = 0;

			tempoPhase2 = (rrStopTime - rrStartTime) / 1000;

			tempoTotal += tempoPhase2;

			escritasPepiLine = join123.nextHaf11.numberofWrite + join123.nextHaf12.numberofWrite
					+ join123.nextHaf13.numberofWrite + join123.nextHaf14.numberofWrite;

			escritasTotal += escritasPepiLine;

			tupleBD = tupleBD + tempoPhase2 + "|" + "1147084" + "|" + escritasPepiLine + "|"
					+ (tempoPhase1 + tempoPhase2) + "|";

			tb1thread11 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();
			tb1thread12 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();
			tb1thread13 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();
			tb1thread14 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();

			mapOfTable11 = new TreeMap<Integer, ArrayList<Integer>>();
			mapOfTable12 = new TreeMap<Integer, ArrayList<Integer>>();
			mapOfTable13 = new TreeMap<Integer, ArrayList<Integer>>();
			mapOfTable14 = new TreeMap<Integer, ArrayList<Integer>>();

			mapOfTable21 = new TreeMap<Integer, ArrayList<Integer>>();
			mapOfTable22 = new TreeMap<Integer, ArrayList<Integer>>();
			mapOfTable23 = new TreeMap<Integer, ArrayList<Integer>>();
			mapOfTable24 = new TreeMap<Integer, ArrayList<Integer>>();

			tb2thread11 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();
			tb2thread12 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();
			tb2thread13 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();
			tb2thread14 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();

			tb1thread11 = tb1Nextthread11;
			tb1thread12 = tb1Nextthread12;
			tb1thread13 = tb1Nextthread13;
			tb1thread14 = tb1Nextthread14;

			mapOfTable11 = mapOfNextTable11;
			mapOfTable12 = mapOfNextTable12;
			mapOfTable13 = mapOfNextTable13;
			mapOfTable14 = mapOfNextTable14;

			tb1Nextthread11 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();
			tb1Nextthread12 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();
			tb1Nextthread13 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();
			tb1Nextthread14 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();

			mapOfNextTable11 = new TreeMap<Integer, ArrayList<Integer>>();
			mapOfNextTable12 = new TreeMap<Integer, ArrayList<Integer>>();
			mapOfNextTable13 = new TreeMap<Integer, ArrayList<Integer>>();
			mapOfNextTable14 = new TreeMap<Integer, ArrayList<Integer>>();

			setZero(numbersOfKeysInInterMemory, numbersOfTuplesInInterMemory);

			EarlyHash sincron04 = new EarlyHash();

			joinColumns = new String[] { "nationkey" };
			sincron04.setJoinColumns(joinColumns);

			earlyColumns = null;
			sincron04.setEarlyColumns(earlyColumns);

			firstJoinTable = true;
			sincron04.setFirstJoinTable(firstJoinTable);

			firstLastTable = false;
			sincron04.setFirstLastTable(firstLastTable);

			headerJoin = "nationkey[I(18)]|nation[A(55)]|";
			sincron04.setHeaderJoin(headerJoin);

			selectRelation = null;
			sincron04.setSelectRelation(selectRelation);

			sincron04.setTable(data + "nation.b");
			sincron04.setNumberPhase(1);
			sincron04.setNumberTable(2);

			haf21 = new HandleFile(blockSize);
			haf21.create(data + "intTb21.b", headerJoin);
			sincron04.setHaf21(haf21);

			haf22 = new HandleFile(blockSize);
			haf22.create(data + "intTb22.b", headerJoin);
			sincron04.setHaf22(haf22);

			haf23 = new HandleFile(blockSize);
			haf23.create(data + "intTb23.b", headerJoin);
			sincron04.setHaf23(haf23);

			haf24 = new HandleFile(blockSize);
			haf24.create(data + "intTb24.b", headerJoin);
			sincron04.setHaf24(haf24);

			haf11.open(data + "Next2intTb11.b");
			haf12.open(data + "Next2intTb12.b");
			haf13.open(data + "Next2intTb13.b");
			haf14.open(data + "Next2intTb14.b");

			sincron04.setHaf11(haf11);
			sincron04.setHaf12(haf12);
			sincron04.setHaf13(haf13);
			sincron04.setHaf14(haf14);

			sincron04.setMemorySizeJoinKernel(memorySize);

			sincron04.setAuxMemoryCurrentTuple(12);
			sincron04.setAuxMemoryCurrentKey(4);
			sincron04.setAuxMemoryPreviusTuple(36);
			sincron04.setAuxMemoryPreviusKey(4);

			rrStartTime = System.currentTimeMillis();
			join.phaseOne4Threads(sincron04);
			rrStopTime = System.currentTimeMillis();
			tempoPhase1 = 0;
			tempoPhase1 += (rrStopTime - rrStartTime) / 1000;
			escritasOverflowAcumuladasTb1 = haf11.numberofWrite + haf12.numberofWrite + haf13.numberofWrite
					+ haf14.numberofWrite;
			escritasOverflowAcumuladasTb2 = haf21.numberofWrite + haf22.numberofWrite + haf23.numberofWrite
					+ haf24.numberofWrite;

			escritasTotal += escritasOverflowAcumuladasTb1 + escritasOverflowAcumuladasTb2;

			tempoTotal += tempoPhase1;
			tupleBD = tupleBD + "Nation|" + numtotaltuplestb + "|" + tempoPhase1 + "|" + escritasOverflowAcumuladasTb1
					+ "|" + escritasOverflowAcumuladasTb2 + "|";

			LateHash join1234 = new LateHash();

			joinColumns = new String[] { "nationkey" };
			join1234.setJoinColumns(joinColumns);

			earlyColumnsTb1 = null;
			join1234.setEarlyColumnsTb1(earlyColumnsTb1);

			earlyColumnsTb2 = null;
			join1234.setEarlyColumnsTb1(earlyColumnsTb2);

			lastJoin = true;
			join1234.setLastJoin(lastJoin);

			join1234.setJoinTb11(data + "Next2intTb11.b");
			join1234.setJoinTb12(data + "Next2intTb12.b");
			join1234.setJoinTb13(data + "Next2intTb13.b");
			join1234.setJoinTb14(data + "Next2intTb14.b");

			join1234.setJoinTb21(data + "intTb21.b");
			join1234.setJoinTb22(data + "intTb22.b");
			join1234.setJoinTb23(data + "intTb23.b");
			join1234.setJoinTb24(data + "intTb24.b");

			tableforLastRelation = new String[] { data + "customer.b", data + "orders.b", data + "lineitem.b",
					data + "nation.b" };
			join1234.setTableforLastRelation(tableforLastRelation);

			join1234.setFirstJoinTb1(false);
			join1234.setFirstJoinTb2(true);

			join1234.setTb1(new String[] { "customer", "orders", "lineitem" });
			join1234.setTb2(new String[] { "nation" });

			headerJoin = "customer[A(55)]|orders[A(55)]|lineitem[A(55)]|nation[A(18)]|";
			join1234.setHeaderJoin(headerJoin);

			join1234.setReReadDiskTb1(new String[] { "customer", "orders", "lineitem" });
			join1234.setReReadDiskTb2(new String[] { "nation" });

			rrStartTime = System.currentTimeMillis();
			join.phaseTwo4Threads(join1234);

			rrStopTime = System.currentTimeMillis();
			tempoPhase2 = (rrStopTime - rrStartTime) / 1000;

			tempoTotal += tempoPhase2;

			tupleBD = tupleBD + tempoPhase2 + "|" + (tempoPhase1 + tempoPhase2) + "|" + escritasTotal + "|"
					+ reLeiturasAcumuladas + "|" + (numberOfTupleFinalJoin2 + numberOfTupleFinalJoin2Disk) + "|"
					+ tempoTotal;
			System.out.println(tupleBD);
			resultlog.writeLog(tupleBD);
		}

		if (teste.equals("Q5")) {
			/*
			 * select * from customer, orders, lineitem, supplier, nation, region where l
			 * orderkey = o orderkey and c custkey = o custkey and l suppkey = s suppkey and
			 * c nationkey = n nationkey and n regionkey = r regionkey and r name = �ASIA�
			 * and o orderdate >= 1994-01-01 and o orderdate < 1995-01-01
			 */

			EarlyHash sincron01 = new EarlyHash();
			resultlog = new LogFile(result + "4Threads-Q5.txt");

			joinColumns = new String[] { "regionkey" };
			sincron01.setJoinColumns(joinColumns);

			earlyColumns = new String[] { "nationkey" };
			sincron01.setEarlyColumns(earlyColumns);

			firstJoinTable = true;
			sincron01.setFirstJoinTable(firstJoinTable);

			firstLastTable = false;
			sincron01.setFirstLastTable(firstLastTable);

			selectRelation = null;
			sincron01.setSelectRelation(selectRelation);

			headerJoin = "regionkey[I(18)]|nationkey[I(18)]|nation[A(55)]|";
			sincron01.setHeaderJoin(headerJoin);

			sincron01.setTable(data + "nation.b");
			sincron01.setNumberPhase(1);
			sincron01.setNumberTable(1);

			sincron01.setMemorySizeJoinKernel(memorySize);

			HandleFile haf11 = new HandleFile(blockSize);
			haf11.create(data + "intTb11.b", headerJoin);
			sincron01.setHaf11(haf11);

			HandleFile haf12 = new HandleFile(blockSize);
			haf12.create(data + "intTb12.b", headerJoin);
			sincron01.setHaf12(haf12);

			HandleFile haf13 = new HandleFile(blockSize);
			haf13.create(data + "intTb13.b", headerJoin);
			sincron01.setHaf13(haf13);

			HandleFile haf14 = new HandleFile(blockSize);
			haf14.create(data + "intTb14.b", headerJoin);
			sincron01.setHaf14(haf14);

			sincron01.setAuxMemoryCurrentTuple(16);
			sincron01.setAuxMemoryCurrentKey(4);
			sincron01.setAuxMemoryPreviusTuple(0);
			sincron01.setAuxMemoryPreviusKey(0);

			rrStartTime = System.currentTimeMillis();
			join.phaseOne4Threads(sincron01);
			rrStopTime = System.currentTimeMillis();

			tempoPhase1 = (rrStopTime - rrStartTime) / 1000;

			tupleBD = tupleBD + "Nation|" + numtotaltuplestb + "|" + tempoPhase1 + "|";

			// /Tabela 2
			EarlyHash sincron02 = new EarlyHash();

			joinColumns = new String[] { "regionkey" };
			sincron02.setJoinColumns(joinColumns);

			earlyColumns = null;
			sincron02.setEarlyColumns(earlyColumns);

			firstJoinTable = true;
			sincron02.setFirstJoinTable(firstJoinTable);

			firstLastTable = false;
			sincron02.setFirstLastTable(firstLastTable);

			headerJoin = "regionkey[I(18)]|region[A(55)]|";
			sincron02.setHeaderJoin(headerJoin);

			selectRelation = new String[] { "r_name=ASIA" };
			sincron02.setSelectRelation(selectRelation);

			sincron02.setTable(data + "region.b");
			sincron02.setNumberPhase(1);
			sincron02.setNumberTable(2);

			HandleFile haf21 = new HandleFile(blockSize);
			haf21.create(data + "intTb21.b", headerJoin);
			sincron02.setHaf21(haf21);

			HandleFile haf22 = new HandleFile(blockSize);
			haf22.create(data + "intTb22.b", headerJoin);
			sincron02.setHaf22(haf22);

			HandleFile haf23 = new HandleFile(blockSize);
			haf23.create(data + "intTb23.b", headerJoin);
			sincron02.setHaf23(haf23);

			HandleFile haf24 = new HandleFile(blockSize);
			haf24.create(data + "intTb24.b", headerJoin);
			sincron02.setHaf24(haf24);

			sincron02.setMemorySizeJoinKernel(memorySize);
			sincron02.setHaf11(sincron01.haf11);
			sincron02.setHaf12(sincron01.haf12);
			sincron02.setHaf13(sincron01.haf13);
			sincron02.setHaf14(sincron01.haf14);

			sincron02.setAuxMemoryCurrentTuple(12);
			sincron02.setAuxMemoryCurrentKey(4);
			sincron02.setAuxMemoryPreviusTuple(16);
			sincron02.setAuxMemoryPreviusKey(4);

			rrStartTime = System.currentTimeMillis();
			join.phaseOne4Threads(sincron02);
			rrStopTime = System.currentTimeMillis();

			tempoPhase1 = (rrStopTime - rrStartTime) / 1000;

			tupleBD = tupleBD + "Region|" + numtotaltuplestb + "|" + tempoPhase1 + "|";

			tempoTotal += tempoPhase1;

			escritasOverflowAcumuladasTb1 = haf11.numberofWrite + haf12.numberofWrite + haf13.numberofWrite
					+ haf14.numberofWrite;
			escritasOverflowAcumuladasTb2 = haf21.numberofWrite + haf22.numberofWrite + haf23.numberofWrite
					+ haf24.numberofWrite;

			escritasTotal = escritasOverflowAcumuladasTb1 + escritasOverflowAcumuladasTb2;

			tupleBD = tupleBD + escritasOverflowAcumuladasTb1 + "|" + escritasOverflowAcumuladasTb2 + "|" + tempoTotal
					+ "|";

			LateHash join12 = new LateHash();

			joinColumns = new String[] { "regionkey" };
			join12.setJoinColumns(joinColumns);

			earlyColumnsTb1 = new String[] { "nationkey" };
			join12.setEarlyColumnsTb1(earlyColumnsTb1);

			earlyColumnsTb2 = null;
			join12.setEarlyColumnsTb1(earlyColumnsTb2);

			join12.setAtbNextJoin(new String[] { "nationkey" });

			lastJoin = false;
			join12.setLastJoin(lastJoin);

			String[] tableforLastRelation = null;
			join12.setTableforLastRelation(tableforLastRelation);

			join12.setFirstJoinTb1(true);
			join12.setFirstJoinTb2(true);

			headerJoin = "nationkey[I(18)]|nation[A(55)]|region[A(55)]|";
			join12.setHeaderJoin(headerJoin);

			HandleFile nextHaf11 = new HandleFile(blockSize);
			nextHaf11.create(data + "Next1intTb11.b", headerJoin);
			join12.setNextHaf11(nextHaf11);

			HandleFile nextHaf12 = new HandleFile(blockSize);
			nextHaf12.create(data + "Next1intTb12.b", headerJoin);
			join12.setNextHaf12(nextHaf12);

			HandleFile nextHaf13 = new HandleFile(blockSize);
			nextHaf13.create(data + "Next1intTb13.b", headerJoin);
			join12.setNextHaf13(nextHaf13);

			HandleFile nextHaf14 = new HandleFile(blockSize);
			nextHaf14.create(data + "Next1intTb14.b", headerJoin);
			join12.setNextHaf14(nextHaf14);

			join12.setJoinTb11(data + "intTb11.b");
			join12.setJoinTb12(data + "intTb12.b");
			join12.setJoinTb13(data + "intTb13.b");
			join12.setJoinTb14(data + "intTb14.b");

			join12.setJoinTb21(data + "intTb21.b");
			join12.setJoinTb22(data + "intTb22.b");
			join12.setJoinTb23(data + "intTb23.b");
			join12.setJoinTb24(data + "intTb24.b");

			join12.setReReadDiskTb1(new String[] { "nation" });
			join12.setReReadDiskTb2(new String[] { "region" });

			join12.setAuxMemoryKey(4);
			join12.setAuxMemoryRowid(24);
			join12.setTb1(new String[] { "nationkey", "nation" });

			join12.setTb2(new String[] { "region" });

			rrStartTime = System.currentTimeMillis();
			join.phaseTwo4Threads(join12);
			rrStopTime = System.currentTimeMillis();

			tempoPhase2 = (rrStopTime - rrStartTime) / 1000;

			tempoTotal += tempoPhase2;

			escritasPepiLine = (join12.nextHaf11.numberofWrite + join12.nextHaf12.numberofWrite
					+ join12.nextHaf13.numberofWrite + join12.nextHaf14.numberofWrite);

			escritasTotal += escritasPepiLine;

			tupleBD = tupleBD + tempoPhase2 + "|" + "5" + "|" + escritasPepiLine + "|" + (tempoPhase1 + tempoPhase2)
					+ "|";

			tb1thread11 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();
			tb1thread12 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();
			tb1thread13 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();
			tb1thread14 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();

			mapOfTable11 = new TreeMap<Integer, ArrayList<Integer>>();
			mapOfTable12 = new TreeMap<Integer, ArrayList<Integer>>();
			mapOfTable13 = new TreeMap<Integer, ArrayList<Integer>>();
			mapOfTable14 = new TreeMap<Integer, ArrayList<Integer>>();

			mapOfTable21 = new TreeMap<Integer, ArrayList<Integer>>();
			mapOfTable22 = new TreeMap<Integer, ArrayList<Integer>>();
			mapOfTable23 = new TreeMap<Integer, ArrayList<Integer>>();
			mapOfTable24 = new TreeMap<Integer, ArrayList<Integer>>();

			tb2thread11 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();
			tb2thread12 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();
			tb2thread13 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();
			tb2thread14 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();

			tb1thread11 = tb1Nextthread11;
			tb1thread12 = tb1Nextthread12;
			tb1thread13 = tb1Nextthread13;
			tb1thread14 = tb1Nextthread14;

			mapOfTable11 = mapOfNextTable11;
			mapOfTable12 = mapOfNextTable12;
			mapOfTable13 = mapOfNextTable13;
			mapOfTable14 = mapOfNextTable14;

			tb1Nextthread11 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();
			tb1Nextthread12 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();
			tb1Nextthread13 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();
			tb1Nextthread14 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();

			mapOfNextTable11 = new TreeMap<Integer, ArrayList<Integer>>();
			mapOfNextTable12 = new TreeMap<Integer, ArrayList<Integer>>();
			mapOfNextTable13 = new TreeMap<Integer, ArrayList<Integer>>();
			mapOfNextTable14 = new TreeMap<Integer, ArrayList<Integer>>();

			setZero(numbersOfKeysInInterMemory, numbersOfTuplesInInterMemory);

			EarlyHash sincron03 = new EarlyHash();

			joinColumns = new String[] { "nationkey" };
			sincron03.setJoinColumns(joinColumns);

			earlyColumns = new String[] { "custkey" };
			sincron03.setEarlyColumns(earlyColumns);

			firstJoinTable = true;
			sincron03.setFirstJoinTable(firstJoinTable);

			firstLastTable = false;
			sincron03.setFirstLastTable(firstLastTable);

			headerJoin = "nationkey[I(18)]|custkey[I(18)]|customer[A(55)]|";
			sincron03.setHeaderJoin(headerJoin);

			selectRelation = null;
			sincron03.setSelectRelation(selectRelation);

			sincron03.setMemorySizeJoinKernel(memorySize);

			sincron03.setTable(data + "customer.b");
			sincron03.setNumberPhase(1);
			sincron03.setNumberTable(2);

			haf21 = new HandleFile(blockSize);
			haf21.create(data + "intTb21.b", headerJoin);
			sincron03.setHaf21(haf21);

			haf22 = new HandleFile(blockSize);
			haf22.create(data + "intTb22.b", headerJoin);
			sincron03.setHaf22(haf22);

			haf23 = new HandleFile(blockSize);
			haf23.create(data + "intTb23.b", headerJoin);
			sincron03.setHaf23(haf23);

			haf24 = new HandleFile(blockSize);
			haf24.create(data + "intTb24.b", headerJoin);
			sincron03.setHaf24(haf24);

			join12.nextHaf11.close();
			join12.nextHaf12.close();
			join12.nextHaf13.close();
			join12.nextHaf14.close();

			haf11.open(data + "Next1intTb11.b");
			haf12.open(data + "Next1intTb12.b");
			haf13.open(data + "Next1intTb13.b");
			haf14.open(data + "Next1intTb14.b");

			sincron03.setHaf11(haf11);
			sincron03.setHaf12(haf12);
			sincron03.setHaf13(haf13);
			sincron03.setHaf14(haf14);

			sincron03.setAuxMemoryCurrentTuple(16);
			sincron03.setAuxMemoryCurrentKey(4);
			sincron03.setAuxMemoryPreviusTuple(24);
			sincron03.setAuxMemoryPreviusKey(4);

			rrStartTime = System.currentTimeMillis();
			join.phaseOne4Threads(sincron03);
			rrStopTime = System.currentTimeMillis();

			tempoPhase1 = (rrStopTime - rrStartTime) / 1000;

			escritasOverflowAcumuladasTb1 = haf11.numberofWrite + haf12.numberofWrite + haf13.numberofWrite
					+ haf14.numberofWrite;
			escritasOverflowAcumuladasTb2 = haf21.numberofWrite + haf22.numberofWrite + haf23.numberofWrite
					+ haf24.numberofWrite;

			escritasTotal += escritasOverflowAcumuladasTb1 + escritasOverflowAcumuladasTb2;

			tempoTotal += tempoPhase1;

			tupleBD = tupleBD + "Customer|" + numtotaltuplestb + "|" + tempoPhase1 + "|";

			tupleBD = tupleBD + escritasOverflowAcumuladasTb1 + "|" + escritasOverflowAcumuladasTb2 + "|";

			LateHash join123 = new LateHash();

			joinColumns = new String[] { "nationkey" };
			join123.setJoinColumns(joinColumns);

			earlyColumnsTb1 = null;
			join123.setEarlyColumnsTb1(earlyColumnsTb1);

			earlyColumnsTb2 = new String[] { "custkey" };
			join123.setEarlyColumnsTb1(earlyColumnsTb2);

			lastJoin = false;
			join123.setLastJoin(lastJoin);

			tableforLastRelation = new String[] { data + "nation.b", data + "region.b", data + "customer.b" };
			join123.setTableforLastRelation(tableforLastRelation);

			join123.setFirstJoinTb1(false);
			join123.setFirstJoinTb2(true);

			join123.setTb1(new String[] { "nation", "region" });
			join123.setTb2(new String[] { "custkey", "customer" });

			headerJoin = "nation[A(18)]|region[A(55)]|custkey[I(18)]|customer[A(55)]|";
			join123.setHeaderJoin(headerJoin);

			join123.setReReadDiskTb1(new String[] { "nation", "region" });
			join123.setReReadDiskTb2(new String[] { "customer" });

			nextHaf11 = new HandleFile(blockSize);
			nextHaf11.create(data + "Next2intTb11.b", headerJoin);
			join123.setNextHaf11(nextHaf11);

			nextHaf12 = new HandleFile(blockSize);
			nextHaf12.create(data + "Next2intTb12.b", headerJoin);
			join123.setNextHaf12(nextHaf12);

			nextHaf13 = new HandleFile(blockSize);
			nextHaf13.create(data + "Next2intTb13.b", headerJoin);
			join123.setNextHaf13(nextHaf13);

			nextHaf14 = new HandleFile(blockSize);
			nextHaf14.create(data + "Next2intTb14.b", headerJoin);
			join123.setNextHaf14(nextHaf14);

			join123.setJoinTb11(data + "Next1intTb11.b");
			join123.setJoinTb12(data + "Next1intTb12.b");
			join123.setJoinTb13(data + "Next1intTb13.b");
			join123.setJoinTb14(data + "Next1intTb14.b");

			join123.setJoinTb21(data + "intTb21.b");
			join123.setJoinTb22(data + "intTb22.b");
			join123.setJoinTb23(data + "intTb23.b");
			join123.setJoinTb24(data + "intTb24.b");

			join123.setAuxMemoryKey(4);
			join123.setAuxMemoryRowid(36);

			join123.setAtbNextJoin(new String[] { "custkey" });

			rrStartTime = System.currentTimeMillis();
			join.phaseTwo4Threads(join123);

			rrStopTime = System.currentTimeMillis();

			tempoPhase2 = (rrStopTime - rrStartTime) / 1000;

			tempoTotal += tempoPhase2;

			escritasPepiLine = (join123.nextHaf11.numberofWrite + join123.nextHaf12.numberofWrite
					+ join123.nextHaf13.numberofWrite + join123.nextHaf14.numberofWrite);

			escritasTotal += escritasPepiLine;

			tupleBD = tupleBD + tempoPhase2 + "|" + "300270" + "|" + escritasPepiLine + "|"
					+ (tempoPhase1 + tempoPhase2) + "|";

			tb1thread11 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();
			tb1thread12 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();
			tb1thread13 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();
			tb1thread14 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();

			mapOfTable11 = new TreeMap<Integer, ArrayList<Integer>>();
			mapOfTable12 = new TreeMap<Integer, ArrayList<Integer>>();
			mapOfTable13 = new TreeMap<Integer, ArrayList<Integer>>();
			mapOfTable14 = new TreeMap<Integer, ArrayList<Integer>>();

			mapOfTable21 = new TreeMap<Integer, ArrayList<Integer>>();
			mapOfTable22 = new TreeMap<Integer, ArrayList<Integer>>();
			mapOfTable23 = new TreeMap<Integer, ArrayList<Integer>>();
			mapOfTable24 = new TreeMap<Integer, ArrayList<Integer>>();

			tb2thread11 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();
			tb2thread12 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();
			tb2thread13 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();
			tb2thread14 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();

			tb1thread11 = tb1Nextthread11;
			tb1thread12 = tb1Nextthread12;
			tb1thread13 = tb1Nextthread13;
			tb1thread14 = tb1Nextthread14;

			mapOfTable11 = mapOfNextTable11;
			mapOfTable12 = mapOfNextTable12;
			mapOfTable13 = mapOfNextTable13;
			mapOfTable14 = mapOfNextTable14;

			tb1Nextthread11 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();
			tb1Nextthread12 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();
			tb1Nextthread13 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();
			tb1Nextthread14 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();

			mapOfNextTable11 = new TreeMap<Integer, ArrayList<Integer>>();
			mapOfNextTable12 = new TreeMap<Integer, ArrayList<Integer>>();
			mapOfNextTable13 = new TreeMap<Integer, ArrayList<Integer>>();
			mapOfNextTable14 = new TreeMap<Integer, ArrayList<Integer>>();

			setZero(numbersOfKeysInInterMemory, numbersOfTuplesInInterMemory);

			EarlyHash sincron04 = new EarlyHash();

			joinColumns = new String[] { "custkey" };
			sincron04.setJoinColumns(joinColumns);

			earlyColumns = new String[] { "orderkey" };
			sincron04.setEarlyColumns(earlyColumns);

			firstJoinTable = true;
			sincron04.setFirstJoinTable(firstJoinTable);

			firstLastTable = false;
			sincron04.setFirstLastTable(firstLastTable);

			headerJoin = "custkey[I(18)]|orderkey[I(18)]|orders[A(55)]|";
			sincron04.setHeaderJoin(headerJoin);

			selectRelation = new String[] { "o_orderdate>=1994-01-01 and o_orderdate<1995-01-01" };
			sincron04.setSelectRelation(selectRelation);

			sincron04.setTable(data + "orders.b");
			sincron04.setNumberPhase(1);
			sincron04.setNumberTable(2);

			haf21 = new HandleFile(blockSize);
			haf21.create(data + "intTb21.b", headerJoin);
			sincron04.setHaf21(haf21);

			haf22 = new HandleFile(blockSize);
			haf22.create(data + "intTb22.b", headerJoin);
			sincron04.setHaf22(haf22);

			haf23 = new HandleFile(blockSize);
			haf23.create(data + "intTb23.b", headerJoin);
			sincron04.setHaf23(haf23);

			haf24 = new HandleFile(blockSize);
			haf24.create(data + "intTb24.b", headerJoin);
			sincron04.setHaf24(haf24);

			join12.nextHaf11.close();
			join12.nextHaf12.close();
			join12.nextHaf13.close();
			join12.nextHaf14.close();

			haf11.open(data + "Next2intTb11.b");
			haf12.open(data + "Next2intTb12.b");
			haf13.open(data + "Next2intTb13.b");
			haf14.open(data + "Next2intTb14.b");

			sincron04.setHaf11(haf11);
			sincron04.setHaf12(haf12);
			sincron04.setHaf13(haf13);
			sincron04.setHaf14(haf14);

			sincron04.setMemorySizeJoinKernel(memorySize);

			sincron04.setAuxMemoryCurrentTuple(16);
			sincron04.setAuxMemoryCurrentKey(4);
			sincron04.setAuxMemoryPreviusTuple(36);
			sincron04.setAuxMemoryPreviusKey(4);

			rrStartTime = System.currentTimeMillis();
			join.phaseOne4Threads(sincron04);
			rrStopTime = System.currentTimeMillis();

			tempoPhase1 = (rrStopTime - rrStartTime) / 1000;
			escritasOverflowAcumuladasTb1 = haf11.numberofWrite + haf12.numberofWrite + haf13.numberofWrite
					+ haf14.numberofWrite;
			escritasOverflowAcumuladasTb2 = haf21.numberofWrite + haf22.numberofWrite + haf23.numberofWrite
					+ haf24.numberofWrite;

			escritasTotal += escritasOverflowAcumuladasTb1 + escritasOverflowAcumuladasTb2;

			tempoTotal += tempoPhase1;

			tupleBD = tupleBD + "Orders|" + numtotaltuplestb + "|" + tempoPhase1 + "|";

			tupleBD = tupleBD + escritasOverflowAcumuladasTb1 + "|" + escritasOverflowAcumuladasTb2 + "|";

			LateHash join1234 = new LateHash();

			joinColumns = new String[] { "custkey" };
			join1234.setJoinColumns(joinColumns);

			earlyColumnsTb1 = null;
			join1234.setEarlyColumnsTb1(earlyColumnsTb1);

			earlyColumnsTb2 = new String[] { "orderkey" };
			join1234.setEarlyColumnsTb1(earlyColumnsTb2);

			lastJoin = false;
			join1234.setLastJoin(lastJoin);

			tableforLastRelation = new String[] { data + "nation.b", data + "region.b", data + "customer.b",
					data + "orders.b" };
			join1234.setTableforLastRelation(tableforLastRelation);

			join1234.setFirstJoinTb1(false);
			join1234.setFirstJoinTb2(true);

			join1234.setTb1(new String[] { "nation", "region", "customer" });
			join1234.setTb2(new String[] { "orderkey", "orders" });

			headerJoin = "nation[A(18)]|region[A(55)]|customer[A(55)]|orderkey[I(18)]|orders[A(55)]|";
			join1234.setHeaderJoin(headerJoin);

			join1234.setReReadDiskTb1(new String[] { "nation", "region", "customer" });
			join1234.setReReadDiskTb2(new String[] { "orders" });

			nextHaf11 = new HandleFile(blockSize);
			nextHaf11.create(data + "Next3intTb11.b", headerJoin);
			join1234.setNextHaf11(nextHaf11);

			nextHaf12 = new HandleFile(blockSize);
			nextHaf12.create(data + "Next3intTb12.b", headerJoin);
			join1234.setNextHaf12(nextHaf12);

			nextHaf13 = new HandleFile(blockSize);
			nextHaf13.create(data + "Next3intTb13.b", headerJoin);
			join1234.setNextHaf13(nextHaf13);

			nextHaf14 = new HandleFile(blockSize);
			nextHaf14.create(data + "Next3intTb14.b", headerJoin);
			join1234.setNextHaf14(nextHaf14);

			join1234.setJoinTb11(data + "Next2intTb11.b");
			join1234.setJoinTb12(data + "Next2intTb12.b");
			join1234.setJoinTb13(data + "Next2intTb13.b");
			join1234.setJoinTb14(data + "Next2intTb14.b");

			join1234.setJoinTb21(data + "intTb21.b");
			join1234.setJoinTb22(data + "intTb22.b");
			join1234.setJoinTb23(data + "intTb23.b");
			join1234.setJoinTb24(data + "intTb24.b");

			join1234.setAuxMemoryKey(4);
			join1234.setAuxMemoryRowid(48);

			join1234.setAtbNextJoin(new String[] { "orderkey" });

			rrStartTime = System.currentTimeMillis();
			join.phaseTwo4Threads(join1234);

			rrStopTime = System.currentTimeMillis();

			tempoPhase2 = (rrStopTime - rrStartTime) / 1000;

			tempoTotal += tempoPhase2;

			escritasPepiLine = (join1234.nextHaf11.numberofWrite + join1234.nextHaf12.numberofWrite
					+ join1234.nextHaf13.numberofWrite + join1234.nextHaf14.numberofWrite);

			escritasTotal += escritasPepiLine;

			tupleBD = tupleBD + tempoPhase2 + "|" + "456771" + "|" + escritasPepiLine + "|"
					+ (tempoPhase1 + tempoPhase2) + "|";

			tb1thread11 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();
			tb1thread12 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();
			tb1thread13 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();
			tb1thread14 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();

			mapOfTable11 = new TreeMap<Integer, ArrayList<Integer>>();
			mapOfTable12 = new TreeMap<Integer, ArrayList<Integer>>();
			mapOfTable13 = new TreeMap<Integer, ArrayList<Integer>>();
			mapOfTable14 = new TreeMap<Integer, ArrayList<Integer>>();

			mapOfTable21 = new TreeMap<Integer, ArrayList<Integer>>();
			mapOfTable22 = new TreeMap<Integer, ArrayList<Integer>>();
			mapOfTable23 = new TreeMap<Integer, ArrayList<Integer>>();
			mapOfTable24 = new TreeMap<Integer, ArrayList<Integer>>();

			tb2thread11 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();
			tb2thread12 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();
			tb2thread13 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();
			tb2thread14 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();

			tb1thread11 = tb1Nextthread11;
			tb1thread12 = tb1Nextthread12;
			tb1thread13 = tb1Nextthread13;
			tb1thread14 = tb1Nextthread14;

			mapOfTable11 = mapOfNextTable11;
			mapOfTable12 = mapOfNextTable12;
			mapOfTable13 = mapOfNextTable13;
			mapOfTable14 = mapOfNextTable14;

			tb1Nextthread11 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();
			tb1Nextthread12 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();
			tb1Nextthread13 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();
			tb1Nextthread14 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();

			mapOfNextTable11 = new TreeMap<Integer, ArrayList<Integer>>();
			mapOfNextTable12 = new TreeMap<Integer, ArrayList<Integer>>();
			mapOfNextTable13 = new TreeMap<Integer, ArrayList<Integer>>();
			mapOfNextTable14 = new TreeMap<Integer, ArrayList<Integer>>();

			setZero(numbersOfKeysInInterMemory, numbersOfTuplesInInterMemory);

			EarlyHash sincron05 = new EarlyHash();

			joinColumns = new String[] { "orderkey" };
			sincron05.setJoinColumns(joinColumns);

			earlyColumns = new String[] { "suppkey" };
			sincron05.setEarlyColumns(earlyColumns);

			firstJoinTable = true;
			sincron05.setFirstJoinTable(firstJoinTable);

			firstLastTable = false;
			sincron05.setFirstLastTable(firstLastTable);

			headerJoin = "orderkey[I(18)]|suppkey[I(18)]|lineitem[A(55)]|";
			sincron05.setHeaderJoin(headerJoin);

			selectRelation = null;
			sincron05.setSelectRelation(selectRelation);

			sincron05.setTable(data + "lineitem.b");
			sincron05.setNumberPhase(1);
			sincron05.setNumberTable(2);

			haf21 = new HandleFile(blockSize);
			haf21.create(data + "intTb21.b", headerJoin);
			sincron05.setHaf21(haf21);

			haf22 = new HandleFile(blockSize);
			haf22.create(data + "intTb22.b", headerJoin);
			sincron05.setHaf22(haf22);

			haf23 = new HandleFile(blockSize);
			haf23.create(data + "intTb23.b", headerJoin);
			sincron05.setHaf23(haf23);

			haf24 = new HandleFile(blockSize);
			haf24.create(data + "intTb24.b", headerJoin);
			sincron05.setHaf24(haf24);

			sincron05.setHaf11(sincron04.haf11);
			sincron05.setHaf12(sincron04.haf12);
			sincron05.setHaf13(sincron04.haf13);
			sincron05.setHaf14(sincron04.haf14);

			sincron05.setMemorySizeJoinKernel(memorySize);

			sincron05.setAuxMemoryCurrentTuple(16);
			sincron05.setAuxMemoryCurrentKey(4);
			sincron05.setAuxMemoryPreviusTuple(48);
			sincron05.setAuxMemoryPreviusKey(4);

			rrStartTime = System.currentTimeMillis();
			join.phaseOne4Threads(sincron05);

			rrStopTime = System.currentTimeMillis();

			tempoPhase1 = (rrStopTime - rrStartTime) / 1000;

			escritasOverflowAcumuladasTb1 = haf11.numberofWrite + haf12.numberofWrite + haf13.numberofWrite
					+ haf14.numberofWrite;
			escritasOverflowAcumuladasTb2 = haf21.numberofWrite + haf22.numberofWrite + haf23.numberofWrite
					+ haf24.numberofWrite;

			escritasTotal += escritasOverflowAcumuladasTb1 + escritasOverflowAcumuladasTb2;

			tempoTotal += tempoPhase1;

			tupleBD = tupleBD + "Lineitem|" + numtotaltuplestb + "|" + tempoPhase1 + "|";

			tupleBD = tupleBD + escritasOverflowAcumuladasTb1 + "|" + escritasOverflowAcumuladasTb2 + "|";

			LateHash join12345 = new LateHash();

			joinColumns = new String[] { "orderkey" };
			join12345.setJoinColumns(joinColumns);

			earlyColumnsTb1 = null;
			join12345.setEarlyColumnsTb1(earlyColumnsTb1);

			earlyColumnsTb2 = new String[] { "suppkey" };
			join12345.setEarlyColumnsTb1(earlyColumnsTb2);

			lastJoin = false;
			join12345.setLastJoin(lastJoin);

			join12345.setHaf1(haf11);

			join12345.setHaf2(haf21);

			headerJoin = "nation[A(18)]|region[A(55)]|customer[A(55)]|orders[A(55)]|suppkey[I(18)]|lineitem[A(55)]|";
			join12345.setHeaderJoin(headerJoin);

			nextHaf11 = new HandleFile(blockSize);
			nextHaf11.create(data + "Next4intTb11.b", headerJoin);
			join12345.setNextHaf11(nextHaf11);

			nextHaf12 = new HandleFile(blockSize);
			nextHaf12.create(data + "Next4intTb12.b", headerJoin);
			join12345.setNextHaf12(nextHaf12);

			nextHaf13 = new HandleFile(blockSize);
			nextHaf13.create(data + "Next4intTb13.b", headerJoin);
			join12345.setNextHaf13(nextHaf13);

			nextHaf14 = new HandleFile(blockSize);
			nextHaf14.create(data + "Next4intTb14.b", headerJoin);
			join12345.setNextHaf14(nextHaf14);

			join12345.setJoinTb11(data + "Next3intTb11.b");
			join12345.setJoinTb12(data + "Next3intTb12.b");
			join12345.setJoinTb13(data + "Next3intTb13.b");
			join12345.setJoinTb14(data + "Next3intTb14.b");

			join12345.setJoinTb21(data + "intTb21.b");
			join12345.setJoinTb22(data + "intTb22.b");
			join12345.setJoinTb23(data + "intTb23.b");
			join12345.setJoinTb24(data + "intTb24.b");

			tableforLastRelation = new String[] { data + "nation.b", data + "region.b", data + "customer.b",
					data + "orders.b", data + "lineitem.b" };
			join12345.setTableforLastRelation(tableforLastRelation);

			join12345.setFirstJoinTb1(false);
			join12345.setFirstJoinTb2(true);

			join12345.setTb1(new String[] { "nation", "region", "customer", "orders" });
			join12345.setTb2(new String[] { "suppkey", "lineitem" });

			join12345.setReReadDiskTb1(new String[] { "nation", "region", "customer", "orders" });
			join12345.setReReadDiskTb2(new String[] { "lineitem" });

			join12345.setAuxMemoryKey(4);
			join12345.setAuxMemoryRowid(60);

			join12345.setAtbNextJoin(new String[] { "suppkey" });

			rrStartTime = System.currentTimeMillis();
			join.phaseTwo4Threads(join12345);

			rrStopTime = System.currentTimeMillis();

			tempoPhase2 = (rrStopTime - rrStartTime) / 1000;

			tempoTotal += tempoPhase2;

			escritasPepiLine = (join12345.nextHaf11.numberofWrite + join12345.nextHaf12.numberofWrite
					+ +join12345.nextHaf13.numberofWrite + join12345.nextHaf14.numberofWrite);

			escritasTotal += escritasPepiLine;

			tupleBD = tupleBD + tempoPhase2 + "|" + "1825856" + "|" + escritasPepiLine + "|"
					+ (tempoPhase1 + tempoPhase2) + "|";

			tb1thread11 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();
			tb1thread12 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();
			tb1thread13 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();
			tb1thread14 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();

			mapOfTable11 = new TreeMap<Integer, ArrayList<Integer>>();
			mapOfTable12 = new TreeMap<Integer, ArrayList<Integer>>();
			mapOfTable13 = new TreeMap<Integer, ArrayList<Integer>>();
			mapOfTable14 = new TreeMap<Integer, ArrayList<Integer>>();

			mapOfTable21 = new TreeMap<Integer, ArrayList<Integer>>();
			mapOfTable22 = new TreeMap<Integer, ArrayList<Integer>>();
			mapOfTable23 = new TreeMap<Integer, ArrayList<Integer>>();
			mapOfTable24 = new TreeMap<Integer, ArrayList<Integer>>();

			tb2thread11 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();
			tb2thread12 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();
			tb2thread13 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();
			tb2thread14 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();

			tb1thread11 = tb1Nextthread11;
			tb1thread12 = tb1Nextthread12;
			tb1thread13 = tb1Nextthread13;
			tb1thread14 = tb1Nextthread14;

			mapOfTable11 = mapOfNextTable11;
			mapOfTable12 = mapOfNextTable12;
			mapOfTable13 = mapOfNextTable13;
			mapOfTable14 = mapOfNextTable14;

			tb1Nextthread11 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();
			tb1Nextthread12 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();
			tb1Nextthread13 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();
			tb1Nextthread14 = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();

			mapOfNextTable11 = new TreeMap<Integer, ArrayList<Integer>>();
			mapOfNextTable12 = new TreeMap<Integer, ArrayList<Integer>>();
			mapOfNextTable13 = new TreeMap<Integer, ArrayList<Integer>>();
			mapOfNextTable14 = new TreeMap<Integer, ArrayList<Integer>>();

			setZero(numbersOfKeysInInterMemory, numbersOfTuplesInInterMemory);

			EarlyHash sincron06 = new EarlyHash();

			joinColumns = new String[] { "suppkey" };
			sincron06.setJoinColumns(joinColumns);

			earlyColumns = null;
			sincron06.setEarlyColumns(earlyColumns);

			firstJoinTable = true;
			sincron06.setFirstJoinTable(firstJoinTable);

			firstLastTable = false;
			sincron06.setFirstLastTable(firstLastTable);

			headerJoin = "suppkey[I(18)]|supplier[A(55)]|";
			sincron06.setHeaderJoin(headerJoin);

			selectRelation = null;
			sincron06.setSelectRelation(selectRelation);

			sincron06.setTable(data + "supplier.b");
			sincron06.setNumberPhase(1);
			sincron06.setNumberTable(2);

			haf21 = new HandleFile(blockSize);
			haf21.create(data + "intTb21.b", headerJoin);
			sincron06.setHaf21(haf21);

			haf22 = new HandleFile(blockSize);
			haf22.create(data + "intTb22.b", headerJoin);
			sincron06.setHaf22(haf22);

			haf23 = new HandleFile(blockSize);
			haf23.create(data + "intTb23.b", headerJoin);
			sincron06.setHaf23(haf23);

			haf24 = new HandleFile(blockSize);
			haf24.create(data + "intTb24.b", headerJoin);
			sincron06.setHaf24(haf24);

			sincron06.setHaf11(sincron05.haf11);
			sincron06.setHaf12(sincron05.haf12);
			sincron06.setHaf13(sincron05.haf13);
			sincron06.setHaf14(sincron05.haf14);

			sincron06.setMemorySizeJoinKernel(memorySize);

			sincron06.setAuxMemoryCurrentTuple(12);
			sincron06.setAuxMemoryCurrentKey(4);
			sincron06.setAuxMemoryPreviusTuple(60);
			sincron06.setAuxMemoryPreviusKey(4);

			rrStartTime = System.currentTimeMillis();
			join.phaseOne4Threads(sincron06);

			rrStopTime = System.currentTimeMillis();

			tempoPhase1 = (rrStopTime - rrStartTime) / 1000;

			escritasOverflowAcumuladasTb1 = haf11.numberofWrite + haf12.numberofWrite + haf13.numberofWrite
					+ haf14.numberofWrite;
			escritasOverflowAcumuladasTb2 = haf21.numberofWrite + haf22.numberofWrite + haf23.numberofWrite
					+ haf24.numberofWrite;

			escritasTotal += escritasOverflowAcumuladasTb1 + escritasOverflowAcumuladasTb2;

			tempoTotal += tempoPhase1;

			tupleBD = tupleBD + "Supplier|" + numtotaltuplestb + "|" + tempoPhase1 + "|";

			tupleBD = tupleBD + escritasOverflowAcumuladasTb1 + "|" + escritasOverflowAcumuladasTb2 + "|";

			LateHash join123456 = new LateHash();

			joinColumns = new String[] { "suppkey" };
			join123456.setJoinColumns(joinColumns);

			earlyColumnsTb1 = null;
			join123456.setEarlyColumnsTb1(earlyColumnsTb1);

			earlyColumnsTb2 = null;
			join123456.setEarlyColumnsTb1(earlyColumnsTb2);

			lastJoin = true;
			join123456.setLastJoin(lastJoin);

			join123456.setJoinTb11(data + "Next4intTb11.b");
			join123456.setJoinTb12(data + "Next4intTb12.b");
			join123456.setJoinTb13(data + "Next4intTb13.b");
			join123456.setJoinTb14(data + "Next4intTb14.b");

			join123456.setJoinTb21(data + "intTb21.b");
			join123456.setJoinTb22(data + "intTb22.b");
			join123456.setJoinTb23(data + "intTb23.b");
			join123456.setJoinTb24(data + "intTb24.b");

			tableforLastRelation = new String[] { data + "nation.b", data + "region.b", data + "customer.b",
					data + "orders.b", data + "lineitem.b", data + "supplier.b" };
			join123456.setTableforLastRelation(tableforLastRelation);

			join123456.setFirstJoinTb1(false);
			join123456.setFirstJoinTb2(true);

			join123456.setTb1(new String[] { "nation", "region", "customer", "orders", "lineitem" });
			join123456.setTb2(new String[] { "supplier" });

			headerJoin = "nation[A(18)]|region[A(18)]|customer[A(55)]|orders[A(55)]|lineitem[A(55)]|supplier[A(55)]|";
			join123456.setHeaderJoin(headerJoin);

			join123456.setReReadDiskTb1(new String[] { "nation", "region", "customer", "orders", "lineitem" });
			join123456.setReReadDiskTb2(new String[] { "supplier" });

			rrStartTime = System.currentTimeMillis();
			join.phaseTwo4Threads(join123456);
			rrStopTime = System.currentTimeMillis();

			tempoPhase2 = (rrStopTime - rrStartTime) / 1000;

			tempoTotal += tempoPhase2;

			tupleBD = tupleBD + tempoPhase2 + "|" + (tempoPhase1 + tempoPhase2) + "|" + escritasTotal + "|"
					+ reLeiturasAcumuladas + "|" + (numberOfTupleFinalJoin2 + numberOfTupleFinalJoin2Disk) + "|"
					+ tempoTotal;
			System.out.println(tupleBD);
			resultlog.writeLog(tupleBD);

		}

	}

	private static void setZero(int numbersOfKeysInInterMemory2, int numbersOfTuplesInInterMemory2) {
		numbersOfKeysInMemorytb1 = 0;
		numbersOfTuplesInMemorytb1 = 0;

		numbersOfTuplesInMemorytb2 = 0;
		numbersOfKeysInMemorytb2 = 0;

		numbersOfKeysInInterMemory = 0;
		numbersOfTuplesInInterMemory = 0;

		keyInMemorytb11 = numbersOfKeysInInterMemory2;
		tupleInMemorytb11 = numbersOfTuplesInInterMemory2;

		keyOutMemorytb11 = 0;
		tupleOutMemorytb11 = 0;

		keyInMemorytb12 = 0;
		tupleInMemorytb12 = 0;

		keyOutMemorytb12 = 0;
		tupleOutMemorytb12 = 0;

		keyInMemorytb13 = 0;
		tupleInMemorytb13 = 0;

		keyOutMemorytb13 = 0;
		tupleOutMemorytb13 = 0;

		keyInMemorytb14 = 0;
		tupleInMemorytb14 = 0;

		keyOutMemorytb14 = 0;
		tupleOutMemorytb14 = 0;

		keyInMemorytb21 = 0;
		tupleInMemorytb21 = 0;

		keyOutMemorytb21 = 0;
		tupleOutMemorytb21 = 0;

		keyInMemorytb22 = 0;
		tupleInMemorytb22 = 0;

		keyOutMemorytb22 = 0;
		tupleOutMemorytb22 = 0;

		keyInMemorytb23 = 0;
		tupleInMemorytb23 = 0;

		keyOutMemorytb23 = 0;
		tupleOutMemorytb23 = 0;

		keyInMemorytb24 = 0;
		tupleInMemorytb24 = 0;

		keyOutMemorytb24 = 0;
		tupleOutMemorytb24 = 0;

	}

	private void phaseOne4Threads(EarlyHash sincron) throws Exception {
		RunnablePhaseOne read11 = new RunnablePhaseOne(sincron, 1);
		RunnablePhaseOne read12 = new RunnablePhaseOne(sincron, 2);
		RunnablePhaseOne read13 = new RunnablePhaseOne(sincron, 3);
		RunnablePhaseOne read14 = new RunnablePhaseOne(sincron, 4);

		Thread fpt11 = new Thread(read11);
		Thread fpt12 = new Thread(read12);
		Thread fpt13 = new Thread(read13);
		Thread fpt14 = new Thread(read14);
		numtotaltuplestb = 0;
		fpt11.start();
		fpt12.start();
		fpt13.start();
		fpt14.start();

		try {
			fpt11.join();
			fpt12.join();
			fpt13.join();
			fpt14.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	class RunnablePhaseOne implements Runnable {

		EarlyHash sincron = new EarlyHash();
		int numerobloco = 0;

		public RunnablePhaseOne(EarlyHash sincron, int numerobloco) {
			this.sincron = sincron;
			this.numerobloco = numerobloco;
		}

		@Override
		public void run() {
			if (sincron.numberPhase == 1) {
				try {
					HandleFile haf = new HandleFile(blockSize);
					haf.open(sincron.table);
					String header = RafIOCalc.getHeaderString(haf);
					boolean select = false;
					byte block[] = new byte[blockSize];
					byte tupleBlock[];
					int numblockstart = this.numerobloco;
					int realeaseMemoryNumberPartition = (hFull.hashTableSize - 1);
					int indcol;

					int[] indJoinCols = null;
					int[] auxMemoryPercent = new int[6];
					auxMemoryPercent[0] = sincron.memorySizeJoinKernel;
					auxMemoryPercent[1] = sincron.auxMemoryCurrentKey;
					auxMemoryPercent[2] = sincron.auxMemoryCurrentTuple;
					auxMemoryPercent[3] = sincron.auxMemoryPreviusKey;
					auxMemoryPercent[4] = sincron.auxMemoryPreviusTuple;
					auxMemoryPercent[5] = sincron.numberTable;
					if (sincron.earlyColumns != null) {
						indJoinCols = new int[sincron.earlyColumns.length];
					} else {
						indJoinCols = new int[0];
					}
					for (int i = 0; i < indJoinCols.length; i++) {
						indJoinCols[i] = haf.getColumnPos(sincron.earlyColumns[i]);
						header = header + sincron.earlyColumns[i] + "[I(18)]|";
					}
					indcol = haf.getColumnPos(sincron.joinColumns[0]);
					header = RafIOCalc.getHeaderString(haf);
					String tuple = "";
					byte[] rowid;
					int hashResult4 = 0;
					int keyJoin = 0;
					String keyss = "";
					block = haf.readBlock(numblockstart);
					int numtupleTb = 0;
					while (block != null) {
						tupleBlock = haf.nextTuple(block);
						while (tupleBlock != null) {
							if (sincron.selectRelation != null) {
								for (int i = 0; i < sincron.selectRelation.length; i++) {
									select = haf.checkCondition(sincron.selectRelation[i], tupleBlock);
									if (!select) {
										i = sincron.selectRelation.length;
									}
								}
							} else {
								select = true;
							}

							if (select) {
								numtupleTb++;
								keyJoin = RafIOCalc.getKey(tupleBlock, indcol, haf.getQtCols());
								if (indJoinCols != null) {
									keyss = "";
									for (int i = 0; i < indJoinCols.length; i++) {
										keyss = RafIOCalc.getColumn(haf, tupleBlock, indJoinCols[i], haf.getQtCols())
												+ "|" + keyss;
									}
								}
								if (sincron.firstJoinTable) {
									// tupleId = haf.getCurrentTupleId();
									// rowid = takeRowid(indcol, 1,
									// numblockstart,
									// tupleId);
									rowid = haf.getRowid(indcol);
									tuple = "";
									tuple = keyJoin + "|" + keyss + RafIOCalc.getRowid(rowid) + "|";
								} else {
									tuple = RafIOCalc.getLineString(haf, tupleBlock, haf.getQtCols());
								}

								hashResult4 = h4.hashCode(keyJoin);

								memory = calcMemory(sincron);
								if (sincron.numberTable == 1) {
									if (memory) {
										if (hashResult4 == 0) {
											addTupleMemory1Tb1(keyJoin, tuple.getBytes());
										}
										if (hashResult4 == 1) {
											addTupleMemory2Tb1(keyJoin, tuple.getBytes());
										}
										if (hashResult4 == 2) {
											addTupleMemory3Tb1(keyJoin, tuple.getBytes());
										}
										if (hashResult4 == 3) {
											addTupleMemory4Tb1(keyJoin, tuple.getBytes());
										}
									} else {
										if (hashResult4 == 0) {
											// releaseMemory1(sincron);
											addTupleDisk1Tb1(keyJoin,

													tuple.getBytes(), sincron.haf11);
										}
										if (hashResult4 == 1) {
											// releaseMemory2(sincron);
											addTupleDisk2Tb1(keyJoin,

													tuple.getBytes(), sincron.haf12);
										}
										if (hashResult4 == 2) {
											// releaseMemory2(sincron);
											addTupleDisk3Tb1(keyJoin,

													tuple.getBytes(), sincron.haf13);
										}
										if (hashResult4 == 3) {
											// releaseMemory2(sincron);
											addTupleDisk4Tb1(keyJoin,

													tuple.getBytes(), sincron.haf14);
										}
									}
								} else {
									memory = calcMemory(sincron);

									if (this.numerobloco == 1 && numbersOfTuplesInMemorytb1 != 0 && !memory
											&& (sincron.haf11.raf.getContainerFilename().equals(data + "intTb11.b"))) {
										realeaseTb1Thread1(realeaseMemoryNumberPartition, sincron.haf11);
										realeaseMemoryNumberPartition--;
									}
									if (this.numerobloco == 2 && numbersOfTuplesInMemorytb1 != 0 && !memory
											&& (sincron.haf12.raf.getContainerFilename().equals(data + "intTb12.b"))) {
										realeaseTb1Thread2(realeaseMemoryNumberPartition, sincron.haf12);
										realeaseMemoryNumberPartition--;
									}
									if (this.numerobloco == 3 && numbersOfTuplesInMemorytb1 != 0 && !memory
											&& (sincron.haf13.raf.getContainerFilename().equals(data + "intTb13.b"))) {
										realeaseTb1Thread3(realeaseMemoryNumberPartition, sincron.haf13);
										realeaseMemoryNumberPartition--;
									}
									if (this.numerobloco == 4 && numbersOfTuplesInMemorytb1 != 0 && !memory
											&& (sincron.haf14.raf.getContainerFilename().equals(data + "intTb14.b"))) {
										realeaseTb1Thread4(realeaseMemoryNumberPartition, sincron.haf14);
										realeaseMemoryNumberPartition--;
									}

									memory = calcMemory(sincron);
									if (memory) {
										if (hashResult4 == 0) {
											addTupleMemory1Tb2(keyJoin, tuple.getBytes());
										}
										if (hashResult4 == 1) {
											addTupleMemory2Tb2(keyJoin, tuple.getBytes());
										}
										if (hashResult4 == 2) {
											addTupleMemory3Tb2(keyJoin, tuple.getBytes());
										}
										if (hashResult4 == 3) {
											addTupleMemory4Tb2(keyJoin, tuple.getBytes());
										}
									} else {
										if (hashResult4 == 0) {
											addTupleDisk1Tb2(keyJoin,

													tuple.getBytes(),

													sincron.haf21);
										}
										if (hashResult4 == 1) {
											addTupleDisk2Tb2(keyJoin,

													tuple.getBytes(),

													sincron.haf22);
										}
										if (hashResult4 == 2) {
											addTupleDisk3Tb2(keyJoin,

													tuple.getBytes(),

													sincron.haf23);
										}
										if (hashResult4 == 3) {
											addTupleDisk4Tb2(keyJoin,

													tuple.getBytes(),

													sincron.haf24);
										}
									}
								}
							}
							tupleBlock = haf.nextTuple(block);
						}
						numblockstart += 4;
						block = haf.readBlock(numblockstart);
					}
					numtotaltuplestb += numtupleTb;
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

		}
	}

	private void realeaseTb1Thread1(int numPartition, HandleFile haf11) throws Exception {
		int numblockwritten;
		TreeMap<Integer, ArrayList<byte[]>> auxBucketsInMemory = new TreeMap<Integer, ArrayList<byte[]>>();
		ArrayList<Integer> vetOfhashResult = null;
		ArrayList<byte[]> auxBucketsInMemoryInside = new ArrayList<byte[]>();
		String linhaToAux = "";
		synchronized (haf11) {
			synchronized (tb1thread11) {
				synchronized (mapOfTable11) {
					if (tb1thread11.containsKey(numPartition)) {
						auxBucketsInMemory = tb1thread11.get(numPartition);
						vetOfhashResult = mapOfTable11.get(numPartition);
						if (vetOfhashResult == null) {
							vetOfhashResult = new ArrayList<>();
						}
						for (Integer chave : auxBucketsInMemory.keySet()) {
							// numbersOfKeysInMemorytb1--;
							keyOutMemorytb11++;
							auxBucketsInMemoryInside = auxBucketsInMemory.get(chave);
							// exceeded memory for buckets
							// remove of memory and copy to disk
							for (int j = 0; j < auxBucketsInMemoryInside.size(); j++) {
								linhaToAux = new String(auxBucketsInMemoryInside.get(j));
								// numbersOfTuplesInMemorytb1--;
								tupleOutMemorytb11++;
								haf11.writeTuple(linhaToAux);
								linhaToAux = "";
								numblockwritten = haf11.raf.getMaxBlockNo();
								// write map of blocks
								if (!vetOfhashResult.contains(numblockwritten)) {
									vetOfhashResult.add(numblockwritten);
								}
								mapOfTable11.put(numPartition, vetOfhashResult);
							}
						}
						haf11.flush();
						// haf11.close();
						tb1thread11.remove(numPartition);
					}
				}
			}
		}
	}

	private void realeaseTb1Thread2(int numPartition, HandleFile haf12) throws Exception {
		int numblockwritten;
		TreeMap<Integer, ArrayList<byte[]>> auxBucketsInMemory = new TreeMap<Integer, ArrayList<byte[]>>();
		ArrayList<Integer> vetOfhashResult = null;
		ArrayList<byte[]> auxBucketsInMemoryInside = new ArrayList<byte[]>();
		String linhaToAux = "";
		synchronized (haf12) {
			synchronized (tb1thread12) {
				synchronized (mapOfTable12) {
					if (tb1thread12.containsKey(numPartition)) {
						auxBucketsInMemory = tb1thread12.get(numPartition);
						vetOfhashResult = mapOfTable12.get(numPartition);
						if (vetOfhashResult == null) {
							vetOfhashResult = new ArrayList<>();
						}
						for (Integer chave : auxBucketsInMemory.keySet()) {
							keyOutMemorytb12++;
							auxBucketsInMemoryInside = auxBucketsInMemory.get(chave);
							// exceeded memory for buckets
							// remove of memory and copy to disk
							for (int j = 0; j < auxBucketsInMemoryInside.size(); j++) {
								linhaToAux = new String(auxBucketsInMemoryInside.get(j));
								tupleOutMemorytb12++;
								haf12.writeTuple(linhaToAux);
								linhaToAux = "";
								numblockwritten = haf12.raf.getMaxBlockNo();
								// write map of blocks
								if (!vetOfhashResult.contains(numblockwritten)) {
									vetOfhashResult.add(numblockwritten);
								}
								mapOfTable12.put(numPartition, vetOfhashResult);
							}
						}
						haf12.flush();
						tb1thread12.remove(numPartition);
					}
				}
			}
		}
	}

	private void realeaseTb1Thread3(int numPartition, HandleFile haf13) throws Exception {
		int numblockwritten;
		TreeMap<Integer, ArrayList<byte[]>> auxBucketsInMemory = new TreeMap<Integer, ArrayList<byte[]>>();
		ArrayList<Integer> vetOfhashResult = null;
		ArrayList<byte[]> auxBucketsInMemoryInside = new ArrayList<byte[]>();
		String linhaToAux = "";
		synchronized (haf13) {
			synchronized (tb1thread13) {
				synchronized (mapOfTable13) {
					if (tb1thread13.containsKey(numPartition)) {
						auxBucketsInMemory = tb1thread13.get(numPartition);
						vetOfhashResult = mapOfTable13.get(numPartition);
						if (vetOfhashResult == null) {
							vetOfhashResult = new ArrayList<>();
						}
						for (Integer chave : auxBucketsInMemory.keySet()) {
							keyOutMemorytb13++;
							auxBucketsInMemoryInside = auxBucketsInMemory.get(chave);
							// exceeded memory for buckets
							// remove of memory and copy to disk
							for (int j = 0; j < auxBucketsInMemoryInside.size(); j++) {
								linhaToAux = new String(auxBucketsInMemoryInside.get(j));
								tupleOutMemorytb13++;
								haf13.writeTuple(linhaToAux);
								linhaToAux = "";
								numblockwritten = haf13.raf.getMaxBlockNo();
								// write map of blocks
								if (!vetOfhashResult.contains(numblockwritten)) {
									vetOfhashResult.add(numblockwritten);
								}
								mapOfTable13.put(numPartition, vetOfhashResult);
							}
						}
						haf13.flush();
						tb1thread13.remove(numPartition);
					}
				}
			}
		}
	}

	private void realeaseTb1Thread4(int numPartition, HandleFile haf14) throws Exception {
		int numblockwritten;
		TreeMap<Integer, ArrayList<byte[]>> auxBucketsInMemory = new TreeMap<Integer, ArrayList<byte[]>>();
		ArrayList<Integer> vetOfhashResult = null;
		ArrayList<byte[]> auxBucketsInMemoryInside = new ArrayList<byte[]>();
		String linhaToAux = "";
		synchronized (haf14) {
			synchronized (tb1thread14) {
				synchronized (mapOfTable14) {
					if (tb1thread14.containsKey(numPartition)) {
						auxBucketsInMemory = tb1thread14.get(numPartition);
						vetOfhashResult = mapOfTable14.get(numPartition);
						if (vetOfhashResult == null) {
							vetOfhashResult = new ArrayList<>();
						}
						for (Integer chave : auxBucketsInMemory.keySet()) {
							keyOutMemorytb14++;
							auxBucketsInMemoryInside = auxBucketsInMemory.get(chave);
							// exceeded memory for buckets
							// remove of memory and copy to disk
							for (int j = 0; j < auxBucketsInMemoryInside.size(); j++) {
								linhaToAux = new String(auxBucketsInMemoryInside.get(j));
								tupleOutMemorytb14++;
								haf14.writeTuple(linhaToAux);
								linhaToAux = "";
								numblockwritten = haf14.raf.getMaxBlockNo();
								// write map of blocks
								if (!vetOfhashResult.contains(numblockwritten)) {
									vetOfhashResult.add(numblockwritten);
								}
								mapOfTable14.put(numPartition, vetOfhashResult);
							}
						}
						haf14.flush();
						tb1thread14.remove(numPartition);
					}
				}
			}
		}
	}

	private void phaseTwo4Threads(LateHash sincron) throws Exception {
		RunnablePhaseTwo join1 = new RunnablePhaseTwo(sincron, 1);
		RunnablePhaseTwo join2 = new RunnablePhaseTwo(sincron, 2);
		RunnablePhaseTwo join3 = new RunnablePhaseTwo(sincron, 3);
		RunnablePhaseTwo join4 = new RunnablePhaseTwo(sincron, 4);

		Thread fpt1 = new Thread(join1);
		Thread fpt2 = new Thread(join2);
		Thread fpt3 = new Thread(join3);
		Thread fpt4 = new Thread(join4);

		fpt1.start();
		fpt2.start();
		fpt3.start();
		fpt4.start();

		try {
			fpt1.join();
			fpt2.join();
			fpt3.join();
			fpt4.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}

	class RunnablePhaseTwo implements Runnable {

		LateHash sincron = new LateHash();
		int numberThread = 0;
		int numeroblocoend = 0;

		public RunnablePhaseTwo(LateHash sincron, int numberThread) {
			this.sincron = sincron;
			this.numberThread = numberThread;

		}

		@Override
		public void run() {
			TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>> tb1threadFull = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();
			TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>> tb2threadFull = new TreeMap<Integer, TreeMap<Integer, ArrayList<byte[]>>>();

			TreeMap<Integer, ArrayList<Integer>> mapOfTable1Full = new TreeMap<Integer, ArrayList<Integer>>();
			TreeMap<Integer, ArrayList<Integer>> mapOfTable2Full = new TreeMap<Integer, ArrayList<Integer>>();
			// Auxiliares
			TreeMap<Integer, ArrayList<byte[]>> auxBucketsInMemory = new TreeMap<Integer, ArrayList<byte[]>>();
			ArrayList<byte[]> auxBucketsInMemoryInside = new ArrayList<byte[]>();

			TreeMap<Integer, ArrayList<byte[]>> auxBucketsInMemoryToHash = new TreeMap<Integer, ArrayList<byte[]>>();
			ArrayList<byte[]> auxBucketsInMemoryInsideToHash = new ArrayList<byte[]>();

			int numberOfTupleFinalJoin = 0;

			String linhaAuxSmaller = "";
			String linhaAuxBigger = "";

			HandleFile hafFulltb1 = new HandleFile(blockSize);
			HandleFile hafFulltb2 = new HandleFile(blockSize);

			try {
				if (numberThread == 1) {
					tb1threadFull = tb1thread11;
					tb2threadFull = tb2thread11;
					mapOfTable1Full = mapOfTable11;
					mapOfTable2Full = mapOfTable21;
					hafFulltb1.open(sincron.joinTb11);
					hafFulltb2.open(sincron.joinTb21);
				}
				if (numberThread == 2) {
					tb1threadFull = tb1thread12;
					tb2threadFull = tb2thread12;
					mapOfTable1Full = mapOfTable12;
					mapOfTable2Full = mapOfTable22;
					hafFulltb1.open(sincron.joinTb12);
					hafFulltb2.open(sincron.joinTb22);
				}
				if (numberThread == 3) {
					tb1threadFull = tb1thread13;
					tb2threadFull = tb2thread13;
					mapOfTable1Full = mapOfTable13;
					mapOfTable2Full = mapOfTable23;
					hafFulltb1.open(sincron.joinTb13);
					hafFulltb2.open(sincron.joinTb23);
				}
				if (numberThread == 4) {
					tb1threadFull = tb1thread14;
					tb2threadFull = tb2thread14;
					mapOfTable1Full = mapOfTable14;
					mapOfTable2Full = mapOfTable24;
					hafFulltb1.open(sincron.joinTb14);
					hafFulltb2.open(sincron.joinTb24);
				}

				// Estou sempre fazendo o Smaller para o Bigger
				int keyJoin, hashResult4;
				byte[] tupleBlock;

				boolean sendToDiskNext = false;

				int[] colsReReadTb1 = new int[sincron.reReadDiskTb1.length];
				int[] colsReReadTb2 = new int[sincron.reReadDiskTb2.length];

				for (int i = 0; i < colsReReadTb1.length; i++) {
					colsReReadTb1[i] = hafFulltb1.getColumnPos(sincron.reReadDiskTb1[i]);
				}
				for (int i = 0; i < colsReReadTb2.length; i++) {
					colsReReadTb2[i] = hafFulltb2.getColumnPos(sincron.reReadDiskTb2[i]);
				}

				if (sincron.lastJoin) {

					HandleFile hafReRead0 = new HandleFile(blockSize);
					HandleFile hafReRead1 = new HandleFile(blockSize);
					HandleFile hafReRead2 = new HandleFile(blockSize);
					HandleFile hafReRead3 = new HandleFile(blockSize);
					HandleFile hafReRead4 = new HandleFile(blockSize);
					HandleFile hafReRead5 = new HandleFile(blockSize);

					for (int i = 0; i < sincron.tableforLastRelation.length; i++) {
						try {
							if (i == 0) {
								hafReRead0.open(sincron.tableforLastRelation[i]);
							}
							if (i == 1) {
								hafReRead1.open(sincron.tableforLastRelation[i]);
							}
							if (i == 2) {
								hafReRead2.open(sincron.tableforLastRelation[i]);
							}
							if (i == 3) {
								hafReRead3.open(sincron.tableforLastRelation[i]);
							}
							if (i == 4) {
								hafReRead4.open(sincron.tableforLastRelation[i]);
							}
							if (i == 5) {
								hafReRead5.open(sincron.tableforLastRelation[i]);
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}

					// Memoria x Memoria
					String[] auxLine;
					numberOfTupleFinalJoin = 0;

					for (Integer hashResultInMapForSmaller : tb1threadFull.keySet()) {
						if (tb2threadFull.containsKey(hashResultInMapForSmaller)) {
							auxBucketsInMemoryToHash = tb1threadFull.get(hashResultInMapForSmaller);
							auxBucketsInMemory = tb2threadFull.get(hashResultInMapForSmaller);
							// Eu j� estou com os dois buckets Smaller e Bigger
							// Vou percorrer o smaller verificando se tem no
							// bigger
							for (Integer hashResultInMapForSmallerInside : auxBucketsInMemoryToHash.keySet()) {
								// Verifica��o j� a n�vel de atributo de jun��o
								if (auxBucketsInMemory.containsKey(hashResultInMapForSmallerInside)) {
									auxBucketsInMemoryInsideToHash = auxBucketsInMemoryToHash
											.get(hashResultInMapForSmallerInside);
									auxBucketsInMemoryInside = auxBucketsInMemory.get(hashResultInMapForSmallerInside);
									for (int z = 0; z < auxBucketsInMemoryInsideToHash.size(); z++) {
										// por se tem � porque eles tem o mesmo
										// valor de
										linhaAuxSmaller = new String(auxBucketsInMemoryInsideToHash.get(z));
										auxLine = linhaAuxSmaller.split("\\|");
										linhaAuxSmaller = "";
										for (int j = 0; j < colsReReadTb1.length; j++) {
											linhaAuxSmaller += auxLine[colsReReadTb1[j]] + "|";
										}

										for (int j = 0; j < auxBucketsInMemoryInside.size(); j++) {
											// Como � first a linha j� � o rowid
											linhaAuxBigger = new String(auxBucketsInMemoryInside.get(j));

											auxLine = linhaAuxBigger.split("\\|");
											linhaAuxBigger = "";
											for (int k = 0; k < colsReReadTb2.length; k++) {
												linhaAuxBigger += auxLine[colsReReadTb2[k]] + "|";
											}

											String linhaToAux = linhaAuxSmaller + linhaAuxBigger;
											numberOfTupleFinalJoin++;
											String line = "";

											String[] reRead = linhaToAux.split("\\|");
											byte[] reReadTuple;

											for (int i = 0; i < sincron.tableforLastRelation.length; i++) {
												try {
													if (i == 0) {
														reReadTuple = hafReRead0.readRowid(reRead[i]);
														line = line + RafIOCalc.getLineString(hafReRead0, reReadTuple,
																hafReRead0.getQtCols());
													}
													if (i == 1) {
														reReadTuple = hafReRead1.readRowid(reRead[i]);
														line = line + RafIOCalc.getLineString(hafReRead1, reReadTuple,
																hafReRead1.getQtCols());
													}
													if (i == 2) {
														reReadTuple = hafReRead2.readRowid(reRead[i]);
														line = line + RafIOCalc.getLineString(hafReRead2, reReadTuple,
																hafReRead2.getQtCols());
													}
													if (i == 3) {
														reReadTuple = hafReRead3.readRowid(reRead[i]);
														line = line + RafIOCalc.getLineString(hafReRead3, reReadTuple,
																hafReRead3.getQtCols());
													}
													if (i == 4) {
														reReadTuple = hafReRead4.readRowid(reRead[i]);
														line = line + RafIOCalc.getLineString(hafReRead4, reReadTuple,
																hafReRead4.getQtCols());
													}
													if (i == 5) {
														reReadTuple = hafReRead5.readRowid(reRead[i]);
														line = line + RafIOCalc.getLineString(hafReRead5, reReadTuple,
																hafReRead5.getQtCols());
													}
												} catch (Exception e) {
													// TODO Auto-generated catch
													// block
													e.printStackTrace();
												}
											}
											linhaToAux = "";
											linhaAuxBigger = "";
										}
									}
									linhaAuxSmaller = "";
								}
							}
						}
					}

					// Fazer Memoria x Disco | Menor x Maior
					ArrayList<Integer> veTreeMapDiskResult = new ArrayList<>();
					//
					HashMap<Integer, ArrayList<Integer>> hashIndice = new HashMap<Integer, ArrayList<Integer>>();
					ArrayList<Integer> auxHash = new ArrayList<>();
					int keyHashMapSmaller, numblockreaded, currentTupleId, numblockreadedAuxBigger, keysSmaller,
							keysBigger;
					byte[] block, blockAuxBigger, tupleBlockAuxBigger;

					String linha, linhaToAux;
					int[] indJoinColstb1 = new int[sincron.joinColumns.length];
					int[] indJoinColstb2 = new int[sincron.joinColumns.length];
					boolean isThere;
					ArrayList<Integer> vetOfhashResultAuxBigger;
					ArrayList<Integer> vetOfhashResult;
					int contDisk = 0;

					for (int i = 0; i < sincron.joinColumns.length; i++) {
						indJoinColstb1[i] = hafFulltb1.getColumnPos(sincron.joinColumns[i]);
						indJoinColstb2[i] = hafFulltb2.getColumnPos(sincron.joinColumns[i]);
					}
					//
					for (Integer hashResultInMapForSmaller : tb1threadFull.keySet()) {
						if (mapOfTable2Full.containsKey(hashResultInMapForSmaller)) {
							auxBucketsInMemory = tb1threadFull.get(hashResultInMapForSmaller);
							veTreeMapDiskResult = mapOfTable2Full.get(hashResultInMapForSmaller);
							for (int i = 0; i < veTreeMapDiskResult.size(); i++) {
								numblockreaded = veTreeMapDiskResult.get(i);
								block = hafFulltb2.readBlock(numblockreaded);
								tupleBlock = hafFulltb2.nextTuple(block);
								if (tupleBlock != null) {
									linha = RafIOCalc.getLineString(hafFulltb2, tupleBlock, hafFulltb2.getQtCols());
								} else {
									linha = null;
								}
								while (linha != null) {
									keysSmaller = RafIOCalc.getKey(tupleBlock, indJoinColstb2[0],
											hafFulltb2.getQtCols());
									if (auxBucketsInMemory.containsKey(keysSmaller)) {
										linhaAuxBigger = RafIOCalc.getColumn(hafFulltb2, tupleBlock, colsReReadTb2[0],
												hafFulltb2.getQtCols());

										auxBucketsInMemoryInsideToHash = auxBucketsInMemory.get(keysSmaller);

										for (int j = 0; j < auxBucketsInMemoryInsideToHash.size(); j++) {
											linhaAuxSmaller = new String(auxBucketsInMemoryInsideToHash.get(j));
											auxLine = linhaAuxSmaller.split("\\|");
											linhaAuxSmaller = "";
											for (int k = 0; k < colsReReadTb1.length; k++) {
												linhaAuxSmaller += auxLine[colsReReadTb1[k]] + "|";
											}

											linhaToAux = linhaAuxSmaller + linhaAuxBigger;
											contDisk++;
											String line = "";

											String[] reRead = linhaToAux.split("\\|");
											byte[] reReadTuple;

											for (int k = 0; k < sincron.tableforLastRelation.length; k++) {
												try {
													if (k == 0) {
														reReadTuple = hafReRead0.readRowid(reRead[k]);
														line = line + RafIOCalc.getLineString(hafReRead0, reReadTuple,
																hafReRead0.getQtCols());
													}
													if (k == 1) {
														reReadTuple = hafReRead1.readRowid(reRead[k]);
														line = line + RafIOCalc.getLineString(hafReRead1, reReadTuple,
																hafReRead1.getQtCols());
													}
													if (k == 2) {
														reReadTuple = hafReRead2.readRowid(reRead[k]);
														line = line + RafIOCalc.getLineString(hafReRead2, reReadTuple,
																hafReRead2.getQtCols());
													}
													if (k == 3) {
														reReadTuple = hafReRead3.readRowid(reRead[k]);
														line = line + RafIOCalc.getLineString(hafReRead3, reReadTuple,
																hafReRead3.getQtCols());
													}
													if (k == 4) {
														reReadTuple = hafReRead4.readRowid(reRead[k]);
														line = line + RafIOCalc.getLineString(hafReRead4, reReadTuple,
																hafReRead4.getQtCols());
													}
													if (k == 5) {
														reReadTuple = hafReRead5.readRowid(reRead[k]);
														line = line + RafIOCalc.getLineString(hafReRead5, reReadTuple,
																hafReRead5.getQtCols());
													}
												} catch (Exception e) {
													// TODO Auto-generated catch
													// block
													e.printStackTrace();
												}
											}

										}

									}

									tupleBlock = hafFulltb2.nextTuple(block);

									if (tupleBlock != null) {
										linha = RafIOCalc.getLineString(hafFulltb2, tupleBlock, hafFulltb2.getQtCols());
									} else {
										linha = null;
									}
								}
							}
						}
					}

					for (Integer hashResultInMapForSmaller : tb2threadFull.keySet()) {
						if (mapOfTable1Full.containsKey(hashResultInMapForSmaller)) {
							auxBucketsInMemory = tb2threadFull.get(hashResultInMapForSmaller);
							veTreeMapDiskResult = mapOfTable1Full.get(hashResultInMapForSmaller);

							for (int i = 0; i < veTreeMapDiskResult.size(); i++) {
								numblockreaded = veTreeMapDiskResult.get(i);
								block = hafFulltb1.readBlock(numblockreaded);
								tupleBlock = hafFulltb1.nextTuple(block);
								if (tupleBlock != null) {
									linha = RafIOCalc.getLineString(hafFulltb1, tupleBlock, hafFulltb1.getQtCols());
								} else {
									linha = null;
								}

								while (linha != null) {
									keysSmaller = RafIOCalc.getKey(tupleBlock, indJoinColstb1[0],
											hafFulltb1.getQtCols());
									if (auxBucketsInMemory.containsKey(keysSmaller)) {

										linhaAuxSmaller = "";
										for (int k = 0; k < colsReReadTb1.length; k++) {
											linhaAuxSmaller = linhaAuxSmaller + RafIOCalc.getColumn(hafFulltb1,
													tupleBlock, colsReReadTb1[k], hafFulltb1.getQtCols()) + "|";
										}

										auxBucketsInMemoryInsideToHash = auxBucketsInMemory.get(keysSmaller);

										for (int j = 0; j < auxBucketsInMemoryInsideToHash.size(); j++) {
											linhaAuxBigger = new String(auxBucketsInMemoryInsideToHash.get(j));
											auxLine = linhaAuxBigger.split("\\|");
											linhaAuxBigger = "";
											for (int k = 0; k < colsReReadTb2.length; k++) {
												linhaAuxBigger += auxLine[colsReReadTb2[k]] + "|";
											}

											linhaToAux = linhaAuxSmaller + linhaAuxBigger;
											contDisk++;

											String line = "";

											String[] reRead = linhaToAux.split("\\|");
											byte[] reReadTuple;

											for (int k = 0; k < sincron.tableforLastRelation.length; k++) {
												try {
													if (k == 0) {
														reReadTuple = hafReRead0.readRowid(reRead[k]);
														line = line + RafIOCalc.getLineString(hafReRead0, reReadTuple,
																hafReRead0.getQtCols());
													}
													if (k == 1) {
														reReadTuple = hafReRead1.readRowid(reRead[k]);
														line = line + RafIOCalc.getLineString(hafReRead1, reReadTuple,
																hafReRead1.getQtCols());
													}
													if (k == 2) {
														reReadTuple = hafReRead2.readRowid(reRead[k]);
														line = line + RafIOCalc.getLineString(hafReRead2, reReadTuple,
																hafReRead2.getQtCols());
													}
													if (k == 3) {
														reReadTuple = hafReRead3.readRowid(reRead[k]);
														line = line + RafIOCalc.getLineString(hafReRead3, reReadTuple,
																hafReRead3.getQtCols());
													}
													if (k == 4) {
														reReadTuple = hafReRead4.readRowid(reRead[k]);
														line = line + RafIOCalc.getLineString(hafReRead4, reReadTuple,
																hafReRead4.getQtCols());
													}
													if (k == 5) {
														reReadTuple = hafReRead5.readRowid(reRead[k]);
														line = line + RafIOCalc.getLineString(hafReRead5, reReadTuple,
																hafReRead5.getQtCols());
													}
												} catch (Exception e) {
													// TODO Auto-generated catch
													// block
													e.printStackTrace();
												}
											}

										}

									}

									tupleBlock = hafFulltb1.nextTuple(block);
									if (tupleBlock != null) {
										linha = RafIOCalc.getLineString(hafFulltb1, tupleBlock, hafFulltb1.getQtCols());
									} else {
										linha = null;
									}
								}
							}
						}
					}

					// Disk x Disk
					for (Entry<Integer, ArrayList<Integer>> entry : mapOfTable1Full.entrySet()) {
						// hash(key)
						keyHashMapSmaller = entry.getKey();
						// blockNo of tuples of this hash
						vetOfhashResult = entry.getValue();
						// for each blockNo
						for (int i = 0; i < vetOfhashResult.size(); i++) {
							numblockreaded = vetOfhashResult.get(i);
							block = hafFulltb1.readBlock(numblockreaded);
							tupleBlock = hafFulltb1.nextTuple(block);
							if (tupleBlock != null) {
								linha = RafIOCalc.getLineString(hafFulltb1, tupleBlock, hafFulltb1.getQtCols());
							} else {
								linha = null;
							}
							while (linha != null) {
								keysSmaller = RafIOCalc.getKey(tupleBlock, indJoinColstb1[0], hafFulltb1.getQtCols());
								currentTupleId = hafFulltb1.getCurrentTupleId();
								isThere = hashIndice.containsKey(keysSmaller);
								if (!isThere) {
									auxHash = new ArrayList<>();
									auxHash.add(numblockreaded);
									auxHash.add(currentTupleId);
									hashIndice.put(keysSmaller, auxHash);
								} else {
									auxHash = hashIndice.get(keysSmaller);
									auxHash.add(numblockreaded);
									auxHash.add(currentTupleId);
									hashIndice.put(keysSmaller, auxHash);
								}

								tupleBlock = hafFulltb1.nextTuple(block);

								if (tupleBlock != null) {
									linha = RafIOCalc.getLineString(hafFulltb1, tupleBlock, hafFulltb1.getQtCols());
								} else {
									linha = null;
								}
							}
						}

						// Chech if keyHashMapSmaller exists on mapOfTableBigger
						if (mapOfTable2Full.containsKey(keyHashMapSmaller)) {
							vetOfhashResultAuxBigger = mapOfTable2Full.get(keyHashMapSmaller);
							for (int i = 0; i < vetOfhashResultAuxBigger.size(); i++) {
								numblockreadedAuxBigger = vetOfhashResultAuxBigger.get(i);
								blockAuxBigger = hafFulltb2.readBlock(numblockreadedAuxBigger);
								tupleBlockAuxBigger = hafFulltb2.nextTuple(blockAuxBigger);
								if (tupleBlockAuxBigger != null) {
									linhaAuxBigger = RafIOCalc.getColumn(hafFulltb2, tupleBlockAuxBigger,
											colsReReadTb2[0], hafFulltb2.getQtCols());
								} else {
									linhaAuxBigger = null;
								}
								while (linhaAuxBigger != null) {
									keysBigger = RafIOCalc.getKey(tupleBlockAuxBigger, indJoinColstb2[0],
											hafFulltb2.getQtCols());
									auxHash = hashIndice.get(keysBigger);
									if (auxHash != null) {
										for (int j = 0; j < auxHash.size(); j = j + 2) {
											block = hafFulltb1.raf.readBlock(auxHash.get(j));
											tupleBlock = hafFulltb1.readTupleById(block, auxHash.get(j + 1));
											linhaAuxSmaller = "";
											for (int k = 0; k < colsReReadTb1.length; k++) {
												linhaAuxSmaller = linhaAuxSmaller + RafIOCalc.getColumn(hafFulltb1,
														tupleBlock, colsReReadTb1[k], hafFulltb1.getQtCols()) + "|";
											}

											linhaToAux = linhaAuxSmaller + linhaAuxBigger;
											contDisk++;
											String line = "";

											String[] reRead = linhaToAux.split("\\|");
											byte[] reReadTuple;

											for (int k = 0; k < sincron.tableforLastRelation.length; k++) {
												try {
													if (k == 0) {
														reReadTuple = hafReRead0.readRowid(reRead[k]);
														line = line + RafIOCalc.getLineString(hafReRead0, reReadTuple,
																hafReRead0.getQtCols());
													}
													if (k == 1) {
														reReadTuple = hafReRead1.readRowid(reRead[k]);
														line = line + RafIOCalc.getLineString(hafReRead1, reReadTuple,
																hafReRead1.getQtCols());
													}
													if (k == 2) {
														reReadTuple = hafReRead2.readRowid(reRead[k]);
														line = line + RafIOCalc.getLineString(hafReRead2, reReadTuple,
																hafReRead2.getQtCols());
													}
													if (k == 3) {
														reReadTuple = hafReRead3.readRowid(reRead[k]);
														line = line + RafIOCalc.getLineString(hafReRead3, reReadTuple,
																hafReRead3.getQtCols());
													}
													if (k == 4) {
														reReadTuple = hafReRead4.readRowid(reRead[k]);
														line = line + RafIOCalc.getLineString(hafReRead4, reReadTuple,
																hafReRead4.getQtCols());
													}
													if (k == 5) {
														reReadTuple = hafReRead5.readRowid(reRead[k]);
														line = line + RafIOCalc.getLineString(hafReRead5, reReadTuple,
																hafReRead5.getQtCols());
													}
												} catch (Exception e) {
													// TODO Auto-generated catch
													// block
													e.printStackTrace();
												}
											}

											linhaToAux = "";
											linhaAuxSmaller = "";
										}
									}

									linhaAuxBigger = "";
									tupleBlockAuxBigger = hafFulltb2.nextTuple(blockAuxBigger);
									if (tupleBlockAuxBigger != null) {
										linhaAuxBigger = RafIOCalc.getColumn(hafFulltb2, tupleBlockAuxBigger,
												colsReReadTb2[0], hafFulltb2.getQtCols());
									} else {
										linhaAuxBigger = null;
									}
								}

							}

						}
						hashIndice.clear();
					}

					numberOfTupleFinalJoin2 += numberOfTupleFinalJoin;
					numberOfTupleFinalJoin2Disk += contDisk;
					System.out.println("Finishing thread number: " + numberThread);

					reLeiturasAcumuladas += hafReRead0.numberofReReadbyRowId + hafReRead1.numberofReReadbyRowId
							+ hafReRead2.numberofReReadbyRowId + hafReRead3.numberofReReadbyRowId
							+ hafReRead4.numberofReReadbyRowId + hafReRead5.numberofReReadbyRowId;

					reLeiturasAcumuladasTb1 += hafReRead0.numberofReReadbyRowId;
					reLeiturasAcumuladasTb2 += hafReRead1.numberofReReadbyRowId;
					reLeiturasAcumuladasTb3 += hafReRead3.numberofReReadbyRowId;
					reLeiturasAcumuladasTb4 += hafReRead4.numberofReReadbyRowId;
					reLeiturasAcumuladasTb5 += hafReRead5.numberofReReadbyRowId;

				} else {
					int nextAtb = sincron.nextHaf11.getColumnPos(sincron.atbNextJoin[0]);
					int nextAtbTb1[] = new int[sincron.tb1.length];
					int nextAtbTb2[] = new int[sincron.tb2.length];
					for (int i = 0; i < sincron.tb1.length; i++) {
						nextAtbTb1[i] = hafFulltb1.getColumnPos(sincron.tb1[i]);
					}
					for (int i = 0; i < sincron.tb2.length; i++) {
						nextAtbTb2[i] = hafFulltb2.getColumnPos(sincron.tb2[i]);
					}
					String[] auxTb1;
					String[] auxTb2;
					String linhaToAux;

					for (Integer hashResultInMapForSmaller : tb1threadFull.keySet()) {
						if (tb2threadFull.containsKey(hashResultInMapForSmaller)) {
							auxBucketsInMemoryToHash = tb1threadFull.get(hashResultInMapForSmaller);
							auxBucketsInMemory = tb2threadFull.get(hashResultInMapForSmaller);
							// Eu j� estou com os dois buckets Smaller e Bigger
							// Vou percorrer o smaller verificando se tem no
							// bigger
							for (Integer hashResultInMapForSmallerInside : auxBucketsInMemoryToHash.keySet()) {
								// Verifica��o j� a n�vel de atributo de jun��o
								if (auxBucketsInMemory.containsKey(hashResultInMapForSmallerInside)) {
									auxBucketsInMemoryInsideToHash = auxBucketsInMemoryToHash
											.get(hashResultInMapForSmallerInside);
									auxBucketsInMemoryInside = auxBucketsInMemory.get(hashResultInMapForSmallerInside);

									for (int i = 0; i < auxBucketsInMemoryInsideToHash.size(); i++) {
										// por se tem � porque eles tem o mesmo
										// valor de
										linhaAuxSmaller = new String(auxBucketsInMemoryInsideToHash.get(i));
										auxTb1 = linhaAuxSmaller.split("\\|");

										linhaAuxSmaller = "";

										for (int j = 0; j < nextAtbTb1.length; j++) {

											linhaAuxSmaller += auxTb1[nextAtbTb1[j]] + "|";
										}

										for (int j = 0; j < auxBucketsInMemoryInside.size(); j++) {

											// Como � first a linha j� � o rowid
											linhaAuxBigger = new String(auxBucketsInMemoryInside.get(j));

											auxTb2 = linhaAuxBigger.split("\\|");

											linhaAuxBigger = "";

											for (int k = 0; k < nextAtbTb2.length; k++) {
												linhaAuxBigger += auxTb2[nextAtbTb2[k]] + "|";
											}

											linhaToAux = linhaAuxSmaller + linhaAuxBigger;

											String[] reRead = linhaToAux.split("\\|");

											keyJoin = Integer.parseInt(reRead[nextAtb]);

											hashResult4 = h4.hashCode(keyJoin);

											sendToDiskNext = calcInterMemory(sincron);
											try {
												if (sendToDiskNext) {
													if (hashResult4 == 0) {
														sendToInterMemory1(keyJoin, linhaToAux, sincron);
													}
													if (hashResult4 == 1) {
														sendToInterMemory2(keyJoin, linhaToAux, sincron);
													}
													if (hashResult4 == 2) {
														sendToInterMemory3(keyJoin, linhaToAux, sincron);
													}
													if (hashResult4 == 3) {
														sendToInterMemory4(keyJoin, linhaToAux, sincron);
													}

												} else {
													if (hashResult4 == 0) {
														sendToDisk1(sincron.nextHaf11, nextAtb);
													}
													if (hashResult4 == 1) {
														sendToDisk2(sincron.nextHaf12, nextAtb);
													}
													if (hashResult4 == 2) {
														sendToDisk3(sincron.nextHaf13, nextAtb);
													}
													if (hashResult4 == 3) {
														sendToDisk4(sincron.nextHaf14, nextAtb);
													}
												}
											} catch (Exception e) {
												// TODO Auto-generated catch
												// block
												e.printStackTrace();
											}
											numberOfTupleFinalJoin++;
											linhaToAux = "";
											linhaAuxBigger = "";
										}
									}
									linhaAuxSmaller = "";
								}
							}
						}
					}
					// Memoria x Disco
					// Fazer Memoria x Disco Menor x Maior
					ArrayList<Integer> veTreeMapDiskResult = new ArrayList<>();
					//
					HashMap<Integer, ArrayList<Integer>> hashIndice = new HashMap<Integer, ArrayList<Integer>>();
					ArrayList<Integer> auxHash = new ArrayList<>();
					int keyHashMapSmaller, numblockreaded, currentTupleId, numblockreadedAuxBigger, keysSmaller,
							keysBigger;
					byte[] block, blockAuxBigger, tupleBlockAuxBigger;

					String linha;
					int[] indJoinColstb1 = new int[sincron.joinColumns.length];
					int[] indJoinColstb2 = new int[sincron.joinColumns.length];
					boolean isThere;
					ArrayList<Integer> vetOfhashResultAuxBigger;
					ArrayList<Integer> vetOfhashResult;
					String[] auxLine;

					for (int i = 0; i < sincron.joinColumns.length; i++) {
						indJoinColstb1[i] = hafFulltb1.getColumnPos(sincron.joinColumns[i]);
						indJoinColstb2[i] = hafFulltb2.getColumnPos(sincron.joinColumns[i]);
					}
					for (Integer hashResultInMapForSmaller : tb1threadFull.keySet()) {
						if (mapOfTable2Full.containsKey(hashResultInMapForSmaller)) {
							auxBucketsInMemory = tb1threadFull.get(hashResultInMapForSmaller);
							veTreeMapDiskResult = mapOfTable2Full.get(hashResultInMapForSmaller);
							for (int i = 0; i < veTreeMapDiskResult.size(); i++) {
								numblockreaded = veTreeMapDiskResult.get(i);
								block = hafFulltb2.readBlock(numblockreaded);
								tupleBlock = hafFulltb2.nextTuple(block);
								if (tupleBlock != null) {
									linha = RafIOCalc.getLineString(hafFulltb2, tupleBlock, hafFulltb2.getQtCols());
								} else {
									linha = null;
								}
								while (linha != null) {
									keysSmaller = RafIOCalc.getKey(tupleBlock, indJoinColstb2[0],
											hafFulltb2.getQtCols());
									if (auxBucketsInMemory.containsKey(keysSmaller)) {
										auxLine = linha.split("\\|");
										linhaAuxBigger = "";
										for (int k = 0; k < nextAtbTb2.length; k++) {
											linhaAuxBigger += auxLine[nextAtbTb2[k]] + "|";
										}
										auxBucketsInMemoryInsideToHash = auxBucketsInMemory.get(keysSmaller);

										for (int j = 0; j < auxBucketsInMemoryInsideToHash.size(); j++) {
											linhaAuxSmaller = new String(auxBucketsInMemoryInsideToHash.get(j));
											auxLine = linhaAuxSmaller.split("\\|");
											linhaAuxSmaller = "";
											for (int k = 0; k < nextAtbTb1.length; k++) {
												linhaAuxSmaller += auxLine[nextAtbTb1[k]] + "|";
											}

											linhaToAux = linhaAuxSmaller + linhaAuxBigger;
											String[] reRead = linhaToAux.split("\\|");
											keyJoin = Integer.parseInt(reRead[nextAtb]);
											hashResult4 = h4.hashCode(keyJoin);
											sendToDiskNext = calcInterMemory(sincron);
											try {
												if (sendToDiskNext) {
													if (hashResult4 == 0) {
														sendToInterMemory1(keyJoin, linhaToAux, sincron);
													}
													if (hashResult4 == 1) {
														sendToInterMemory2(keyJoin, linhaToAux, sincron);
													}
													if (hashResult4 == 2) {
														sendToInterMemory3(keyJoin, linhaToAux, sincron);
													}
													if (hashResult4 == 3) {
														sendToInterMemory4(keyJoin, linhaToAux, sincron);
													}

												} else {
													if (hashResult4 == 0) {
														sendToDisk1(sincron.nextHaf11, nextAtb);
													}
													if (hashResult4 == 1) {
														sendToDisk2(sincron.nextHaf12, nextAtb);
													}
													if (hashResult4 == 2) {
														sendToDisk3(sincron.nextHaf13, nextAtb);
													}
													if (hashResult4 == 3) {
														sendToDisk4(sincron.nextHaf14, nextAtb);
													}
												}
											} catch (Exception e) {
												// TODO Auto-generated catch
												// block
												e.printStackTrace();
											}

										}

									}

									tupleBlock = hafFulltb2.nextTuple(block);

									if (tupleBlock != null) {
										linha = RafIOCalc.getLineString(hafFulltb2, tupleBlock, hafFulltb2.getQtCols());
									} else {
										linha = null;
									}
								}
							}
						}
					}

					for (Integer hashResultInMapForSmaller : tb2threadFull.keySet()) {
						if (mapOfTable1Full.containsKey(hashResultInMapForSmaller)) {
							auxBucketsInMemory = tb2threadFull.get(hashResultInMapForSmaller);
							veTreeMapDiskResult = mapOfTable1Full.get(hashResultInMapForSmaller);
							for (int i = 0; i < veTreeMapDiskResult.size(); i++) {
								numblockreaded = veTreeMapDiskResult.get(i);
								block = hafFulltb1.readBlock(numblockreaded);
								tupleBlock = hafFulltb1.nextTuple(block);
								if (tupleBlock != null) {
									linha = RafIOCalc.getLineString(hafFulltb1, tupleBlock, hafFulltb1.getQtCols());
								} else {
									linha = null;
								}
								while (linha != null) {
									keysSmaller = RafIOCalc.getKey(tupleBlock, indJoinColstb1[0],
											hafFulltb1.getQtCols());
									if (auxBucketsInMemory.containsKey(keysSmaller)) {

										linhaAuxSmaller = "";
										for (int k = 0; k < nextAtbTb1.length; k++) {
											linhaAuxSmaller = linhaAuxSmaller + RafIOCalc.getColumn(hafFulltb1,
													tupleBlock, nextAtbTb1[k], hafFulltb1.getQtCols()) + "|";
										}

										auxBucketsInMemoryInsideToHash = auxBucketsInMemory.get(keysSmaller);

										for (int j = 0; j < auxBucketsInMemoryInsideToHash.size(); j++) {
											linhaAuxBigger = new String(auxBucketsInMemoryInsideToHash.get(j));
											auxLine = linhaAuxBigger.split("\\|");
											linhaAuxBigger = "";
											for (int k = sincron.joinColumns.length; k < auxLine.length; k++) {
												linhaAuxBigger += auxLine[k] + "|";
											}

											linhaToAux = linhaAuxSmaller + linhaAuxBigger;
											numberOfTupleFinalJoin++;

											String[] reRead = linhaToAux.split("\\|");

											keyJoin = Integer.parseInt(reRead[nextAtb]);

											hashResult4 = h4.hashCode(keyJoin);

											sendToDiskNext = calcInterMemory(sincron);
											try {
												if (sendToDiskNext) {
													if (hashResult4 == 0) {
														sendToInterMemory1(keyJoin, linhaToAux, sincron);
													}
													if (hashResult4 == 1) {
														sendToInterMemory2(keyJoin, linhaToAux, sincron);
													}
													if (hashResult4 == 2) {
														sendToInterMemory3(keyJoin, linhaToAux, sincron);
													}
													if (hashResult4 == 3) {
														sendToInterMemory4(keyJoin, linhaToAux, sincron);
													}

												} else {
													if (hashResult4 == 0) {
														sendToDisk1(sincron.nextHaf11, nextAtb);
													}
													if (hashResult4 == 1) {
														sendToDisk2(sincron.nextHaf12, nextAtb);
													}
													if (hashResult4 == 2) {
														sendToDisk3(sincron.nextHaf13, nextAtb);
													}
													if (hashResult4 == 3) {
														sendToDisk4(sincron.nextHaf14, nextAtb);
													}
												}
											} catch (Exception e) {
												// TODO Auto-generated catch
												// block
												e.printStackTrace();
											}

										}

									}

									tupleBlock = hafFulltb1.nextTuple(block);

									if (tupleBlock != null) {
										linha = RafIOCalc.getLineString(hafFulltb1, tupleBlock, hafFulltb1.getQtCols());
									} else {
										linha = null;
									}
								}
							}
						}
					}

					// Disk x Disk
					for (Entry<Integer, ArrayList<Integer>> entry : mapOfTable1Full.entrySet()) {
						keyHashMapSmaller = entry.getKey();
						// blockNo of tuples of this hash
						vetOfhashResult = entry.getValue();
						// for each blockNo
						for (int i = 0; i < vetOfhashResult.size(); i++) {
							numblockreaded = vetOfhashResult.get(i);
							block = hafFulltb1.readBlock(numblockreaded);
							tupleBlock = hafFulltb1.nextTuple(block);
							if (tupleBlock != null) {
								linha = RafIOCalc.getLineString(hafFulltb1, tupleBlock, hafFulltb1.getQtCols());
							} else {
								linha = null;
							}
							while (linha != null) {
								keysSmaller = RafIOCalc.getKey(tupleBlock, indJoinColstb1[0], hafFulltb1.getQtCols());
								currentTupleId = hafFulltb1.getCurrentTupleId();
								isThere = hashIndice.containsKey(keysSmaller);
								if (!isThere) {
									auxHash = new ArrayList<>();
									auxHash.add(numblockreaded);
									auxHash.add(currentTupleId);
									hashIndice.put(keysSmaller, auxHash);
								} else {
									auxHash = hashIndice.get(keysSmaller);
									auxHash.add(numblockreaded);
									auxHash.add(currentTupleId);
									hashIndice.put(keysSmaller, auxHash);
								}

								tupleBlock = hafFulltb1.nextTuple(block);

								if (tupleBlock != null) {
									linha = RafIOCalc.getLineString(hafFulltb1, tupleBlock, hafFulltb1.getQtCols());
								} else {
									linha = null;
								}
							}
						}

						// Chech if keyHashMapSmaller exists on mapOfTableBigger
						if (mapOfTable2Full.containsKey(keyHashMapSmaller)) {
							vetOfhashResultAuxBigger = mapOfTable2Full.get(keyHashMapSmaller);
							for (int i = 0; i < vetOfhashResultAuxBigger.size(); i++) {
								numblockreadedAuxBigger = vetOfhashResultAuxBigger.get(i);
								blockAuxBigger = hafFulltb2.readBlock(numblockreadedAuxBigger);
								tupleBlockAuxBigger = hafFulltb2.nextTuple(blockAuxBigger);
								if (tupleBlockAuxBigger != null) {
									linhaAuxBigger = "";
									for (int z = 0; z < nextAtbTb2.length; z++) {
										linhaAuxBigger += RafIOCalc.getColumn(hafFulltb2, tupleBlockAuxBigger,
												nextAtbTb2[z], hafFulltb2.getQtCols()) + "|";
									}
								} else {
									linhaAuxBigger = null;
								}
								while (linhaAuxBigger != null) {
									keysBigger = RafIOCalc.getKey(tupleBlockAuxBigger, indJoinColstb2[0],
											hafFulltb2.getQtCols());
									auxHash = hashIndice.get(keysBigger);
									if (auxHash != null) {
										for (int j = 0; j < auxHash.size(); j = j + 2) {
											block = hafFulltb1.raf.readBlock(auxHash.get(j));
											tupleBlock = hafFulltb1.readTupleById(block, auxHash.get(j + 1));
											linhaAuxSmaller = "";
											for (int k = 0; k < nextAtbTb1.length; k++) {
												linhaAuxSmaller = linhaAuxSmaller + RafIOCalc.getColumn(hafFulltb1,
														tupleBlock, nextAtbTb1[k], hafFulltb1.getQtCols()) + "|";
											}

											linhaToAux = linhaAuxSmaller + linhaAuxBigger;
											String[] reRead = linhaToAux.split("\\|");

											keyJoin = Integer.parseInt(reRead[nextAtb]);

											hashResult4 = h4.hashCode(keyJoin);

											sendToDiskNext = calcInterMemory(sincron);
											try {
												if (sendToDiskNext) {
													if (hashResult4 == 0) {
														sendToInterMemory1(keyJoin, linhaToAux, sincron);
													}
													if (hashResult4 == 1) {
														sendToInterMemory2(keyJoin, linhaToAux, sincron);
													}
													if (hashResult4 == 2) {
														sendToInterMemory3(keyJoin, linhaToAux, sincron);
													}
													if (hashResult4 == 3) {
														sendToInterMemory4(keyJoin, linhaToAux, sincron);
													}

												} else {
													if (hashResult4 == 0) {
														sendToDisk1(sincron.nextHaf11, nextAtb);
													}
													if (hashResult4 == 1) {
														sendToDisk2(sincron.nextHaf12, nextAtb);
													}
													if (hashResult4 == 2) {
														sendToDisk3(sincron.nextHaf13, nextAtb);
													}
													if (hashResult4 == 3) {
														sendToDisk4(sincron.nextHaf14, nextAtb);
													}
												}
											} catch (Exception e) {
												// TODO Auto-generated catch
												// block
												e.printStackTrace();
											}

											// System.out.println(linhaToAux);
											// hafFlashJoin.writeTuple(linhaToAux);
											linhaToAux = "";
											linhaAuxSmaller = "";

										}
									} else {
										// System.out.println("Nao achou na HashIndice");
									}

									linhaAuxBigger = "";
									tupleBlockAuxBigger = hafFulltb2.nextTuple(blockAuxBigger);
									if (tupleBlockAuxBigger != null) {
										for (int z = 0; z < nextAtbTb2.length; z++) {
											linhaAuxBigger += RafIOCalc.getColumn(hafFulltb2, tupleBlockAuxBigger,
													nextAtbTb2[z], hafFulltb2.getQtCols()) + "|";
										}
									} else {
										linhaAuxBigger = null;
									}
								}

							}

						}
						hashIndice.clear();
					}

				}

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	private boolean calcInterMemory(LateHash sincron) {
		boolean memory = false;
		int usedMemory = 0;
		usedMemory += numbersOfKeysInInterMemory * sincron.auxMemoryKey;
		usedMemory += numbersOfTuplesInInterMemory * sincron.auxMemoryRowid;
		if (usedMemory < calcmemoryInter) {
			memory = true;
		}
		return memory;
	}

	private void sendToInterMemory1(int keyJoin, String linhaToAux, LateHash sincron) {
		int hashResult;
		TreeMap<Integer, ArrayList<byte[]>> auxBucketsInMemory = new TreeMap<Integer, ArrayList<byte[]>>();
		ArrayList<byte[]> auxBucketsInMemoryInside = new ArrayList<byte[]>();
		boolean isThere = false;
		hashResult = hFull.hashCode(keyJoin);
		synchronized (tb1Nextthread11) {
			if (tb1Nextthread11.containsKey(hashResult)) {
				auxBucketsInMemory = tb1Nextthread11.get(hashResult);
				// Cabe em memoria e cabe no bucket
				isThere = auxBucketsInMemory.containsKey(keyJoin);
				if (!isThere) {
					numbersOfKeysInInterMemory++;
					auxBucketsInMemoryInside = new ArrayList<byte[]>();
				} else {
					auxBucketsInMemoryInside = auxBucketsInMemory.get(keyJoin);
				}
				numbersOfTuplesInInterMemory++;
				auxBucketsInMemoryInside.add(linhaToAux.getBytes());
				auxBucketsInMemory.put(keyJoin, auxBucketsInMemoryInside);
				tb1Nextthread11.put(hashResult, auxBucketsInMemory);
			} else {
				auxBucketsInMemory = new TreeMap<Integer, ArrayList<byte[]>>();
				auxBucketsInMemoryInside = new ArrayList<byte[]>();
				auxBucketsInMemoryInside.add(linhaToAux.getBytes());
				numbersOfTuplesInInterMemory++;
				numbersOfKeysInInterMemory++;
				auxBucketsInMemory.put(keyJoin, auxBucketsInMemoryInside);
				tb1Nextthread11.put(hashResult, auxBucketsInMemory);
			}
		}
	}

	private void sendToInterMemory2(int keyJoin, String linhaToAux, LateHash sincron) {
		int hashResult;
		TreeMap<Integer, ArrayList<byte[]>> auxBucketsInMemory = new TreeMap<Integer, ArrayList<byte[]>>();
		ArrayList<byte[]> auxBucketsInMemoryInside = new ArrayList<byte[]>();
		boolean isThere = false;
		hashResult = hFull.hashCode(keyJoin);
		synchronized (tb1Nextthread12) {
			if (tb1Nextthread12.containsKey(hashResult)) {
				auxBucketsInMemory = tb1Nextthread12.get(hashResult);
				// Cabe em memoria e cabe no bucket
				isThere = auxBucketsInMemory.containsKey(keyJoin);
				if (!isThere) {
					numbersOfKeysInInterMemory++;
					auxBucketsInMemoryInside = new ArrayList<byte[]>();
				} else {
					auxBucketsInMemoryInside = auxBucketsInMemory.get(keyJoin);
				}
				numbersOfTuplesInInterMemory++;
				auxBucketsInMemoryInside.add(linhaToAux.getBytes());
				auxBucketsInMemory.put(keyJoin, auxBucketsInMemoryInside);
				tb1Nextthread12.put(hashResult, auxBucketsInMemory);
			} else {
				auxBucketsInMemory = new TreeMap<Integer, ArrayList<byte[]>>();
				auxBucketsInMemoryInside = new ArrayList<byte[]>();
				auxBucketsInMemoryInside.add(linhaToAux.getBytes());
				numbersOfTuplesInInterMemory++;
				numbersOfKeysInInterMemory++;
				auxBucketsInMemory.put(keyJoin, auxBucketsInMemoryInside);
				tb1Nextthread12.put(hashResult, auxBucketsInMemory);
			}
		}

	}

	private void sendToInterMemory3(int keyJoin, String linhaToAux, LateHash sincron) {
		int hashResult;
		TreeMap<Integer, ArrayList<byte[]>> auxBucketsInMemory = new TreeMap<Integer, ArrayList<byte[]>>();
		ArrayList<byte[]> auxBucketsInMemoryInside = new ArrayList<byte[]>();
		boolean isThere = false;
		hashResult = hFull.hashCode(keyJoin);
		synchronized (tb1Nextthread13) {
			if (tb1Nextthread13.containsKey(hashResult)) {
				auxBucketsInMemory = tb1Nextthread13.get(hashResult);
				// Cabe em memoria e cabe no bucket
				isThere = auxBucketsInMemory.containsKey(keyJoin);
				if (!isThere) {
					numbersOfKeysInInterMemory++;
					auxBucketsInMemoryInside = new ArrayList<byte[]>();
				} else {
					auxBucketsInMemoryInside = auxBucketsInMemory.get(keyJoin);
				}
				numbersOfTuplesInInterMemory++;
				auxBucketsInMemoryInside.add(linhaToAux.getBytes());
				auxBucketsInMemory.put(keyJoin, auxBucketsInMemoryInside);
				tb1Nextthread13.put(hashResult, auxBucketsInMemory);
			} else {
				auxBucketsInMemory = new TreeMap<Integer, ArrayList<byte[]>>();
				auxBucketsInMemoryInside = new ArrayList<byte[]>();
				auxBucketsInMemoryInside.add(linhaToAux.getBytes());
				numbersOfTuplesInInterMemory++;
				numbersOfKeysInInterMemory++;
				auxBucketsInMemory.put(keyJoin, auxBucketsInMemoryInside);
				tb1Nextthread13.put(hashResult, auxBucketsInMemory);
			}
		}

	}

	private void sendToInterMemory4(int keyJoin, String linhaToAux, LateHash sincron) {
		int hashResult;
		TreeMap<Integer, ArrayList<byte[]>> auxBucketsInMemory = new TreeMap<Integer, ArrayList<byte[]>>();
		ArrayList<byte[]> auxBucketsInMemoryInside = new ArrayList<byte[]>();
		boolean isThere = false;
		hashResult = hFull.hashCode(keyJoin);
		synchronized (tb1Nextthread14) {
			if (tb1Nextthread14.containsKey(hashResult)) {
				auxBucketsInMemory = tb1Nextthread14.get(hashResult);
				// Cabe em memoria e cabe no bucket
				isThere = auxBucketsInMemory.containsKey(keyJoin);
				if (!isThere) {
					numbersOfKeysInInterMemory++;
					auxBucketsInMemoryInside = new ArrayList<byte[]>();
				} else {
					auxBucketsInMemoryInside = auxBucketsInMemory.get(keyJoin);
				}
				numbersOfTuplesInInterMemory++;
				auxBucketsInMemoryInside.add(linhaToAux.getBytes());
				auxBucketsInMemory.put(keyJoin, auxBucketsInMemoryInside);
				tb1Nextthread14.put(hashResult, auxBucketsInMemory);
			} else {
				auxBucketsInMemory = new TreeMap<Integer, ArrayList<byte[]>>();
				auxBucketsInMemoryInside = new ArrayList<byte[]>();
				auxBucketsInMemoryInside.add(linhaToAux.getBytes());
				numbersOfTuplesInInterMemory++;
				numbersOfKeysInInterMemory++;
				auxBucketsInMemory.put(keyJoin, auxBucketsInMemoryInside);
				tb1Nextthread14.put(hashResult, auxBucketsInMemory);
			}
		}

	}

	private boolean calcMemory(EarlyHash sincron) {
		boolean memory = false;
		int usedMemory = 0;

		if (j.equals("QA") || j.equals("QB") || j.equals("QC")) {
			if (sincron.numberTable == 1) {
				numbersOfKeysInMemorytb1 = (keyInMemorytb11 + keyInMemorytb12 + keyInMemorytb13 + keyInMemorytb14)
						- (keyOutMemorytb11 + keyOutMemorytb12 + keyOutMemorytb13 + keyOutMemorytb14);
				numbersOfTuplesInMemorytb1 = (tupleInMemorytb11 + tupleInMemorytb12 + tupleInMemorytb13
						+ tupleInMemorytb14)
						- (tupleOutMemorytb11 + tupleOutMemorytb12 + tupleOutMemorytb13 + tupleOutMemorytb14);

				usedMemory += numbersOfKeysInMemorytb1 * sincron.auxMemoryCurrentKey;
				usedMemory += numbersOfTuplesInMemorytb1 * sincron.auxMemoryCurrentTuple;
				if (usedMemory < (calcmemory + calcmemoryInter)) {
					memory = true;
				}
			} else {
				numbersOfKeysInMemorytb1 = (keyInMemorytb11 + keyInMemorytb12 + keyInMemorytb13 + keyInMemorytb14)
						- (keyOutMemorytb11 + keyOutMemorytb12 + keyOutMemorytb13 + keyOutMemorytb14);
				numbersOfTuplesInMemorytb1 = (tupleInMemorytb11 + tupleInMemorytb12 + tupleInMemorytb13
						+ tupleInMemorytb14)
						- (tupleOutMemorytb11 + tupleOutMemorytb12 + tupleOutMemorytb13 + tupleOutMemorytb14);

				numbersOfKeysInMemorytb2 = (keyInMemorytb21 + keyInMemorytb22 + keyInMemorytb23 + keyInMemorytb24)
						- (keyOutMemorytb21 + keyOutMemorytb22 + keyOutMemorytb23 + keyOutMemorytb24);
				numbersOfTuplesInMemorytb2 = (tupleInMemorytb21 + tupleInMemorytb22 + tupleInMemorytb23
						+ tupleInMemorytb24)
						- (tupleOutMemorytb21 + tupleOutMemorytb22 + tupleOutMemorytb23 + tupleOutMemorytb24);

				usedMemory += numbersOfKeysInMemorytb1 * sincron.auxMemoryPreviusKey;
				usedMemory += numbersOfTuplesInMemorytb1 * sincron.auxMemoryPreviusTuple;
				usedMemory += numbersOfKeysInMemorytb2 * sincron.auxMemoryCurrentKey;
				usedMemory += numbersOfTuplesInMemorytb2 * sincron.auxMemoryCurrentTuple;
				if (usedMemory < (calcmemory + calcmemoryInter)) {
					memory = true;
				}
			}
		} else {
			if (sincron.numberTable == 1) {
				numbersOfKeysInMemorytb1 = (keyInMemorytb11 + keyInMemorytb12 + keyInMemorytb13 + keyInMemorytb14)
						- (keyOutMemorytb11 + keyOutMemorytb12 + keyOutMemorytb13 + keyOutMemorytb14);
				numbersOfTuplesInMemorytb1 = (tupleInMemorytb11 + tupleInMemorytb12 + tupleInMemorytb13
						+ tupleInMemorytb14)
						- (tupleOutMemorytb11 + tupleOutMemorytb12 + tupleOutMemorytb13 + tupleOutMemorytb14);

				usedMemory += numbersOfKeysInMemorytb1 * sincron.auxMemoryCurrentKey;
				usedMemory += numbersOfTuplesInMemorytb1 * sincron.auxMemoryCurrentTuple;
				if (usedMemory < calcmemory) {
					memory = true;
				}
			} else {
				numbersOfKeysInMemorytb1 = (keyInMemorytb11 + keyInMemorytb12 + keyInMemorytb13 + keyInMemorytb14)
						- (keyOutMemorytb11 + keyOutMemorytb12 + keyOutMemorytb13 + keyOutMemorytb14);
				numbersOfTuplesInMemorytb1 = (tupleInMemorytb11 + tupleInMemorytb12 + tupleInMemorytb13
						+ tupleInMemorytb14)
						- (tupleOutMemorytb11 + tupleOutMemorytb12 + tupleOutMemorytb13 + tupleOutMemorytb14);

				numbersOfKeysInMemorytb2 = (keyInMemorytb21 + keyInMemorytb22 + keyInMemorytb23 + keyInMemorytb24)
						- (keyOutMemorytb21 + keyOutMemorytb22 + keyOutMemorytb23 + keyOutMemorytb24);
				numbersOfTuplesInMemorytb2 = (tupleInMemorytb21 + tupleInMemorytb22 + tupleInMemorytb23
						+ tupleInMemorytb24)
						- (tupleOutMemorytb21 + tupleOutMemorytb22 + tupleOutMemorytb23 + tupleOutMemorytb24);
				usedMemory += numbersOfKeysInMemorytb1 * sincron.auxMemoryPreviusKey;
				usedMemory += numbersOfTuplesInMemorytb1 * sincron.auxMemoryPreviusTuple;
				usedMemory += numbersOfKeysInMemorytb2 * sincron.auxMemoryCurrentKey;
				usedMemory += numbersOfTuplesInMemorytb2 * sincron.auxMemoryCurrentTuple;
				if (usedMemory < calcmemory) {
					memory = true;
				}
			}
		}
		return memory;

	}

	private void sendToDisk1(HandleFile nextHaf11, int nextAtb) throws Exception {
		TreeMap<Integer, ArrayList<byte[]>> auxBucketsInMemoryToHash = new TreeMap<Integer, ArrayList<byte[]>>();
		ArrayList<byte[]> auxBucketsInMemoryInsideToHash = new ArrayList<byte[]>();
		int numblockwritten;
		ArrayList<Integer> vetOfhashResult = new ArrayList<>();

		synchronized (tb1Nextthread11) {
			synchronized (mapOfNextTable11) {
				for (Integer hashResultInMapForSmaller : tb1Nextthread11.keySet()) {
					auxBucketsInMemoryToHash = tb1Nextthread11.get(hashResultInMapForSmaller);
					vetOfhashResult = mapOfNextTable11.get(hashResultInMapForSmaller);
					if (vetOfhashResult == null) {
						vetOfhashResult = new ArrayList<>();
					}
					for (Integer hashResultInMapInside : auxBucketsInMemoryToHash.keySet()) {
						auxBucketsInMemoryInsideToHash = auxBucketsInMemoryToHash.get(hashResultInMapInside);
						for (int i = 0; i < auxBucketsInMemoryInsideToHash.size(); i++) {
							String linha = new String(auxBucketsInMemoryInsideToHash.get(i));
							numbersOfTuplesInInterMemory--;
							nextHaf11.writeTuple(linha);
							numblockwritten = nextHaf11.raf.getMaxBlockNo();
							if (!vetOfhashResult.contains(numblockwritten)) {
								vetOfhashResult.add(numblockwritten);
								mapOfNextTable11.put(hashResultInMapForSmaller, vetOfhashResult);
							}
						}
						numbersOfKeysInInterMemory--;
					}
					nextHaf11.flush();
				}
				tb1Nextthread11.clear();
			}
		}
	}

	private void sendToDisk2(HandleFile nextHaf12, int nextAtb) throws Exception {
		TreeMap<Integer, ArrayList<byte[]>> auxBucketsInMemoryToHash = new TreeMap<Integer, ArrayList<byte[]>>();
		ArrayList<byte[]> auxBucketsInMemoryInsideToHash = new ArrayList<byte[]>();
		int numblockwritten;
		ArrayList<Integer> vetOfhashResult = new ArrayList<>();
		synchronized (tb1Nextthread12) {
			synchronized (mapOfNextTable12) {
				for (Integer hashResultInMapForSmaller : tb1Nextthread12.keySet()) {
					auxBucketsInMemoryToHash = tb1Nextthread12.get(hashResultInMapForSmaller);
					vetOfhashResult = mapOfNextTable12.get(hashResultInMapForSmaller);
					if (vetOfhashResult == null) {
						vetOfhashResult = new ArrayList<>();
					}
					for (Integer hashResultInMapInside : auxBucketsInMemoryToHash.keySet()) {
						auxBucketsInMemoryInsideToHash = auxBucketsInMemoryToHash.get(hashResultInMapInside);
						for (int i = 0; i < auxBucketsInMemoryInsideToHash.size(); i++) {
							String linha = new String(auxBucketsInMemoryInsideToHash.get(i));
							numbersOfTuplesInInterMemory--;
							nextHaf12.writeTuple(linha);
							numblockwritten = nextHaf12.raf.getMaxBlockNo();

							if (!vetOfhashResult.contains(numblockwritten)) {
								vetOfhashResult.add(numblockwritten);
								mapOfNextTable12.put(hashResultInMapForSmaller, vetOfhashResult);
							}
						}
						numbersOfKeysInInterMemory--;
					}
					nextHaf12.flush();
				}
				tb1Nextthread12.clear();
			}
		}
	}

	private void sendToDisk3(HandleFile nextHaf13, int nextAtb) throws Exception {
		TreeMap<Integer, ArrayList<byte[]>> auxBucketsInMemoryToHash = new TreeMap<Integer, ArrayList<byte[]>>();
		ArrayList<byte[]> auxBucketsInMemoryInsideToHash = new ArrayList<byte[]>();
		int numblockwritten;
		ArrayList<Integer> vetOfhashResult = new ArrayList<>();
		synchronized (tb1Nextthread13) {
			synchronized (mapOfNextTable13) {
				for (Integer hashResultInMapForSmaller : tb1Nextthread13.keySet()) {
					auxBucketsInMemoryToHash = tb1Nextthread13.get(hashResultInMapForSmaller);
					vetOfhashResult = mapOfNextTable13.get(hashResultInMapForSmaller);
					if (vetOfhashResult == null) {
						vetOfhashResult = new ArrayList<>();
					}
					for (Integer hashResultInMapInside : auxBucketsInMemoryToHash.keySet()) {
						auxBucketsInMemoryInsideToHash = auxBucketsInMemoryToHash.get(hashResultInMapInside);
						for (int i = 0; i < auxBucketsInMemoryInsideToHash.size(); i++) {
							String linha = new String(auxBucketsInMemoryInsideToHash.get(i));
							numbersOfTuplesInInterMemory--;
							nextHaf13.writeTuple(linha);
							numblockwritten = nextHaf13.raf.getMaxBlockNo();

							if (!vetOfhashResult.contains(numblockwritten)) {
								vetOfhashResult.add(numblockwritten);
								mapOfNextTable13.put(hashResultInMapForSmaller, vetOfhashResult);
							}
						}
						numbersOfKeysInInterMemory--;
					}
					nextHaf13.flush();
				}
				tb1Nextthread13.clear();
			}
		}
	}

	private void sendToDisk4(HandleFile nextHaf14, int nextAtb) throws Exception {
		TreeMap<Integer, ArrayList<byte[]>> auxBucketsInMemoryToHash = new TreeMap<Integer, ArrayList<byte[]>>();
		ArrayList<byte[]> auxBucketsInMemoryInsideToHash = new ArrayList<byte[]>();
		int numblockwritten;
		ArrayList<Integer> vetOfhashResult = new ArrayList<>();
		synchronized (tb1Nextthread14) {
			synchronized (mapOfNextTable14) {
				for (Integer hashResultInMapForSmaller : tb1Nextthread14.keySet()) {
					auxBucketsInMemoryToHash = tb1Nextthread14.get(hashResultInMapForSmaller);
					vetOfhashResult = mapOfNextTable14.get(hashResultInMapForSmaller);
					if (vetOfhashResult == null) {
						vetOfhashResult = new ArrayList<>();
					}
					for (Integer hashResultInMapInside : auxBucketsInMemoryToHash.keySet()) {
						auxBucketsInMemoryInsideToHash = auxBucketsInMemoryToHash.get(hashResultInMapInside);
						for (int i = 0; i < auxBucketsInMemoryInsideToHash.size(); i++) {
							String linha = new String(auxBucketsInMemoryInsideToHash.get(i));
							numbersOfTuplesInInterMemory--;
							nextHaf14.writeTuple(linha);
							numblockwritten = nextHaf14.raf.getMaxBlockNo();

							if (!vetOfhashResult.contains(numblockwritten)) {
								vetOfhashResult.add(numblockwritten);
								mapOfNextTable14.put(hashResultInMapForSmaller, vetOfhashResult);
							}
						}
						numbersOfKeysInInterMemory--;
					}
					nextHaf14.flush();
				}
				tb1Nextthread14.clear();
			}
		}
	}

	private void addTupleMemory1Tb2(int keyJoin, byte[] tuple) {

		synchronized (tb2thread11) {
			synchronized (mapOfTable21) {
				int hashResult;
				TreeMap<Integer, ArrayList<byte[]>> auxBucketsInMemory = new TreeMap<Integer, ArrayList<byte[]>>();
				ArrayList<byte[]> auxBucketsInMemoryInside = new ArrayList<byte[]>();
				boolean isThere = false;
				hashResult = hFull.hashCode(keyJoin);
				if (tb2thread11.containsKey(hashResult)) {
					auxBucketsInMemory = tb2thread11.get(hashResult);
					// Cabe em memoria e cabe no bucket
					isThere = auxBucketsInMemory.containsKey(keyJoin);
					if (!isThere) {
						// numbersOfKeysInMemorytb2++;
						keyInMemorytb21++;
						auxBucketsInMemoryInside = new ArrayList<byte[]>();
					} else {
						auxBucketsInMemoryInside = auxBucketsInMemory.get(keyJoin);
					}
					// numbersOfTuplesInMemorytb2++;
					tupleInMemorytb21++;
					auxBucketsInMemoryInside.add(tuple);
					auxBucketsInMemory.put(keyJoin, auxBucketsInMemoryInside);
					tb2thread11.put(hashResult, auxBucketsInMemory);
				} else {
					auxBucketsInMemory = new TreeMap<Integer, ArrayList<byte[]>>();
					auxBucketsInMemoryInside = new ArrayList<byte[]>();
					auxBucketsInMemoryInside.add(tuple);
					// numbersOfTuplesInMemorytb2++;
					// numbersOfKeysInMemorytb2++;
					tupleInMemorytb21++;
					keyInMemorytb21++;
					auxBucketsInMemory.put(keyJoin, auxBucketsInMemoryInside);
					tb2thread11.put(hashResult, auxBucketsInMemory);
				}
			}
		}
	}

	private void addTupleMemory2Tb2(int keyJoin, byte[] tuple) {
		int hashResult;
		TreeMap<Integer, ArrayList<byte[]>> auxBucketsInMemory = new TreeMap<Integer, ArrayList<byte[]>>();
		ArrayList<byte[]> auxBucketsInMemoryInside = new ArrayList<byte[]>();
		boolean isThere = false;
		hashResult = hFull.hashCode(keyJoin);
		synchronized (tb2thread12) {
			synchronized (mapOfTable22) {
				if (tb2thread12.containsKey(hashResult)) {
					auxBucketsInMemory = tb2thread12.get(hashResult);
					// Cabe em memoria e cabe no bucket
					isThere = auxBucketsInMemory.containsKey(keyJoin);
					if (!isThere) {
						// numbersOfKeysInMemorytb2++;
						keyInMemorytb22++;
						auxBucketsInMemoryInside = new ArrayList<byte[]>();
					} else {
						auxBucketsInMemoryInside = auxBucketsInMemory.get(keyJoin);
					}
					// numbersOfTuplesInMemorytb2++;
					tupleInMemorytb22++;
					auxBucketsInMemoryInside.add(tuple);
					auxBucketsInMemory.put(keyJoin, auxBucketsInMemoryInside);
					tb2thread12.put(hashResult, auxBucketsInMemory);
				} else {
					auxBucketsInMemory = new TreeMap<Integer, ArrayList<byte[]>>();
					auxBucketsInMemoryInside = new ArrayList<byte[]>();
					auxBucketsInMemoryInside.add(tuple);
					// numbersOfTuplesInMemorytb2++;
					// numbersOfKeysInMemorytb2++;
					tupleInMemorytb22++;
					keyInMemorytb22++;
					auxBucketsInMemory.put(keyJoin, auxBucketsInMemoryInside);
					tb2thread12.put(hashResult, auxBucketsInMemory);
				}
			}
		}
	}

	private void addTupleMemory3Tb2(int keyJoin, byte[] tuple) {
		int hashResult;
		TreeMap<Integer, ArrayList<byte[]>> auxBucketsInMemory = new TreeMap<Integer, ArrayList<byte[]>>();
		ArrayList<byte[]> auxBucketsInMemoryInside = new ArrayList<byte[]>();
		boolean isThere = false;
		hashResult = hFull.hashCode(keyJoin);
		synchronized (tb2thread13) {
			synchronized (mapOfTable23) {
				if (tb2thread13.containsKey(hashResult)) {
					auxBucketsInMemory = tb2thread13.get(hashResult);
					// Cabe em memoria e cabe no bucket
					isThere = auxBucketsInMemory.containsKey(keyJoin);
					if (!isThere) {
						// numbersOfKeysInMemorytb2++;
						keyInMemorytb23++;
						auxBucketsInMemoryInside = new ArrayList<byte[]>();
					} else {
						auxBucketsInMemoryInside = auxBucketsInMemory.get(keyJoin);
					}
					// numbersOfTuplesInMemorytb2++;
					tupleInMemorytb23++;
					auxBucketsInMemoryInside.add(tuple);
					auxBucketsInMemory.put(keyJoin, auxBucketsInMemoryInside);
					tb2thread13.put(hashResult, auxBucketsInMemory);
				} else {
					auxBucketsInMemory = new TreeMap<Integer, ArrayList<byte[]>>();
					auxBucketsInMemoryInside = new ArrayList<byte[]>();
					auxBucketsInMemoryInside.add(tuple);
					// numbersOfTuplesInMemorytb2++;
					// numbersOfKeysInMemorytb2++;
					tupleInMemorytb23++;
					keyInMemorytb23++;
					auxBucketsInMemory.put(keyJoin, auxBucketsInMemoryInside);
					tb2thread13.put(hashResult, auxBucketsInMemory);
				}
			}
		}
	}

	private void addTupleMemory4Tb2(int keyJoin, byte[] tuple) {
		int hashResult;
		TreeMap<Integer, ArrayList<byte[]>> auxBucketsInMemory = new TreeMap<Integer, ArrayList<byte[]>>();
		ArrayList<byte[]> auxBucketsInMemoryInside = new ArrayList<byte[]>();
		boolean isThere = false;
		hashResult = hFull.hashCode(keyJoin);
		synchronized (tb2thread14) {
			synchronized (mapOfTable24) {
				if (tb2thread14.containsKey(hashResult)) {
					auxBucketsInMemory = tb2thread14.get(hashResult);
					// Cabe em memoria e cabe no bucket
					isThere = auxBucketsInMemory.containsKey(keyJoin);
					if (!isThere) {
						// numbersOfKeysInMemorytb2++;
						keyInMemorytb24++;
						auxBucketsInMemoryInside = new ArrayList<byte[]>();
					} else {
						auxBucketsInMemoryInside = auxBucketsInMemory.get(keyJoin);
					}
					// numbersOfTuplesInMemorytb2++;
					tupleInMemorytb24++;
					auxBucketsInMemoryInside.add(tuple);
					auxBucketsInMemory.put(keyJoin, auxBucketsInMemoryInside);
					tb2thread14.put(hashResult, auxBucketsInMemory);
				} else {
					auxBucketsInMemory = new TreeMap<Integer, ArrayList<byte[]>>();
					auxBucketsInMemoryInside = new ArrayList<byte[]>();
					auxBucketsInMemoryInside.add(tuple);
					keyInMemorytb24++;
					tupleInMemorytb24++;
					// numbersOfTuplesInMemorytb2++;
					// numbersOfKeysInMemorytb2++;
					auxBucketsInMemory.put(keyJoin, auxBucketsInMemoryInside);
					tb2thread14.put(hashResult, auxBucketsInMemory);
				}
			}
		}
	}

	private void addTupleMemory1Tb1(int keyJoin, byte[] tuple) {
		synchronized (tb1thread11) {
			synchronized (mapOfTable11) {
				int hashResult;
				TreeMap<Integer, ArrayList<byte[]>> auxBucketsInMemory = new TreeMap<Integer, ArrayList<byte[]>>();
				ArrayList<byte[]> auxBucketsInMemoryInside = new ArrayList<byte[]>();
				boolean isThere = false;
				hashResult = hFull.hashCode(keyJoin);
				if (tb1thread11.containsKey(hashResult)) {
					auxBucketsInMemory = tb1thread11.get(hashResult);
					// Cabe em memoria e cabe no bucket
					isThere = auxBucketsInMemory.containsKey(keyJoin);
					if (!isThere) {
						// numbersOfKeysInMemorytb1++;
						keyInMemorytb11++;
						auxBucketsInMemoryInside = new ArrayList<byte[]>();
					} else {
						auxBucketsInMemoryInside = auxBucketsInMemory.get(keyJoin);
					}
					// numbersOfTuplesInMemorytb1++;
					tupleInMemorytb11++;
					auxBucketsInMemoryInside.add(tuple);
					auxBucketsInMemory.put(keyJoin, auxBucketsInMemoryInside);
					tb1thread11.put(hashResult, auxBucketsInMemory);
				} else {
					auxBucketsInMemory = new TreeMap<Integer, ArrayList<byte[]>>();
					auxBucketsInMemoryInside = new ArrayList<byte[]>();
					auxBucketsInMemoryInside.add(tuple);
					// numbersOfTuplesInMemorytb1++;
					// numbersOfKeysInMemorytb1++;
					keyInMemorytb11++;
					tupleInMemorytb11++;
					auxBucketsInMemory.put(keyJoin, auxBucketsInMemoryInside);
					tb1thread11.put(hashResult, auxBucketsInMemory);
				}
			}
		}
	}

	private void addTupleMemory2Tb1(int keyJoin, byte[] tuple) {
		int hashResult;
		TreeMap<Integer, ArrayList<byte[]>> auxBucketsInMemory = new TreeMap<Integer, ArrayList<byte[]>>();
		ArrayList<byte[]> auxBucketsInMemoryInside = new ArrayList<byte[]>();
		boolean isThere = false;
		hashResult = hFull.hashCode(keyJoin);
		synchronized (tb1thread12) {
			synchronized (mapOfTable12) {
				if (tb1thread12.containsKey(hashResult)) {
					auxBucketsInMemory = tb1thread12.get(hashResult);
					// Cabe em memoria e cabe no bucket
					isThere = auxBucketsInMemory.containsKey(keyJoin);
					if (!isThere) {
						// numbersOfKeysInMemorytb1++;
						keyInMemorytb12++;
						auxBucketsInMemoryInside = new ArrayList<byte[]>();
					} else {
						auxBucketsInMemoryInside = auxBucketsInMemory.get(keyJoin);
					}
					// numbersOfTuplesInMemorytb1++;
					tupleInMemorytb12++;
					auxBucketsInMemoryInside.add(tuple);
					auxBucketsInMemory.put(keyJoin, auxBucketsInMemoryInside);
					tb1thread12.put(hashResult, auxBucketsInMemory);
				} else {
					auxBucketsInMemory = new TreeMap<Integer, ArrayList<byte[]>>();
					auxBucketsInMemoryInside = new ArrayList<byte[]>();
					auxBucketsInMemoryInside.add(tuple);
					// numbersOfTuplesInMemorytb1++;
					// numbersOfKeysInMemorytb1++;
					tupleInMemorytb12++;
					keyInMemorytb12++;
					auxBucketsInMemory.put(keyJoin, auxBucketsInMemoryInside);
					tb1thread12.put(hashResult, auxBucketsInMemory);
				}
			}
		}
	}

	private void addTupleMemory3Tb1(int keyJoin, byte[] tuple) {
		int hashResult;
		TreeMap<Integer, ArrayList<byte[]>> auxBucketsInMemory = new TreeMap<Integer, ArrayList<byte[]>>();
		ArrayList<byte[]> auxBucketsInMemoryInside = new ArrayList<byte[]>();
		boolean isThere = false;
		hashResult = hFull.hashCode(keyJoin);
		synchronized (tb1thread13) {
			synchronized (mapOfTable13) {
				if (tb1thread13.containsKey(hashResult)) {
					auxBucketsInMemory = tb1thread13.get(hashResult);
					// Cabe em memoria e cabe no bucket
					isThere = auxBucketsInMemory.containsKey(keyJoin);
					if (!isThere) {
						// numbersOfKeysInMemorytb1++;
						keyInMemorytb13++;
						auxBucketsInMemoryInside = new ArrayList<byte[]>();
					} else {
						auxBucketsInMemoryInside = auxBucketsInMemory.get(keyJoin);
					}
					// numbersOfTuplesInMemorytb1++;
					tupleInMemorytb13++;
					auxBucketsInMemoryInside.add(tuple);
					auxBucketsInMemory.put(keyJoin, auxBucketsInMemoryInside);
					tb1thread13.put(hashResult, auxBucketsInMemory);
				} else {
					auxBucketsInMemory = new TreeMap<Integer, ArrayList<byte[]>>();
					auxBucketsInMemoryInside = new ArrayList<byte[]>();
					auxBucketsInMemoryInside.add(tuple);
					// numbersOfTuplesInMemorytb1++;
					// numbersOfKeysInMemorytb1++;
					tupleInMemorytb13++;
					keyInMemorytb13++;
					auxBucketsInMemory.put(keyJoin, auxBucketsInMemoryInside);
					tb1thread13.put(hashResult, auxBucketsInMemory);
				}
			}
		}
	}

	private void addTupleMemory4Tb1(int keyJoin, byte[] tuple) {
		int hashResult;
		TreeMap<Integer, ArrayList<byte[]>> auxBucketsInMemory = new TreeMap<Integer, ArrayList<byte[]>>();
		ArrayList<byte[]> auxBucketsInMemoryInside = new ArrayList<byte[]>();
		boolean isThere = false;
		hashResult = hFull.hashCode(keyJoin);
		synchronized (tb1thread14) {
			synchronized (mapOfTable14) {
				if (tb1thread14.containsKey(hashResult)) {
					auxBucketsInMemory = tb1thread14.get(hashResult);
					// Cabe em memoria e cabe no bucket
					isThere = auxBucketsInMemory.containsKey(keyJoin);
					if (!isThere) {
						// numbersOfKeysInMemorytb1++;
						keyInMemorytb14++;
						auxBucketsInMemoryInside = new ArrayList<byte[]>();
					} else {
						auxBucketsInMemoryInside = auxBucketsInMemory.get(keyJoin);
					}
					// numbersOfTuplesInMemorytb1++;
					tupleInMemorytb14++;
					auxBucketsInMemoryInside.add(tuple);
					auxBucketsInMemory.put(keyJoin, auxBucketsInMemoryInside);
					tb1thread14.put(hashResult, auxBucketsInMemory);
				} else {
					auxBucketsInMemory = new TreeMap<Integer, ArrayList<byte[]>>();
					auxBucketsInMemoryInside = new ArrayList<byte[]>();
					auxBucketsInMemoryInside.add(tuple);
					// numbersOfTuplesInMemorytb1++;
					// numbersOfKeysInMemorytb1++;
					keyInMemorytb14++;
					tupleInMemorytb14++;
					auxBucketsInMemory.put(keyJoin, auxBucketsInMemoryInside);
					tb1thread14.put(hashResult, auxBucketsInMemory);
				}
			}
		}
	}

	private void addTupleDisk1Tb2(int keyJoin, byte[] tuple, HandleFile haf21) throws Exception {
		synchronized (tb2thread11) {
			synchronized (mapOfTable21) {
				int numblockwritten;
				int hashResult = hFull.hashCode(keyJoin);
				TreeMap<Integer, ArrayList<byte[]>> auxBucketsInMemory = new TreeMap<Integer, ArrayList<byte[]>>();
				ArrayList<Integer> vetOfhashResult = null;
				ArrayList<byte[]> auxBucketsInMemoryInside = new ArrayList<byte[]>();
				String linhaToAux = "";
				// HandleFile haf21 = new HandleFile(blockSize);
				// haf21.open(data + "intTb21.b");
				synchronized (haf21) {
					vetOfhashResult = mapOfTable21.get(hashResult);
					if (vetOfhashResult == null) {
						vetOfhashResult = new ArrayList<>();
					}

					if (tb2thread11.containsKey(hashResult)) {
						auxBucketsInMemory = tb2thread11.get(hashResult);
						for (Integer chave : auxBucketsInMemory.keySet()) {
							// numbersOfKeysInMemorytb2--;
							keyOutMemorytb21++;
							auxBucketsInMemoryInside = auxBucketsInMemory.get(chave);
							// exceeded memory for buckets
							// remove of memory and copy to disk
							for (int j = 0; j < auxBucketsInMemoryInside.size(); j++) {
								linhaToAux = new String(auxBucketsInMemoryInside.get(j));
								haf21.writeTuple(linhaToAux);
								// numbersOfTuplesInMemorytb2--;
								tupleOutMemorytb21++;
								linhaToAux = "";
								numblockwritten = haf21.raf.getMaxBlockNo();
								// write map of blocks
								if (!vetOfhashResult.contains(numblockwritten)) {
									vetOfhashResult.add(numblockwritten);
								}
								mapOfTable21.put(hashResult, vetOfhashResult);
							}
						}
						// Adicionando a linha tb2 em quest�o
						linhaToAux = new String(tuple);
						haf21.writeTuple(linhaToAux);
						numblockwritten = haf21.raf.getMaxBlockNo();
						haf21.flush();
						if (!vetOfhashResult.contains(numblockwritten)) {
							vetOfhashResult.add(numblockwritten);
						}
						mapOfTable21.put(hashResult, vetOfhashResult);
						tb2thread11.remove(hashResult);
					} else {
						if (vetOfhashResult.isEmpty()) {
							haf21.writeTuple(new String(tuple));
							numblockwritten = haf21.raf.getMaxBlockNo();
						} else {
							numblockwritten = haf21.writeTupleinBlock(new String(tuple),
									vetOfhashResult.get(vetOfhashResult.size() - 1));
							if (!vetOfhashResult.contains(numblockwritten)) {
								vetOfhashResult.add(numblockwritten);
							}
						}
						mapOfTable21.put(hashResult, vetOfhashResult);

						Random random = new Random();
						auxBucketsInMemory = null;
						int randomKey = 0;
						while (auxBucketsInMemory == null) {
							randomKey = random.nextInt(auxRandom);
							auxBucketsInMemory = tb2thread11.get(randomKey);
						}
						vetOfhashResult = mapOfTable21.get(randomKey);
						if (vetOfhashResult == null) {
							vetOfhashResult = new ArrayList<>();
						}
						for (Integer chave : auxBucketsInMemory.keySet()) {
							keyOutMemorytb21++;
							auxBucketsInMemoryInside = auxBucketsInMemory.get(chave);
							// exceeded memory for buckets
							// remove of memory and copy to disk
							for (int j = 0; j < auxBucketsInMemoryInside.size(); j++) {
								linhaToAux = new String(auxBucketsInMemoryInside.get(j));
								haf21.writeTuple(linhaToAux);
								tupleOutMemorytb21++;
								linhaToAux = "";
								numblockwritten = haf21.raf.getMaxBlockNo();
								// write map of blocks
								if (!vetOfhashResult.contains(numblockwritten)) {
									vetOfhashResult.add(numblockwritten);
								}
								mapOfTable21.put(randomKey, vetOfhashResult);
							}
						}
						numblockwritten = haf21.raf.getMaxBlockNo();
						haf21.flush();
						// write map of blocks
						if (!vetOfhashResult.contains(numblockwritten)) {
							vetOfhashResult.add(numblockwritten);
						}
						mapOfTable21.put(randomKey, vetOfhashResult);
						tb2thread11.remove(randomKey);
					}

				}
			}
		}
	}

	private void addTupleDisk2Tb2(int keyJoin, byte[] tuple, HandleFile haf22) throws Exception {
		synchronized (tb2thread12) {
			synchronized (mapOfTable22) {
				int numblockwritten;
				int hashResult = hFull.hashCode(keyJoin);
				TreeMap<Integer, ArrayList<byte[]>> auxBucketsInMemory = new TreeMap<Integer, ArrayList<byte[]>>();
				ArrayList<Integer> vetOfhashResult = null;
				ArrayList<byte[]> auxBucketsInMemoryInside = new ArrayList<byte[]>();
				String linhaToAux = "";
				synchronized (haf22) {
					// Enviar para disco a particao da tupla tb2
					vetOfhashResult = mapOfTable22.get(hashResult);
					if (vetOfhashResult == null) {
						vetOfhashResult = new ArrayList<>();
					}
					// Pegar em memoria a particao e enviar para disco
					if (tb2thread12.containsKey(hashResult)) {
						auxBucketsInMemory = tb2thread12.get(hashResult);
						for (Integer chave : auxBucketsInMemory.keySet()) {
							keyOutMemorytb22++;
							auxBucketsInMemoryInside = auxBucketsInMemory.get(chave);
							// exceeded memory for buckets
							// remove of memory and copy to disk
							for (int j = 0; j < auxBucketsInMemoryInside.size(); j++) {
								linhaToAux = new String(auxBucketsInMemoryInside.get(j));
								haf22.writeTuple(linhaToAux);
								tupleOutMemorytb22++;
								linhaToAux = "";
								numblockwritten = haf22.raf.getMaxBlockNo();
								// write map of blocks
								if (!vetOfhashResult.contains(numblockwritten)) {
									vetOfhashResult.add(numblockwritten);
								}
								mapOfTable22.put(hashResult, vetOfhashResult);
							}
						}
						// Adicionando a linha tb2 em quest�o
						linhaToAux = new String(tuple);
						haf22.writeTuple(linhaToAux);
						linhaToAux = "";
						numblockwritten = haf22.raf.getMaxBlockNo();
						haf22.flush();
						if (!vetOfhashResult.contains(numblockwritten)) {
							vetOfhashResult.add(numblockwritten);
						}
						mapOfTable22.put(hashResult, vetOfhashResult);

						tb2thread12.remove(hashResult);
					} else {
						if (vetOfhashResult.isEmpty()) {
							haf22.writeTuple(new String(tuple));
							numblockwritten = haf22.raf.getMaxBlockNo();
						} else {
							numblockwritten = haf22.writeTupleinBlock(new String(tuple),
									vetOfhashResult.get(vetOfhashResult.size() - 1));
							if (!vetOfhashResult.contains(numblockwritten)) {
								vetOfhashResult.add(numblockwritten);
							}
						}
						mapOfTable22.put(hashResult, vetOfhashResult);

						Random random = new Random();
						auxBucketsInMemory = null;
						int randomKey = 0;
						while (auxBucketsInMemory == null) {
							randomKey = random.nextInt(auxRandom);
							auxBucketsInMemory = tb2thread12.get(randomKey);
						}
						vetOfhashResult = mapOfTable22.get(randomKey);
						if (vetOfhashResult == null) {
							vetOfhashResult = new ArrayList<>();
						}
						for (Integer chave : auxBucketsInMemory.keySet()) {
							keyOutMemorytb22++;
							auxBucketsInMemoryInside = auxBucketsInMemory.get(chave);
							// exceeded memory for buckets
							// remove of memory and copy to disk
							for (int j = 0; j < auxBucketsInMemoryInside.size(); j++) {
								linhaToAux = new String(auxBucketsInMemoryInside.get(j));
								haf22.writeTuple(linhaToAux);
								numbersOfTuplesInMemorytb2--;
								tupleOutMemorytb22++;
								linhaToAux = "";
								numblockwritten = haf22.raf.getMaxBlockNo();
								// write map of blocks
								if (!vetOfhashResult.contains(numblockwritten)) {
									vetOfhashResult.add(numblockwritten);
								}
								mapOfTable22.put(randomKey, vetOfhashResult);
							}
						}
						numblockwritten = haf22.raf.getMaxBlockNo();
						haf22.flush();
						// write map of blocks
						if (!vetOfhashResult.contains(numblockwritten)) {
							vetOfhashResult.add(numblockwritten);
						}
						mapOfTable22.put(randomKey, vetOfhashResult);
						tb2thread12.remove(randomKey);
					}

				}
			}
		}
	}

	private void addTupleDisk3Tb2(int keyJoin, byte[] tuple, HandleFile haf23) throws Exception {
		synchronized (tb2thread13) {
			synchronized (mapOfTable23) {
				int numblockwritten;
				int hashResult = hFull.hashCode(keyJoin);
				TreeMap<Integer, ArrayList<byte[]>> auxBucketsInMemory = new TreeMap<Integer, ArrayList<byte[]>>();
				ArrayList<Integer> vetOfhashResult = null;
				ArrayList<byte[]> auxBucketsInMemoryInside = new ArrayList<byte[]>();
				String linhaToAux = "";
				synchronized (haf23) {
					// Enviar para disco a particao da tupla tb2
					vetOfhashResult = mapOfTable23.get(hashResult);
					if (vetOfhashResult == null) {
						vetOfhashResult = new ArrayList<>();
					}
					// Pegar em memoria a particao e enviar para disco

					if (tb2thread13.containsKey(hashResult)) {
						auxBucketsInMemory = tb2thread13.get(hashResult);
						for (Integer chave : auxBucketsInMemory.keySet()) {
							numbersOfKeysInMemorytb2--;
							keyOutMemorytb23++;
							auxBucketsInMemoryInside = auxBucketsInMemory.get(chave);
							// exceeded memory for buckets
							// remove of memory and copy to disk
							for (int j = 0; j < auxBucketsInMemoryInside.size(); j++) {
								linhaToAux = new String(auxBucketsInMemoryInside.get(j));
								haf23.writeTuple(linhaToAux);
								tupleOutMemorytb23++;
								linhaToAux = "";
								numblockwritten = haf23.raf.getMaxBlockNo();
								// write map of blocks
								if (!vetOfhashResult.contains(numblockwritten)) {
									vetOfhashResult.add(numblockwritten);
								}
								mapOfTable23.put(hashResult, vetOfhashResult);
							}
						}
						// Adicionando a linha tb2 em quest�o
						linhaToAux = new String(tuple);
						haf23.writeTuple(linhaToAux);
						linhaToAux = "";
						numblockwritten = haf23.raf.getMaxBlockNo();
						haf23.flush();
						if (!vetOfhashResult.contains(numblockwritten)) {
							vetOfhashResult.add(numblockwritten);
						}
						mapOfTable23.put(hashResult, vetOfhashResult);

						tb2thread13.remove(hashResult);
					} else {

						if (vetOfhashResult.isEmpty()) {
							haf23.writeTuple(new String(tuple));
							numblockwritten = haf23.raf.getMaxBlockNo();
						} else {
							numblockwritten = haf23.writeTupleinBlock(new String(tuple),
									vetOfhashResult.get(vetOfhashResult.size() - 1));
						}
						if (!vetOfhashResult.contains(numblockwritten)) {
							vetOfhashResult.add(numblockwritten);
						}
						mapOfTable23.put(hashResult, vetOfhashResult);

						Random random = new Random();
						auxBucketsInMemory = null;
						int randomKey = 0;
						while (auxBucketsInMemory == null) {
							randomKey = random.nextInt(auxRandom);
							auxBucketsInMemory = tb2thread13.get(randomKey);
						}
						vetOfhashResult = mapOfTable23.get(randomKey);
						if (vetOfhashResult == null) {
							vetOfhashResult = new ArrayList<>();
						}
						for (Integer chave : auxBucketsInMemory.keySet()) {
							numbersOfKeysInMemorytb2--;
							keyOutMemorytb23++;
							auxBucketsInMemoryInside = auxBucketsInMemory.get(chave);
							// exceeded memory for buckets
							// remove of memory and copy to disk
							for (int j = 0; j < auxBucketsInMemoryInside.size(); j++) {
								linhaToAux = new String(auxBucketsInMemoryInside.get(j));
								haf23.writeTuple(linhaToAux);
								numbersOfTuplesInMemorytb2--;
								tupleOutMemorytb23++;
								linhaToAux = "";
								numblockwritten = haf23.raf.getMaxBlockNo();
								// write map of blocks
								if (!vetOfhashResult.contains(numblockwritten)) {
									vetOfhashResult.add(numblockwritten);
								}
								mapOfTable23.put(randomKey, vetOfhashResult);
							}
						}
						numblockwritten = haf23.raf.getMaxBlockNo();
						haf23.flush();
						// write map of blocks
						if (!vetOfhashResult.contains(numblockwritten)) {
							vetOfhashResult.add(numblockwritten);
						}
						mapOfTable23.put(randomKey, vetOfhashResult);
						tb2thread13.remove(randomKey);
					}

				}
			}
		}
	}

	private void addTupleDisk4Tb2(int keyJoin, byte[] tuple, HandleFile haf24) throws Exception {
		synchronized (tb2thread14) {
			synchronized (mapOfTable24) {
				int numblockwritten;
				int hashResult = hFull.hashCode(keyJoin);
				TreeMap<Integer, ArrayList<byte[]>> auxBucketsInMemory = new TreeMap<Integer, ArrayList<byte[]>>();
				ArrayList<Integer> vetOfhashResult = null;
				ArrayList<byte[]> auxBucketsInMemoryInside = new ArrayList<byte[]>();
				String linhaToAux = "";
				synchronized (haf24) {
					// Enviar para disco a particao da tupla tb2
					// hashResult = hFull.hashCode(keyJoin);
					vetOfhashResult = mapOfTable24.get(hashResult);
					if (vetOfhashResult == null) {
						vetOfhashResult = new ArrayList<>();
					}
					// Pegar em memoria a particao e enviar para disco
					if (tb2thread14.containsKey(hashResult)) {
						auxBucketsInMemory = tb2thread14.get(hashResult);
						for (Integer chave : auxBucketsInMemory.keySet()) {
							keyOutMemorytb24++;
							auxBucketsInMemoryInside = auxBucketsInMemory.get(chave);
							// exceeded memory for buckets
							// remove of memory and copy to disk
							for (int j = 0; j < auxBucketsInMemoryInside.size(); j++) {
								linhaToAux = new String(auxBucketsInMemoryInside.get(j));
								haf24.writeTuple(linhaToAux);
								tupleOutMemorytb24++;
								linhaToAux = "";
								numblockwritten = haf24.raf.getMaxBlockNo();
								// write map of blocks
								if (!vetOfhashResult.contains(numblockwritten)) {
									vetOfhashResult.add(numblockwritten);
								}
								mapOfTable24.put(hashResult, vetOfhashResult);
							}
						}
						// Adicionando a linha tb2 em quest�o
						linhaToAux = new String(tuple);
						haf24.writeTuple(linhaToAux);
						linhaToAux = "";
						numblockwritten = haf24.raf.getMaxBlockNo();
						haf24.flush();
						if (!vetOfhashResult.contains(numblockwritten)) {
							vetOfhashResult.add(numblockwritten);
						}
						mapOfTable24.put(hashResult, vetOfhashResult);

						tb2thread14.remove(hashResult);
					} else {

						if (vetOfhashResult.isEmpty()) {
							haf24.writeTuple(new String(tuple));
							numblockwritten = haf24.raf.getMaxBlockNo();
						} else {
							numblockwritten = haf24.writeTupleinBlock(new String(tuple),
									vetOfhashResult.get(vetOfhashResult.size() - 1));
						}
						if (!vetOfhashResult.contains(numblockwritten)) {
							vetOfhashResult.add(numblockwritten);
						}
						mapOfTable24.put(hashResult, vetOfhashResult);

						Random random = new Random();
						auxBucketsInMemory = null;
						int randomKey = 0;
						while (auxBucketsInMemory == null) {
							randomKey = random.nextInt(auxRandom);
							auxBucketsInMemory = tb2thread14.get(randomKey);
						}
						vetOfhashResult = mapOfTable24.get(randomKey);
						if (vetOfhashResult == null) {
							vetOfhashResult = new ArrayList<>();
						}
						for (Integer chave : auxBucketsInMemory.keySet()) {
							keyOutMemorytb24++;
							auxBucketsInMemoryInside = auxBucketsInMemory.get(chave);
							// exceeded memory for buckets
							// remove of memory and copy to disk
							for (int j = 0; j < auxBucketsInMemoryInside.size(); j++) {
								linhaToAux = new String(auxBucketsInMemoryInside.get(j));
								haf24.writeTuple(linhaToAux);
								tupleOutMemorytb24++;
								linhaToAux = "";
								numblockwritten = haf24.raf.getMaxBlockNo();
								// write map of blocks
								if (!vetOfhashResult.contains(numblockwritten)) {
									vetOfhashResult.add(numblockwritten);
								}
								mapOfTable24.put(randomKey, vetOfhashResult);
							}
						}
						numblockwritten = haf24.raf.getMaxBlockNo();
						haf24.flush();
						// write map of blocks
						if (!vetOfhashResult.contains(numblockwritten)) {
							vetOfhashResult.add(numblockwritten);
						}
						mapOfTable24.put(randomKey, vetOfhashResult);
						tb2thread14.remove(randomKey);
					}

				}
			}
		}
	}

	private void addTupleDisk1Tb1(int keyJoin, byte[] tuple, HandleFile haf11) throws Exception {
		int numblockwritten;
		int hashResult = hFull.hashCode(keyJoin);
		TreeMap<Integer, ArrayList<byte[]>> auxBucketsInMemory = new TreeMap<Integer, ArrayList<byte[]>>();
		ArrayList<Integer> vetOfhashResult = null;
		ArrayList<byte[]> auxBucketsInMemoryInside = new ArrayList<byte[]>();
		String linhaToAux = "";
		synchronized (tb1thread11) {
			synchronized (mapOfTable11) {
				// Verificando as que estavam em mem�ria
				if (tb1thread11.containsKey(hashResult)) {
					auxBucketsInMemory = tb1thread11.get(hashResult);
					vetOfhashResult = mapOfTable11.get(hashResult);
					if (vetOfhashResult == null) {
						vetOfhashResult = new ArrayList<>();
					}
					for (Integer chave : auxBucketsInMemory.keySet()) {
						keyOutMemorytb11++;
						auxBucketsInMemoryInside = auxBucketsInMemory.get(chave);
						// exceeded memory for buckets
						// remove of memory and copy to disk
						for (int j = 0; j < auxBucketsInMemoryInside.size(); j++) {
							linhaToAux = new String(auxBucketsInMemoryInside.get(j));
							tupleOutMemorytb11++;
							haf11.writeTuple(linhaToAux);
							linhaToAux = "";
							numblockwritten = haf11.raf.getMaxBlockNo();
							// write map of blocks
							if (!vetOfhashResult.contains(numblockwritten)) {
								vetOfhashResult.add(numblockwritten);
							}
							mapOfTable11.put(hashResult, vetOfhashResult);
						}
					}
				}
				// Adicionando a linha em quest�o
				linhaToAux = new String(tuple);
				haf11.writeTuple(linhaToAux);
				numblockwritten = haf11.raf.getMaxBlockNo();
				haf11.flush();
				if (vetOfhashResult == null) {
					vetOfhashResult = new ArrayList<>();
				}
				if (!vetOfhashResult.contains(numblockwritten)) {
					vetOfhashResult.add(numblockwritten);
				}
				mapOfTable11.put(hashResult, vetOfhashResult);
				tb1thread11.remove(hashResult);
			}
		}
	}

	private void addTupleDisk2Tb1(int keyJoin, byte[] tuple, HandleFile haf12) throws Exception {
		int numblockwritten;
		int hashResult = hFull.hashCode(keyJoin);
		TreeMap<Integer, ArrayList<byte[]>> auxBucketsInMemory = new TreeMap<Integer, ArrayList<byte[]>>();
		ArrayList<Integer> vetOfhashResult = null;
		ArrayList<byte[]> auxBucketsInMemoryInside = new ArrayList<byte[]>();
		String linhaToAux = "";
		synchronized (tb1thread12) {
			synchronized (mapOfTable12) {
				// Verificando as que estavam em mem�ria
				if (tb1thread12.containsKey(hashResult)) {
					auxBucketsInMemory = tb1thread12.get(hashResult);
					vetOfhashResult = mapOfTable12.get(hashResult);
					if (vetOfhashResult == null) {
						vetOfhashResult = new ArrayList<>();
					}
					for (Integer chave : auxBucketsInMemory.keySet()) {
						keyOutMemorytb12++;
						auxBucketsInMemoryInside = auxBucketsInMemory.get(chave);
						// exceeded memory for buckets
						// remove of memory and copy to disk
						for (int j = 0; j < auxBucketsInMemoryInside.size(); j++) {
							linhaToAux = new String(auxBucketsInMemoryInside.get(j));
							tupleOutMemorytb12++;
							haf12.writeTuple(linhaToAux);
							linhaToAux = "";
							numblockwritten = haf12.raf.getMaxBlockNo();
							// write map of blocks
							if (!vetOfhashResult.contains(numblockwritten)) {
								vetOfhashResult.add(numblockwritten);
							}
							mapOfTable12.put(hashResult, vetOfhashResult);
						}
					}
				}
				// Adicionando a linha em quest�o
				linhaToAux = new String(tuple);
				haf12.writeTuple(linhaToAux);
				numblockwritten = haf12.raf.getMaxBlockNo();
				haf12.flush();
				if (vetOfhashResult == null) {
					vetOfhashResult = new ArrayList<>();
				}
				if (!vetOfhashResult.contains(numblockwritten)) {
					vetOfhashResult.add(numblockwritten);
				}
				mapOfTable12.put(hashResult, vetOfhashResult);
				tb1thread12.remove(hashResult);
			}
		}
	}

	private void addTupleDisk3Tb1(int keyJoin, byte[] tuple, HandleFile haf13) throws Exception {
		int numblockwritten;
		int hashResult = hFull.hashCode(keyJoin);
		TreeMap<Integer, ArrayList<byte[]>> auxBucketsInMemory = new TreeMap<Integer, ArrayList<byte[]>>();
		ArrayList<Integer> vetOfhashResult = null;
		ArrayList<byte[]> auxBucketsInMemoryInside = new ArrayList<byte[]>();
		String linhaToAux = "";
		synchronized (tb1thread13) {
			synchronized (mapOfTable13) {
				// Verificando as que estavam em mem�ria
				if (tb1thread13.containsKey(hashResult)) {
					auxBucketsInMemory = tb1thread13.get(hashResult);
					vetOfhashResult = mapOfTable13.get(hashResult);
					if (vetOfhashResult == null) {
						vetOfhashResult = new ArrayList<>();
					}
					for (Integer chave : auxBucketsInMemory.keySet()) {
						keyOutMemorytb13++;
						auxBucketsInMemoryInside = auxBucketsInMemory.get(chave);
						// exceeded memory for buckets
						// remove of memory and copy to disk
						for (int j = 0; j < auxBucketsInMemoryInside.size(); j++) {
							linhaToAux = new String(auxBucketsInMemoryInside.get(j));
							tupleOutMemorytb13++;
							haf13.writeTuple(linhaToAux);
							linhaToAux = "";
							numblockwritten = haf13.raf.getMaxBlockNo();
							// write map of blocks
							if (!vetOfhashResult.contains(numblockwritten)) {
								vetOfhashResult.add(numblockwritten);
							}
							mapOfTable13.put(hashResult, vetOfhashResult);
						}
					}
				}
				// Adicionando a linha em quest�o
				linhaToAux = new String(tuple);
				haf13.writeTuple(linhaToAux);
				numblockwritten = haf13.raf.getMaxBlockNo();
				haf13.flush();
				if (vetOfhashResult == null) {
					vetOfhashResult = new ArrayList<>();
				}
				if (!vetOfhashResult.contains(numblockwritten)) {
					vetOfhashResult.add(numblockwritten);
				}
				mapOfTable13.put(hashResult, vetOfhashResult);
				tb1thread13.remove(hashResult);
			}
		}
	}

	private void addTupleDisk4Tb1(int keyJoin, byte[] tuple, HandleFile haf14) throws Exception {
		synchronized (tb1thread14) {
			synchronized (mapOfTable14) {
				int numblockwritten;
				int hashResult = hFull.hashCode(keyJoin);
				TreeMap<Integer, ArrayList<byte[]>> auxBucketsInMemory = new TreeMap<Integer, ArrayList<byte[]>>();
				ArrayList<Integer> vetOfhashResult = null;
				ArrayList<byte[]> auxBucketsInMemoryInside = new ArrayList<byte[]>();
				String linhaToAux = "";

				// Verificando as que estavam em mem�ria
				if (tb1thread14.containsKey(hashResult)) {
					auxBucketsInMemory = tb1thread14.get(hashResult);
					vetOfhashResult = mapOfTable14.get(hashResult);
					if (vetOfhashResult == null) {
						vetOfhashResult = new ArrayList<>();
					}
					for (Integer chave : auxBucketsInMemory.keySet()) {
						keyOutMemorytb13++;
						auxBucketsInMemoryInside = auxBucketsInMemory.get(chave);
						// exceeded memory for buckets
						// remove of memory and copy to disk
						for (int j = 0; j < auxBucketsInMemoryInside.size(); j++) {
							linhaToAux = new String(auxBucketsInMemoryInside.get(j));
							tupleOutMemorytb13++;
							haf14.writeTuple(linhaToAux);
							linhaToAux = "";
							numblockwritten = haf14.raf.getMaxBlockNo();
							// write map of blocks
							if (!vetOfhashResult.contains(numblockwritten)) {
								vetOfhashResult.add(numblockwritten);
							}
							mapOfTable14.put(hashResult, vetOfhashResult);
						}
					}
				}
				// Adicionando a linha em quest�o
				linhaToAux = new String(tuple);
				haf14.writeTuple(linhaToAux);
				numblockwritten = haf14.raf.getMaxBlockNo();
				haf14.flush();
				if (vetOfhashResult == null) {
					vetOfhashResult = new ArrayList<>();
				}
				if (!vetOfhashResult.contains(numblockwritten)) {
					vetOfhashResult.add(numblockwritten);
				}
				mapOfTable14.put(hashResult, vetOfhashResult);
				tb1thread14.remove(hashResult);
			}
		}
	}
}