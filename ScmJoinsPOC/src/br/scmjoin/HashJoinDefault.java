package br.scmjoin;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Random;

public class HashJoinDefault {

	static Random generator = new Random();
	static int a, b, hashTableSize, p;

	public static int buckets = 0;
	public static String smaller = "";
	public static String bigger = "";
	public static String[] colSmaller;
	public static String[] colBigger;
	public static int blockSize = 8192;
	public static int pageSize = 8192;

	private static LogFile resultlog;
	private static boolean stepStop;
	
	// Compute the number of buckets
	// Find and set the smaller and bigger table and their join columns
	// Formula: number of block of smaller table/(pageSize / blockSize)) + 1
	public int calcMaxAndSmallerTable(String table1, String table2,
			String[] joinColumnsSecondRelation,
			String[] joinColumnsFirstRelation) throws Exception {
		int max;
		HandleFile haf = new HandleFile(blockSize);
		haf.open(table1);
		int numbersOfBlockstoTable1 = haf.numberOfBlocks;
		int numbersOfTuplesTable1 = haf.numberOfTuples;
		haf.close();
		haf.open(table2);
		int numbersOfBlockstoTable2 = haf.numberOfBlocks;
		int numbersOfTuplesTable2 = haf.numberOfTuples;
		haf.close();
		// which table has less tuples
		if (numbersOfTuplesTable1 > numbersOfTuplesTable2) {
			max = (numbersOfBlockstoTable2 / (pageSize / blockSize)) + 1;
			smaller = table2;
			colSmaller = joinColumnsFirstRelation;
			bigger = table1;
			colBigger = joinColumnsSecondRelation;

		} else {
			max = (numbersOfBlockstoTable1 / (pageSize / blockSize)) + 1;
			smaller = table1;
			colSmaller = joinColumnsSecondRelation;
			bigger = table2;
			colBigger = joinColumnsFirstRelation;

		}
		return max;
	}

	public void hashJoin(String table1, String[] joinColumnsSecondRelation,
			String table2, String[] joinColumnsFirstRelation,
			String hashfilesmaller, String hashfilebigger, String finalhashfile)
			throws Exception {
		
		Measure.setStart();
		// Map of hash(key), array of blockNo for smaller and bigger table
		HashMap<Integer, ArrayList<Integer>> mapOfTableSmaller = new HashMap<Integer, ArrayList<Integer>>();
		HashMap<Integer, ArrayList<Integer>> mapOfTableBigger = new HashMap<Integer, ArrayList<Integer>>();

		ArrayList<Integer> vetOfhashResult;
		ArrayList<Integer> vetOfhashResultAuxBigger;
		boolean isThere;
		int numblockwritten;
		int numblockreaded;
		int numblockreadedAuxBigger;
		int hashResult = 0;

		int atribColHashIndice = 0;
		int currentTupleId = 0;
		int keyHashMapSmaller = 0;

		byte block[] = new byte[blockSize];
		byte blockAuxBigger[] = new byte[blockSize];
		byte tupleBlock[];
		byte tupleBlockAuxBigger[];
		int indCol = 0, keyJoin = 0, indColAuxBigger = 0;
		String header = "";
		String line = "";
		String lineAuxBigger = "";

		// number of buckets and setting smaller and bigger table
		buckets = calcMaxAndSmallerTable(table1, table2,
				joinColumnsSecondRelation, joinColumnsFirstRelation);
	
		HashFunction.initialize(buckets);

		resultlog.writeLog("Generating hash table for smaller table");
		HandleFile haf = new HandleFile(blockSize);
		haf.open(smaller);
		indCol = haf.getColumnPos(colSmaller[0]);
		header = RafIOCalc.getHeaderString(haf);
		HandleFile haf2 = new HandleFile(blockSize);
		haf2.create(hashfilesmaller, header);

		// Generating the hash table for smaller table
		block = haf.nextBlock();
		while (block != null) {
			tupleBlock = haf.nextTuple(block);
			line = RafIOCalc.getLineString(haf, tupleBlock, haf.getQtCols());
			while (line != null) {
				keyJoin = RafIOCalc.getKey(tupleBlock, indCol, haf.getQtCols());
				hashResult = HashFunction.hashCode(keyJoin);

				// mapOfTableSmaller(hashResult, block numbers)
				isThere = mapOfTableSmaller.containsKey(hashResult);

				// if hashResult not in mapOfTableSmaller
				// add to map with number of block
				if (!isThere) {
					vetOfhashResult = new ArrayList<>();
					haf2.writeTuple(line);
					haf2.flush();
					numblockwritten = haf2.raf.getMaxBlockNo() - 1;
					vetOfhashResult.add(numblockwritten);
					mapOfTableSmaller.put(hashResult, vetOfhashResult);
				} else {
					// if hashResult is in map, read last block written
					// write the tuple and check if block written is the same
					vetOfhashResult = mapOfTableSmaller.get(hashResult);
					numblockwritten = haf2.writeTupleinBlock(line,
							vetOfhashResult.get(vetOfhashResult.size() - 1));
					if (!vetOfhashResult.contains(numblockwritten)) {
						vetOfhashResult.add(numblockwritten);
						mapOfTableSmaller.put(hashResult, vetOfhashResult);
					}
				}

				tupleBlock = haf.nextTuple(block);
				if (tupleBlock != null) {
					line = RafIOCalc.getLineString(haf, tupleBlock,
							haf.getQtCols());
				} else {
					line = null;
				}

			}
			block = haf.nextBlock();
		}
		haf = null;
		haf2 = null;

		resultlog.writeLog("Generating hash table for bigger table");

		haf = new HandleFile(blockSize);
		haf.open(bigger);
		indCol = haf.getColumnPos(colBigger[0]);
		header = RafIOCalc.getHeaderString(haf);
		haf2 = new HandleFile(blockSize);
		haf2.create(hashfilebigger, header);
		block = haf.nextBlock();

		while (block != null) {
			tupleBlock = haf.nextTuple(block);
			line = RafIOCalc.getLineString(haf, tupleBlock, haf.getQtCols());
			while (line != null) {
				keyJoin = RafIOCalc.getKey(tupleBlock, indCol, haf.getQtCols());
				hashResult = HashFunction.hashCode(keyJoin);
				isThere = mapOfTableBigger.containsKey(hashResult);

				if (!isThere) {
					vetOfhashResult = new ArrayList<>();
					haf2.writeTuple(line);
					haf2.flush();
					numblockwritten = haf2.raf.getMaxBlockNo() - 1;
					vetOfhashResult.add(numblockwritten);
					mapOfTableBigger.put(hashResult, vetOfhashResult);
				} else {
					vetOfhashResult = mapOfTableBigger.get(hashResult);
					numblockwritten = haf2.writeTupleinBlock(line,
							vetOfhashResult.get(vetOfhashResult.size() - 1));
					if (!vetOfhashResult.contains(numblockwritten)) {
						vetOfhashResult.add(numblockwritten);
						mapOfTableBigger.put(hashResult, vetOfhashResult);
					}
				}

				tupleBlock = haf.nextTuple(block);
				if (tupleBlock != null) {
					line = RafIOCalc.getLineString(haf, tupleBlock,
							haf.getQtCols());
				} else {
					line = null;
				}

			}
			block = haf.nextBlock();
		}

		haf.close();
		haf2.close();
		haf = null;
		haf2 = null;

		showTable(hashfilesmaller, colSmaller);

		showTable(hashfilebigger, colBigger);

		// Join
		// Generate hash index using hashMap

		HashMap<ByteBuffer, ArrayList<Integer>> hashIndice = new HashMap<ByteBuffer, ArrayList<Integer>>();
		ByteBuffer bb;

		haf = new HandleFile(blockSize);
		haf.open(hashfilesmaller);
		indCol = haf.getColumnPos(colSmaller[0]);

		haf2 = new HandleFile(blockSize);
		haf2.open(hashfilebigger);
		indColAuxBigger = haf2.getColumnPos(colBigger[0]);

		// Final Hash file
		HandleFile haf3 = new HandleFile(blockSize);
		// Header = header of small table + header of bigger table
		header = RafIOCalc.getHeaderString(haf);
		header = header + RafIOCalc.getHeaderString(haf2);
		haf3.create(finalhashfile, header);

		// Arrays to help join by more than one key
		byte[] keysSmaller;
		byte[] keysBigger;
		
		int[] indJoinColSmaller = new int[colSmaller.length];

		for (int i = 0; i < indJoinColSmaller.length; i++) {
			indJoinColSmaller[i] = haf.getColumnPos(colSmaller[i]);
		}
		
		int[] indJoinColBigger = new int[colBigger.length];

		for (int i = 0; i < indJoinColBigger.length; i++) {
			indJoinColBigger[i] = haf2.getColumnPos(colBigger[i]);
		}


		resultlog.writeLog("Generating hash index for smaller table and making join");
		// Join: for each entry in small table map
		// add entry in hash index
		// for each blockNo
		for (Entry<Integer, ArrayList<Integer>> entry : mapOfTableSmaller
				.entrySet()) {

			// hash(key)
			keyHashMapSmaller = entry.getKey();
			// blockNo of tuples of this hash
			vetOfhashResult = entry.getValue();

			// for each blockNo
			ArrayList<Integer> auxHash = new ArrayList<>();
			for (int i = 0; i < vetOfhashResult.size(); i++) {

				numblockreaded = vetOfhashResult.get(i);

				block = haf.readBlock(numblockreaded);
				tupleBlock = haf.nextTuple(block);

				if (tupleBlock != null) {
					line = RafIOCalc.getLineString(haf, tupleBlock,
							haf.getQtCols());
				} else {
					line = null;
				}
				while (line != null) {
					keysSmaller = RafIOCalc.getKeys(tupleBlock,
							indJoinColSmaller, haf.getQtCols());

					bb = ByteBuffer.wrap(keysSmaller);

					currentTupleId = haf.getCurrentTupleId();

					isThere = hashIndice.containsKey(atribColHashIndice);
					if (!isThere) {
						auxHash = new ArrayList<>();
						auxHash.add(numblockreaded);
						auxHash.add(currentTupleId);
						hashIndice.put(bb, auxHash);
					} else {
						auxHash = hashIndice.get(atribColHashIndice);
						auxHash.add(numblockreaded);
						auxHash.add(currentTupleId);
						hashIndice.put(bb, auxHash);
					}

					tupleBlock = haf.nextTuple(block);

					if (tupleBlock != null) {
						line = RafIOCalc.getLineString(haf, tupleBlock,
								haf.getQtCols());
					} else {
						line = null;
					}
				}
			}

			// System.out.println(hashIndice);
			// Chech if keyHashMapSmaller exists on mapOfTableBigger
			if (mapOfTableBigger.containsKey(keyHashMapSmaller)) {
				vetOfhashResultAuxBigger = mapOfTableBigger
						.get(keyHashMapSmaller);
				for (int i = 0; i < vetOfhashResultAuxBigger.size(); i++) {
					numblockreadedAuxBigger = vetOfhashResultAuxBigger.get(i);

					// Nao sei pq tenho que abrir toda vez
					// haf2.open("C:/RAFIO/table_bigger_temp.b");
					blockAuxBigger = haf2.readBlock(numblockreadedAuxBigger);

					tupleBlockAuxBigger = haf2.nextTuple(blockAuxBigger);

					if (tupleBlockAuxBigger != null) {
						lineAuxBigger = RafIOCalc.getLineString(haf2,
								tupleBlockAuxBigger, haf2.getQtCols());
					} else {
						lineAuxBigger = null;
					}
					// System.out.println(lineAuxBigger);
					while (lineAuxBigger != null) {
						keysBigger = RafIOCalc.getKeys(tupleBlockAuxBigger,
								indJoinColBigger, haf2.getQtCols());

						bb = ByteBuffer.wrap(keysBigger);

						if (hashIndice.containsKey(bb)) {

							auxHash = hashIndice.get(bb);

							for (int j = 0; j < auxHash.size(); j = j + 2) {
								block = haf.raf.readBlock(auxHash.get(j));
								tupleBlock = haf.readTupleById(block,
										auxHash.get(j + 1));
								line = RafIOCalc.getLineString(haf,
										tupleBlock, haf.getQtCols());

								line = line + lineAuxBigger;

								haf3.writeTuple(line);
							}

						}
						tupleBlockAuxBigger = haf2.nextTuple(blockAuxBigger);
						if (tupleBlockAuxBigger != null) {
							lineAuxBigger = RafIOCalc.getLineString(haf2,
									tupleBlockAuxBigger, haf2.getQtCols());
						} else {
							lineAuxBigger = null;
						}
					}

				}

			}
			hashIndice.clear();
		}

		haf3.flush();
		haf3.close();

//		showTable(finalhashfile, colSmaller);

		resultlog.writeLog("End of Hash Join Default");
		resultlog.writeLog("ElapsedTime for join " + table1 + "x" + table2 + " - total time:" + Measure.getElapsedTime()); 
		resultlog.writeLog("Used Memory:" + Measure.getMemory());

	}

	

	private void showTable(String filename, String[] colSmaller2) throws Exception {
		int key, indCol = 0, qtLines = 0;
		byte[] rowid;
		String rowidS;

		System.out.println("Log file for " + filename);
		HandleFile haf = new HandleFile(blockSize);
		haf.open(filename);
//		indCol = haf.getColumnPos(colSmaller2);

		LogFile log = new LogFile();

		String header = RafIOCalc.getHeaderString(haf);
		log.writeLog(header);

		String line;
		byte block[] = new byte[blockSize];
		byte tupleBlock[];

		block = haf.nextBlock();
		log.writeLog("bytes used in this block:" + haf.raf.getBlockBytesUsed());
		while (block != null) {
			tupleBlock = haf.nextTuple(block);
			line = RafIOCalc.getLineString(haf, tupleBlock, haf.getQtCols());
			while (line != null) {
				key = RafIOCalc.getKey(tupleBlock, indCol, haf.getQtCols());
				rowid = haf.getRowid(indCol);

				rowidS = RafIOCalc.getRowid(rowid);
				log.writeLog(line + " chave:" + key + " rowid:" + rowidS);
				qtLines++;
				tupleBlock = haf.nextTuple(block);
				if (tupleBlock != null)
					line = RafIOCalc.getLineString(haf, tupleBlock,
							haf.getQtCols());
				else
					line = null;
			}
			block = haf.nextBlock();
		}
		log.writeLog("Qtd Lines:" + qtLines);
		log.closeLog();

	}

	public static void main(String[] args) {
		
		
		String directory="";
		/*args = new String[10];
		args[1] = "1";
		args[2] = "resultdh.txt";
		args[3] = "4096";
		args[4] = "4096";
		args[5] = null;
		args[6] = "1";
		args[7] = "false";*/
		
		String[] joinColumnsFirstRelation;
	    String[] joinColumnsSecondRelation;
				
		
		directory = "C:/RAFIO/TPCH_1/";
        if (args[1].equals("10"))
        	directory = "C:/RAFIO/TPCH_10/";
        
        resultlog = new LogFile(args[2]);	
        HashJoinDefault.blockSize = Integer.parseInt(args[3]);
        HashJoinDefault.pageSize  = Integer.parseInt(args[4]);
        
		
		if (args[7].equals("true"))
			stepStop = true;
		else
			stepStop = false;
		
		resultlog.writeLog("DefaultHashJoin");
    	resultlog.writeLog("tpchType = " + args[1]);
    	resultlog.writeLog("fileResultName = " + args[2]);
    	resultlog.writeLog("blockSize = " + args[3]);
		resultlog.writeLog("pageSize = " + args[4]);
		resultlog.writeLog("join = " + args[6]);
		resultlog.writeLog("stepStop = " + args[7]);
		
		
        
		try {
	        if (args[6].equals("1")) {
	        	/* select p_name, suppkey, ps_availqty, l_quantity, l_linestatus, l_comment
	        	 * from part, partsupp, lineitem
	        	 * where part.partkey = partsupp.partkey
	        	 * and lineitem.partkey = part.partkey
	        	*/
				
	        	resultlog.writeLog("select p_name, suppkey, ps_availqty, l_quantity, l_linestatus, l_comment " +
	        					   " from part, partsupp, lineitem " +
	        					   " where part.partkey = partsupp.partkey " +
	        					   " and lineitem.partkey = part.partkey");        	
	        	HashJoinDefault hj = new HashJoinDefault();
				joinColumnsFirstRelation = new String[1];
				joinColumnsFirstRelation[0]= "partkey";
				joinColumnsSecondRelation = new String[1];
				joinColumnsSecondRelation[0]="partkey";
	        	hj.hashJoin(directory + "part.b", joinColumnsFirstRelation,
	        			directory + "partsupp.b", joinColumnsSecondRelation,
	        			directory + "table_smaller_temp.b",
	        			directory + "table_bigger_temp.b",
	        			directory + "table_HashJoin.b");	
	        	hj = null;
	        	hj = new HashJoinDefault();
	        	hj.hashJoin(directory + "table_HashJoin.b", joinColumnsFirstRelation,
	        			directory + "lineitem.b", joinColumnsSecondRelation,
	        			directory + "table_smaller_temp.b",
	        			directory + "table_bigger_temp.b",
	        			directory + "table_HashJoin2.b");
	        }
	        if (args[6].equals("2")) {
	        	/* select p_name, suppkey, ps_availqty, l_quantity, l_linestatus, l_comment
	        	 * from part, partsupp, lineitem
	        	 * where part.partkey = partsupp.partkey
	        	 * and lineitem.partkey = part.partkey
	        	 * and lineitem.suppkey = partsupp.suppkey
	        	*/
	        	resultlog.writeLog("select p_name, suppkey, ps_availqty, l_quantity, l_linestatus, l_comment " +
	        	 " from part, partsupp, lineitem " + 
	        	 " where part.partkey = partsupp.partkey" + 
	        	 " and lineitem.partkey = part.partkey" + 
	        	 " and lineitem.suppkey = partsupp.suppkey");
	        	
	        	HashJoinDefault hj = new HashJoinDefault();
				joinColumnsFirstRelation = new String[1];
				joinColumnsFirstRelation[0]= "partkey";
				joinColumnsSecondRelation = new String[1];
				joinColumnsSecondRelation[0]="partkey";
				hj.hashJoin(directory + "part.b", joinColumnsFirstRelation,
	        			directory + "partsupp.b", joinColumnsSecondRelation,
	        			directory + "table_smaller_temp.b",
	        			directory + "table_bigger_temp.b",
	        			directory + "table_HashJoin.b");	
	        	hj = null;
	        	hj = new HashJoinDefault();
	        	joinColumnsFirstRelation = new String[2];
				joinColumnsFirstRelation[0]= "partkey";
				joinColumnsFirstRelation[1]= "suppkey";
				joinColumnsSecondRelation = new String[2];
				joinColumnsSecondRelation[0]="partkey";
				joinColumnsSecondRelation[1]="suppkey";
				hj.hashJoin(directory + "table_HashJoin.b", joinColumnsFirstRelation,
	        			directory + "lineitem.b", joinColumnsSecondRelation,
	        			directory + "table_smaller_temp.b",
	        			directory + "table_bigger_temp.b",
	        			directory + "table_HashJoin2.b");				
	        }
	        
	        if (args[6].equals("3")) {
	        	/* select  c_name, o_orderstatus, o_totalprice, l_quantity, l_linestatus, l_comment
	        	 * from customer, order, lineitem
	        	 * where order.custkey = customer.custkey
	        	 * and lineitem.partkey = order.orderkey
	        	*/
	        	resultlog.writeLog("select  c_name, o_orderstatus, o_totalprice, l_quantity, l_linestatus, l_comment " +
	        	 " from customer, orders, lineitem" +
	        	 " where orders.custkey = customer.custkey" +
	        	 " and lineitem.partkey = order.orderkey");
	        	
	        	HashJoinDefault hj = new HashJoinDefault();
	        	joinColumnsFirstRelation = new String[1];
				joinColumnsFirstRelation[0]= "custkey";
				joinColumnsSecondRelation = new String[1];
				joinColumnsSecondRelation[0]="custkey";
				hj.hashJoin(directory + "customer.b", joinColumnsFirstRelation,
	        			directory + "orders.b", joinColumnsSecondRelation,
	        			directory + "table_smaller_temp.b",
	        			directory + "table_bigger_temp.b",
	        			directory + "table_HashJoin.b");	
	        	hj = null;
	        	hj = new HashJoinDefault();
	        	joinColumnsFirstRelation = new String[1];
				joinColumnsFirstRelation[0]= "orderkey";
				joinColumnsSecondRelation = new String[1];
				joinColumnsSecondRelation[0]="orderkey";
	        	hj.hashJoin(directory + "lineitem.b", joinColumnsFirstRelation,
	        			directory + "table_HashJoin.b", joinColumnsSecondRelation,
	        			directory + "table_smaller_temp.b",
	        			directory + "table_bigger_temp.b",
	        			directory + "table_HashJoin2.b");
				
	        }
	        
	        if (args[6].equals("4")) {
	        	/* select p_name, suppkey, ps_availqty, l_quantity, l_linestatus, l_comment,
	        	 *        c_name, o_orderstatus, o_totalprice
	        	 * from part, partsupp, customer, order, lineitem
	        	 * where part.partkey = partsupp.partkey
	        	 * and lineitem.partkey = part.partkey
	        	 * and lineitem.suppkey = partsupp.suppkey        	 * 
	        	 * and order.custkey = customer.custkey
	        	 * and lineitem.orderkey = order.orderkey
	        	*/
	        	resultlog.writeLog("select p_name, suppkey, ps_availqty, l_quantity, l_linestatus, l_comment," +
	        	 "        c_name, o_orderstatus, o_totalprice" +
	        	 " from part, partsupp, customer, order, lineitem" +
	        	 " where part.partkey = partsupp.partkey" +
	        	 " and order.custkey = customer.custkey" +
	        	 " and lineitem.partkey = part.partkey" +
	        	 " and lineitem.suppkey = partsupp.suppkey" +
	        	 " and lineitem.orderkey = order.orderkey");
	        	
	        	HashJoinDefault hj = new HashJoinDefault();
	        	joinColumnsFirstRelation = new String[1];
				joinColumnsFirstRelation[0]= "partkey";
				joinColumnsSecondRelation = new String[1];
				joinColumnsSecondRelation[0]="partkey";
	        	hj.hashJoin(directory + "part.b", joinColumnsFirstRelation,
	        			directory + "partsupp.b", joinColumnsSecondRelation,
	        			directory + "table_smaller_temp.b",
	        			directory + "table_bigger_temp.b",
	        			directory + "table_HashJoin.b");	
	        	hj = null;
	        	hj = new HashJoinDefault();
	        	hj.hashJoin(directory + "table_HashJoin.b", joinColumnsFirstRelation,
	        			directory + "lineitem.b", joinColumnsSecondRelation,
	        			directory + "table_smaller_temp.b",
	        			directory + "table_bigger_temp.b",
	        			directory + "table_HashJoin2.b");
	        	
	        	hj = null;
	        	hj = new HashJoinDefault();
	        	joinColumnsFirstRelation = new String[1];
				joinColumnsFirstRelation[0]= "custkey";
				joinColumnsSecondRelation = new String[1];
				joinColumnsSecondRelation[0]="custkey";
	        	hj.hashJoin(directory + "customer.b", joinColumnsFirstRelation,
	        			directory + "orders.b", joinColumnsSecondRelation,
	        			directory + "table_smaller_temp.b",
	        			directory + "table_bigger_temp.b",
	        			directory + "table_HashJoin3.b");	
	        	
	        	hj = null;
	        	hj = new HashJoinDefault();
	        	joinColumnsFirstRelation = new String[1];
				joinColumnsFirstRelation[0]= "orderkey";
				joinColumnsSecondRelation = new String[1];
				joinColumnsSecondRelation[0]="orderkey";
	        	hj.hashJoin(directory + "table_HashJoin2.b", joinColumnsFirstRelation,
	        			directory + "table_HashJoin3.b", joinColumnsSecondRelation,
	        			directory + "table_smaller_temp.b",
	        			directory + "table_bigger_temp.b",
	        			directory + "table_HashJoin4.b");
				
	        }		
	        
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		/*
		try {
			String[] joinColumnsFirstRelation = new String[] { "partkey",
					"suppkey" };
			String[] joinColumnsSecondRelation = new String[] { "partkey",
					"suppkey" };

			hj.hashJoin("C:/RAFIO/TPCH_1/partsupp.b",
					joinColumnsSecondRelation, "C:/RAFIO/TPCH_1/lineitem.b",
					joinColumnsFirstRelation,
					"C:/RAFIO/TPCH_1/HashJoinDefault_1_table_smaller_temp.b",
					"C:/RAFIO/TPCH_1/HashJoinDefault_1_table_bigger_temp.b",
					"C:/RAFIO/TPCH_1/HashJoinDefault_1_table_HashJoin.b");

			joinColumnsFirstRelation = new String[] { "partkey" };
			joinColumnsSecondRelation = new String[] { "partkey" };

			hj.hashJoin("C:/RAFIO/TPCH_1/HashJoinDefault_1_table_HashJoin.b",
					joinColumnsFirstRelation, "C:/RAFIO/TPCH_1/part.b",
					joinColumnsFirstRelation,
					"C:/RAFIO/TPCH_1/HashJoinDefault_2_table_smaller_temp.b",
					"C:/RAFIO/TPCH_1/HashJoinDefault_2_table_bigger_temp.b",
					"C:/RAFIO/TPCH_1/HashJoinDefault_Final_table_HashJoin.b");
		} catch (Exception e) {
			e.printStackTrace();
		}*/

		// Precisa do flush para gravar o que ficou na memoria
		// NEUSA quando uso com COD_BOOK ficam 2 tuplas a menos e n�o achei o
		// erro
		// Quando fa�o com as tabelas do TPCH d� erro na hora de ler, n�o pude
		// saber se ficou correto.
		// hj.hashJoin("C:/RAFIO/bookedition.b", "COD_BOOK",
		// "C:/RAFIO/book.b", "COD_BOOK");

	}

}
