package br.scmjoin;

import java.util.Random;

public class HashFunctionDaC {

	static Random generator = new Random();
	int hashTableSize;
	static int a, b, p;

	public HashFunctionDaC(int N) {
		p = 4 * N + 1;
		hashTableSize = N;
		while (a == 0)
			a = generator.nextInt(p - 1);
		while (b == 0)
			b = generator.nextInt(p - 1);
	}

	public HashFunctionDaC() {
		// TODO Auto-generated constructor stub
	}

	public int hashCode(long key) {
		int i = (int) ((key >>> 32) + (int) key);
		return compressHashCode(i, hashTableSize);
	}

	public int hashCode(Double key) {
		long bits = Double.doubleToLongBits(key);
		int i = (int) (bits ^ (bits >>> 32));
		return compressHashCode(i, hashTableSize);
	}

	public int hashCode(int key) {
		return compressHashCode(key, hashTableSize);
	}

	static int compressHashCode(int i, int N) {
		return i%N;
	}

	public  int hashFunction(int i, int buckets) {
		int hf = i % buckets;
		return hf + 1;
	}
}
